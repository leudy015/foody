import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import PivateRouter from "./PrivateRouter";
import PublicRouter from "./PublicRouter";

import * as Sentry from "@sentry/browser";
import Session from "./Sesion";

///////////// Screens /////////////////
import Home from "./containers/Pages/Home";
import Login from "./containers/Pages/Login";
import RegisterUser from "./containers/Pages/RegisterUser";
import VerifyPhone from "./containers/Pages/Login/VerifyPhone";
import VerifyPhone1 from "./containers/Pages/Login/VerifyPhone1";
import SendCode from "./containers/Pages/Login/Sendcode";
import SendCode1 from "./containers/Pages/Login/Sendcode1";
import Forgot from "./containers/Pages/RecoverPassword";
import Reset from "./containers/Pages/RecoverPassword/Reset";
import ResetRestaurant from "./containers/Pages/RecoverPasswordRestaurant/Reset";
import Profile from "./containers/Pages/Profile";
import Header from "./containers/Components/Header";
import Footer from "./containers/Components/Footer";
import Restaurant from "./containers/Pages/Restaurant";
import RestaurantDetails from "./containers/Pages/DetailsRestaurant";
import OrdenScreen from "./containers/Pages/OrdenScreen";
import Success from "./containers/Pages/Results/success";
import Errors from "./containers/Pages/Results/error";
import NoFound from "./containers/Pages/Results/nofound";
import PrivateRoute from "./PrivateRouter";
import Restaurantes from "./containers/Pages/Restaurantes";
import Cookies from "./containers/Pages/helpPage/cookies";
import Condition from "./containers/Pages/helpPage/termandconditioin";
import Privacity from "./containers/Pages/helpPage/privacity";
import QyA from "./containers/Pages/helpPage/QyA";
import Team from "./containers/Pages/helpPage/team";

/////////////// End //////////////////

Sentry.init({
  dsn: "https://f955be9c777e4aecb34bb34a01e90f36@sentry.io/1778988",
});

const App = (props) => {
  return (
    <BrowserRouter>
      <Header session={props.session} refetch={props.refetch} />
      <div id="page-wrap">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <PublicRouter exact path="/login" component={Login} />
          <PublicRouter exact path="/register" component={RegisterUser} />
          <Route exact path="/verify-phone/:id">
            <VerifyPhone />
          </Route>
          <Route exact path="/verify-phone/:id/:prof">
            <VerifyPhone />
          </Route>
          <Route exact path="/verify-phone/:id">
            <VerifyPhone1 />
          </Route>
          <Route exact path="/send-code/:id/:phone">
            <SendCode />
          </Route>
          <Route exact path="/send-code/:id/:phone/:prof">
            <SendCode1 />
          </Route>
          <PublicRouter exact path="/forgot-password" component={Forgot} />
          <PublicRouter exact path="/reset-password/:token" component={Reset} />
          <PublicRouter
            exact
            path="/reset-password-restaurant/:token"
            component={ResetRestaurant}
          />
          <PivateRouter exact path="/profile" component={Profile} />
          <Route exact path="/search">
            <Restaurant />
          </Route>
          <Route exact path="/search/:search">
            <Restaurant />
          </Route>
          <Route exact path="/details-restaurant/:id">
            <RestaurantDetails />
          </Route>
          <PrivateRoute
            exact
            path="/order/:id/:restaurant/:options/:total/:extras"
            component={OrdenScreen}
          />
          <PrivateRoute exact path="/payment-success/:id" component={Success} />
          <PrivateRoute exact path="/payment-error" component={Errors} />
          <Route exact path="/restaurant">
            <Restaurantes />
          </Route>
          <Route exact path="/cookies">
            <Cookies />
          </Route>
          <Route exact path="/condiciones-de-uso">
            <Condition />
          </Route>
          <Route exact path="/politica-de-privacidad">
            <Privacity />
          </Route>
          <Route exact path="/preguntas-frecuentes">
            <QyA />
          </Route>
          <Route exact path="/team">
            <Team />
          </Route>
          <Route component={NoFound} />
        </Switch>
      </div>
      <Footer />
    </BrowserRouter>
  );
};

const RootSession = Session(App);

export { RootSession };

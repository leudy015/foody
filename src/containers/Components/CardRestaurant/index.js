import React, { useState } from "react";
import { StarFilled, HeartOutlined, HeartFilled } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { useMutation } from "react-apollo";
import {
  ANADIR_RESTAURANT_FAVORITE,
  ELIMINAR_RESTAURANT_FAVORITE,
} from "../../GraphQL/mutation";
import "./index.css";
import { message } from "antd";

function CardRestaurant(props) {
  const { history, res, key, refetch, coordetate, coordenateone } = props;
  const [distacia, setDistancia] = useState(1200);
  const [time, setTime] = useState("5 min");
  const [eliminarFavorito] = useMutation(ELIMINAR_RESTAURANT_FAVORITE);
  const [crearFavorito] = useMutation(ANADIR_RESTAURANT_FAVORITE);

  const usuario = localStorage.getItem("id");

  const SetUbicacion = () => {
    let apiUrlWithParams = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${coordetate},${coordenateone}&destinations=${res.coordetate},${res.coordenateone}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
    fetch(apiUrlWithParams)
      .then((response) => response.json())
      .then((data) => {
        const datos = data.rows[0].elements[0].distance;
        const ti = data.rows[0].elements[0].duration;
        setDistancia(datos.value);
        setTime(ti.text);
      })
      .catch((error) => {
        console.log("error in getting lat, lng: ", error);
      });
  };

  SetUbicacion();

  let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
  res.Valoracion.forEach((start) => {
    if (start.value === 1) rating["1"] += 1;
    else if (start.value === 2) rating["2"] += 1;
    else if (start.value === 3) rating["3"] += 1;
    else if (start.value === 4) rating["4"] += 1;
    else if (start.value === 5) rating["5"] += 1;
  });

  const ar =
    (5 * rating["5"] +
      4 * rating["4"] +
      3 * rating["3"] +
      2 * rating["2"] +
      1 * rating["1"]) /
    res.Valoracion.length;
  let averageRating = 0;
  if (res.Valoracion.length) {
    averageRating = Number(ar.toFixed(1));
  }

  let consideracion = "";
  let color = "#FFCB40";

  switch (averageRating) {
    case 0:
      consideracion = "Sin valoraciones";
      color = "#FFA500";
      break;

    case 1:
    case 1.1:
    case 1.2:
    case 1.3:
    case 1.4:
    case 1.5:
    case 1.5:
    case 1.6:
    case 1.7:
    case 1.8:
    case 1.9:
      consideracion = "Mala";
      color = "#F5365C";
      break;

    case 2:
    case 2.1:
    case 2.2:
    case 2.3:
    case 2.4:
    case 2.5:
    case 2.5:
    case 2.6:
    case 2.7:
    case 2.8:
    case 2.9:
      consideracion = "Regular";
      color = "#FFFF00";
      break;

    case 3:
    case 3.1:
    case 3.2:
    case 3.3:
    case 3.4:
    case 3.5:
    case 3.5:
    case 3.6:
    case 3.7:
    case 3.8:
    case 3.9:
      consideracion = "Buena";
      color = "#1a73e8";
      break;

    case 4:
    case 4.1:
    case 4.2:
    case 4.3:
    case 4.4:
    case 4.5:
    case 4.5:
    case 4.6:
    case 4.7:
    case 4.8:
    case 4.9:
      consideracion = "Exelente";
      color = "#95ca3e";
      break;

    case 5:
      consideracion = "Exelente";
      color = "#95ca3e";
      break;
  }

  const anadirFavorite = (restaurant, id) => {
    if (!usuario) {
      message.warning("Debes iniciar sesión para añadir a favorito");
      setTimeout(() => {
        history.push(`/login?id=${restaurant}`);
      }, 2000);
    } else {
      crearFavorito({ variables: { restaurantID: restaurant, usuarioId: id } })
        .then(() => {
          message.success("Restaurante añadido a la lista de deseos");
          refetch();
        })
        .catch((err) => {
          message.error("Algo salió mal intentalo de nuevo por favot");
          console.log(err);
        });
    }
  };

  const eliminarFavorite = (restaurant) => {
    eliminarFavorito({ variables: { id: restaurant } })
      .then(() => {
        message.success("Restaurante eliminado a la lista de deseos");
        refetch();
      })
      .catch((err) => {
        message.error("Algo salió mal intentalo de nuevo por favot");
        console.log(err);
      });
  };

  return (
    <div className="card__restaurant" key={key}>
      {res.isnew ? <div className="nuevo">Nuevo</div> : null}

      <div className="favourite">
        {res.anadidoFavorito ? (
          <HeartFilled
            onClick={() => eliminarFavorite(res._id)}
            style={{ cursor: "pointer" }}
          />
        ) : (
          <HeartOutlined
            onClick={() => anadirFavorite(res._id, usuario)}
            style={{ cursor: "pointer" }}
          />
        )}
      </div>

      {!res.open ? <h3 className="volvemos">Volvemos pronto</h3> : null}

      <img
        onClick={() => history.push(`/details-restaurant/${res._id}`)}
        src={res.image}
        alt="imagen"
        className="img__cont"
        style={{
          filter: !res.open ? "blur(3px)" : "blur(0px)",
          WebkitFilter: !res.open ? "blur(3px)" : "blur(0px)",
        }}
      />
      <div className="min">Pedido min. {res.minime}€</div>
      <div className="infor_res">
        <h1>{res.title}</h1>
        <div style={{ display: "flex", color: color }}>
          <StarFilled
            style={{
              marginRight: 10,
              color: color,
            }}
          />{" "}
          {averageRating} {consideracion} ({res.Valoracion.length})
        </div>

        <div style={{ color: "gray", lineHeight: 1.2 }} className="inn">
          {res.categoryName} · {res.type} · A {distacia / 1000} Km · {time}
        </div>
      </div>
    </div>
  );
}

export default withRouter(CardRestaurant);

import React, { useState } from "react";
import "./index.css";
import {
  TwitterOutlined,
  InstagramFilled,
  FacebookFilled,
  LinkedinFilled,
  TransactionOutlined,
  MailOutlined,
  RightOutlined,
  PhoneOutlined,
  EnvironmentOutlined,
  ShopOutlined,
} from "@ant-design/icons";
import { Select, message, Modal } from "antd";
import SimpleMap from "../Maps";

const MainLogo = require("../../Assets/images/icon.png");
const Android = require("../../Assets/images/google.png");
const Apple = require("../../Assets/images/apple.svg");

const { Option } = Select;

const Footer = () => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <div className="Footer">
        <div className="Footer-Content">
          <div className="SuBitems">
            <img src={MainLogo} style={{ width: 60, marginBottom: 15 }} />
            <div>
              <p className="text">
                Haz tu pedido con la app y recógelo en minutos en los
                restaurantes de tu ciudad.
              </p>
            </div>
            <div style={{ display: "flex", width: "100%" }}>
              <a
                target="_blank"
                href="https://play.google.com/store/apps/details?id=com.foodyapp"
              >
                <img
                  src={Android}
                  className="logos googl"
                  style={{ marginRight: 15 }}
                />
              </a>
              <a
                target="_blank"
                href="https://apps.apple.com/es/app/foody-pick-up/id1528906592"
              >
                <img src={Apple} className="logos googl" />
              </a>
            </div>
            <div style={{ display: "flex", marginTop: 20 }}>
              <a
                href="https://twitter.com/foodyapp_es"
                className="icons"
                target="_blank"
              >
                <TwitterOutlined style={{ fontSize: 22, marginRight: 15 }} />
              </a>
              <a
                href="https://www.facebook.com/foodyappes"
                className="icons"
                target="_blank"
              >
                <FacebookFilled style={{ fontSize: 22, marginRight: 15 }} />
              </a>
              <a
                href="https://www.instagram.com/foodyapp_es/"
                className="icons"
                target="_blank"
              >
                <InstagramFilled style={{ fontSize: 22, marginRight: 15 }} />
              </a>
              <a
                href="https://www.linkedin.com/company/locatefit/"
                className="icons"
                target="_blank"
              >
                <LinkedinFilled style={{ fontSize: 22, marginRight: 15 }} />
              </a>
            </div>
          </div>
          <div className="SuBitemsxs">
            <ul>
              <li>
                <span className="SuBitemsspan">Acerca de Foody</span>
              </li>
              <li>
                <a href="/restaurant">Para restaurantes</a>
              </li>
              <li>
                <a href="#ventajas">Ventajas</a>
              </li>
              <li>
                <a href="/team">Nuestro equipo</a>
              </li>
              <li>
                <a href="https://blog.foodyapp.es/" target="_blank">
                  Blog
                </a>
              </li>
              <li>
                <a
                  onClick={() =>
                    message.warning("Aún no tenemos vacantes disponible")
                  }
                >
                  Trabaja con nosotros
                </a>
              </li>
            </ul>
          </div>
          <div className="SuBitemsxs">
            <ul>
              <li>
                <span className="SuBitemsspan">Obtener ayuda</span>
              </li>
              <li>
                <a
                  onClick={() => setVisible(true)}
                  style={{ cursor: "pointer" }}
                >
                  Contacto
                </a>
              </li>
              <li>
                <a href="/preguntas-frecuentes">Preguntas frecuentes</a>
              </li>
              <li>
                <a href="/politica-de-privacidad">Privacidad</a>
              </li>
              <li>
                <a href="/condiciones-de-uso">Condiciones de uso</a>
              </li>
              <li>
                <a href="/cookies">Cookies</a>
              </li>
            </ul>
          </div>
          <div className="SuBitems">
            <Select defaultValue="españa" style={{ width: 140 }}>
              <Option value="españa"> 🇪🇸 España</Option>
              <Option value="lucy">🇫🇷 Francia </Option>
              <Option value="Yiminghe">🇬🇧 United Kingdom</Option>
              <Option value="Yiminghe">🇵🇹 Protugal</Option>
              <Option value="Yiminghe">🇺🇸 USA</Option>
            </Select>
            <Select
              defaultValue="españa"
              style={{ width: 140, marginTop: 20, color: "white" }}
              bordered={false}
            >
              <Option value="idioma">
                {" "}
                <TransactionOutlined
                  style={{ color: "#ffcb40", marginRight: 5 }}
                />{" "}
                Idioma
              </Option>
              <Option value="españa"> 🇪🇸 Español</Option>
              <Option value="lucy">🇫🇷 Frances </Option>
              <Option value="Yiminghe">🇬🇧 Ingles United Kingdom</Option>
              <Option value="Yiminghe">🇵🇹 Protugues</Option>
              <Option value="Yiminghe">🇺🇸 Ingles USA</Option>
            </Select>
          </div>
        </div>
        <div className="Subfooter" />
        <div className="certificados">
          <p style={{ color: "gray", marginRight: 20 }}>© 2020 Foody</p>
        </div>
      </div>
      <Modal
        title="Contacto"
        visible={visible}
        footer={false}
        onCancel={() => setVisible(false)}
      >
        <SimpleMap
          lat={42.3509557}
          lgn={-3.7657629}
          width="100%"
          height={200}
          title="Foody® Pick Up"
        />
        <div style={{ margin: 20, paddingBottom: 30 }}>
          <div className="item_contact">
            <MailOutlined
              style={{ marginRight: 15, color: "#ffcb40", fontSize: 22 }}
            />{" "}
            <a
              href="mailto://info@foodyapp.es"
              style={{ color: "black" }}
              target="_blank"
            >
              info@foodyapp.es
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <PhoneOutlined
              style={{ marginRight: 15, color: "#ffcb40", fontSize: 22 }}
            />{" "}
            <a href="tel:+34689351592" style={{ color: "black" }}>
              Hablar con el equipo de Foody®
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <EnvironmentOutlined
              style={{ marginRight: 15, color: "#ffcb40", fontSize: 22 }}
            />{" "}
            <a href="/" style={{ color: "black" }}>
              Burgos ES
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <ShopOutlined
              style={{ marginRight: 15, color: "#ffcb40", fontSize: 22 }}
            />{" "}
            <a href="/restaurant" style={{ color: "black" }}>
              ¿Tienes un restaurante?
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
        </div>
      </Modal>
    </>
  );
};

export default Footer;

import React, { useState } from "react";
import { Query, useMutation } from "react-apollo";
import { GET_MY_CART_RESTAURANT } from "../../GraphQL/query";
import { DELETE_CARD_ITEMS } from "../../GraphQL/mutation";
import { MoreOutlined } from "@ant-design/icons";
import { Tag, Modal, Menu, Dropdown, Button, message } from "antd";
import Complemento from "../../Components/Menu/complementos";
import Opiniones from "../Menu/opiniones";
import { withRouter } from "react-router-dom";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import "./index.css";

const NoData = require("../../Assets/images/order.png");

function trunc(x, posiciones = 0) {
  var s = x.toString();
  var l = s.length;
  var decimalLength = s.indexOf(".") + 1;
  var numStr = s.substr(0, decimalLength + posiciones);
  return Number(numStr);
}

function ShoppingCart(props) {
  const {
    id,
    restaurant,
    date,
    tipo,
    restaurants,
    history,
    fromPedido,
  } = props;
  const [showModal, setShowModal] = useState(false);
  const [dataModal, setDataModal] = useState(null);
  const [eliminarCardItem] = useMutation(DELETE_CARD_ITEMS);

  const show = (datas) => {
    setShowModal(true);
    setDataModal(datas);
  };

  const hide = () => {
    setShowModal(false);
    setDataModal(null);
  };

  const deleteItemCards = (id, refetch) => {
    setShowModal(false);
    eliminarCardItem({ variables: { id: id } })
      .then(async () => {
        refetch();
        message.success("Articulo eliminado del carrito");
      })
      .catch((err) => {
        console.log("err", err);
        message.error("Algo va mal intentalo de nuevo por favor");
      });
  };

  return (
    <Query
      query={GET_MY_CART_RESTAURANT}
      variables={{ id: id, restaurant: restaurant }}
    >
      {(response) => {
        if (response.loading) {
          return (
            <div className="menu__container" style={{ marginRight: 30 }}>
              <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                <div>
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                </div>
              </SkeletonTheme>
            </div>
          );
        }
        if (response) {
          const respuesta =
            response && response.data && response.data.getMyItemCart
              ? response.data.getMyItemCart.list
              : [];

          response.refetch();

          var suma = 0;
          var totales = 0;
          var extras = 0;
          respuesta.forEach(function (numero) {
            const numeros =
              Number.parseFloat(numero.plato.price) * numero.plato.cant;
            const todos = Number(numero.total);
            const extra = Number(numero.extra);
            suma += numeros;
            totales += todos;
            extras += extra;
          });
          const navegar = () => {
            var options = "";
            switch (tipo) {
              case "Comer aquí":
                options = "Comer aquí";
                break;
              case "Recoger en 25 min":
                options = "Recoger en 25 min";
                break;
              default:
                options = date;
                break;
            }
            history.push(
              `/order/${id}/${restaurant}/${options}/${suma}/${extras}`
            );
          };
          return (
            <div className="menu__container">
              {!fromPedido ? (
                <h1 style={{ marginBottom: 30 }}>Mi carrito</h1>
              ) : null}

              {respuesta.length === 0 ? (
                <div className="No_opinion">
                  <img src={NoData} alt="no_cart" style={{ width: 250 }} />
                  <p style={{ marginTop: -30 }}>
                    Aún no has añadido articulo al carrito
                  </p>
                </div>
              ) : null}

              {respuesta.map((p, i) => {
                const total = Number.parseFloat(p.plato.price) * p.plato.cant;
                return (
                  <div
                    className="plato-card my_cart"
                    key={i}
                    onClick={() => show(p)}
                  >
                    <div>
                      <div style={{ display: "flex" }}>
                        <h4>
                          <span style={{ color: "#ffcb40" }}>
                            {p.plato.cant}× {""}
                          </span>
                          {p.plato.title}
                        </h4>
                        {p.news ? (
                          <Tag
                            style={{ marginLeft: 15, height: 22 }}
                            color="lime"
                          >
                            Nuevo
                          </Tag>
                        ) : null}
                      </div>
                      <p>{p.plato.ingredientes}</p>
                      <div style={{ display: "flex" }}>
                        <p style={{ fontWeight: 600, color: "black" }}>
                          {trunc(total, 2).toFixed(2)}€
                        </p>
                        {p.plato.oferta ? (
                          <p
                            style={{
                              fontWeight: 200,
                              color: "#95ca3e",
                              marginLeft: 15,
                            }}
                          >
                            Oferta
                          </p>
                        ) : null}
                        {p.plato.popular ? (
                          <p
                            style={{
                              fontWeight: 200,
                              color: "#FFA500",
                              marginLeft: 15,
                            }}
                          >
                            Popular
                          </p>
                        ) : null}
                      </div>
                    </div>
                    <img src={p.plato.imagen} alt={p.plato.title} />
                  </div>
                );
              })}

              {dataModal ? (
                <Modal
                  title={dataModal.plato.title}
                  visible={showModal}
                  footer={false}
                  onCancel={hide}
                >
                  <div className="modal_contents">
                    <img
                      src={dataModal.plato.imagen}
                      alt={dataModal.plato.title}
                    />
                    <h2>{dataModal.plato.title}</h2>
                    <p>{dataModal.plato.ingredientes}</p>
                    <div style={{ display: "flex" }}>
                      <p style={{ paddingTop: 5, color: "black" }}>
                        {dataModal.plato.price}€
                      </p>
                      {dataModal.plato.oferta ? (
                        <p
                          style={{
                            fontWeight: 200,
                            color: "#95ca3e",
                            marginLeft: 15,
                            paddingTop: 5,
                          }}
                        >
                          Oferta
                        </p>
                      ) : null}
                      {dataModal.plato.popular ? (
                        <p
                          style={{
                            fontWeight: 200,
                            color: "#FFA500",
                            marginLeft: 15,
                            paddingTop: 5,
                          }}
                        >
                          Popular
                        </p>
                      ) : null}
                    </div>

                    <div>
                      <Complemento
                        id={dataModal.plato._id}
                        acompanante={dataModal.complementos}
                        refetch={response.refetch}
                        hide={hide}
                        precio={dataModal.plato.price}
                        cantidad={dataModal.plato.cant}
                        plato={dataModal.plato}
                        restaurant={restaurant}
                      />
                    </div>
                    <Button
                      danger
                      onClick={() =>
                        deleteItemCards(dataModal.id, response.refetch)
                      }
                      type="dashed"
                      style={{ marginTop: 30, width: "100%", height: 60 }}
                    >
                      Eliminar articulo
                    </Button>
                    <div style={{ marginTop: 40 }}>
                      <Opiniones
                        id={dataModal.plato._id}
                        plato={dataModal.plato.title}
                      />
                    </div>
                  </div>
                </Modal>
              ) : null}

              {!fromPedido ? (
                <>
                  {respuesta.length > 0 ? (
                    <div className="btn-cont">
                      <Button
                        onClick={() => navegar()}
                        type="primary"
                        style={{ marginTop: 30, width: "100%", height: 60 }}
                      >
                        {`Pedir ${respuesta.length} por ${totales.toFixed(2)}€`}
                      </Button>
                    </div>
                  ) : null}
                </>
              ) : null}
            </div>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(ShoppingCart);

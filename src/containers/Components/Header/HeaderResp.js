import React from "react";
import { reveal as Menu } from "react-burger-menu";
import "./HeadersResp.css";

class HeaderResp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Menu pageWrapId={"page-wrap"} outerContainerId={"outer-container"} right>
        {!this.props.data ? (
          <a
            href="/login"
            className="btn-custom-ptimary"
            style={{ marginTop: 40 }}
          >
            Iniciar sesión
          </a>
        ) : null}
        {!this.props.data ? (
          <a href="/register" className="btn-custom-secundary">
            Regístrarme
          </a>
        ) : null}
        <a
          id="about"
          className="menu-item"
          href="/restaurant"
          target="blak"
          style={{ marginTop: 30 }}
        >
          ¿Tienes un restaurante?
        </a>
        <a id="home" className="menu-item" href="/search/">
          Explorar
        </a>
        <a id="about" className="menu-item" href="/search/">
          Restaurantes
        </a>
      </Menu>
    );
  }
}

export default HeaderResp;

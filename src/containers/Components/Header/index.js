import React from "react";
import "./Headers.css";
import $ from "jquery";
import HeaderResp from "./HeaderResp";
import {
  BellOutlined,
  EyeOutlined,
  ClockCircleOutlined,
  HeartOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import {
  USER_DETAIL,
  GET_NOTIFICATION,
  RESTAURANT_FAVORITO,
} from "../../GraphQL/query";
import { Query, Mutation } from "react-apollo";
import { Avatar, Badge, Popover, Empty, message, List, Spin } from "antd";
import { IMAGES_PATH } from "../../Utils/UrlConfig";
import { READ_NOTIFICATION } from "../../GraphQL/mutation";
import moment from "moment";
import "moment/locale/es";
import CardRestaurant from "../../Components/CardRestaurant";

const noti = require("../../Assets/images/notification.png");
const fav = require("../../Assets/images/heart.png");

const Headers = ({ session, refetch }) => {
  refetch();
  if (session && session.obtenerUsuario && session.obtenerUsuario.id) {
    localStorage.setItem("id", session.obtenerUsuario.id);
  }

  let barra = session.obtenerUsuario ? (
    <UsuarioAutenticado user={session} />
  ) : (
    <UsuarioNoAutenticado />
  );

  refetch();
  return <div>{barra}</div>;
};

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 30) {
      $(".nav").addClass("shadow");
      /* $(".logo").addClass("logo-white"); */
    } else {
      $(".nav").removeClass("shadow");
      /* $(".logo").removeClass("logo-white"); */
    }
  });
});

let id = localStorage.getItem("id");

const UsuarioAutenticado = () => {
  return (
    <Query query={USER_DETAIL} variables={{ id }}>
      {({ loading, error, data }) => {
        if (error) {
          console.log("Response Error-------", error);
          return null;
        }
        if (loading) {
          return null;
        }
        if (data) {
          const userDatas = data.getUsuario.data;
          return (
            <div className="nav">
              <div className="ContainerNav">
                <div>
                  <a href="/">
                    <div className="logo" />
                  </a>
                </div>
                <div className="items">
                  <a href="/restaurant" className="btn-custom-secundary">
                    <HomeOutlined /> ¿Tienes un restaurante?
                  </a>

                  <Query query={GET_NOTIFICATION} variables={{ Id: id }}>
                    {({ loading, error, data, refetch }) => {
                      refetch = refetch;
                      if (error)
                        return console.log(
                          "error getting user detail: ",
                          error
                        );

                      let notificaciones = [];
                      let contentnotiCount = 0;
                      if (data && data.getNotifications.notifications) {
                        notificaciones = data.getNotifications.notifications;
                        contentnotiCount =
                          data.getNotifications.notifications.length;
                      }
                      return (
                        <Badge count={contentnotiCount} overflowCount={10}>
                          <Popover
                            placement="bottom"
                            title={text}
                            content={content(notificaciones, refetch)}
                            trigger="click"
                            className="Box-notification"
                          >
                            <a className="subItems-img">
                              {loading ? (
                                <Spin size="small" />
                              ) : (
                                <BellOutlined
                                  style={{ fontSize: 30, color: "#ffcb40" }}
                                />
                              )}
                            </a>
                          </Popover>
                        </Badge>
                      );
                    }}
                  </Query>

                  <Query query={RESTAURANT_FAVORITO} variables={{ id: id }}>
                    {({ loading, error, data, refetch }) => {
                      if (error)
                        return console.log(
                          "error getting user detail: ",
                          error
                        );

                      let getRestaurantFavorito = [];
                      let getRestaurantFavoritoCount = 0;
                      if (
                        data &&
                        data.getRestaurantFavorito &&
                        data.getRestaurantFavorito.list
                      ) {
                        getRestaurantFavorito = data.getRestaurantFavorito.list;

                        getRestaurantFavoritoCount =
                          data.getRestaurantFavorito.list.length;
                      }
                      return (
                        <Popover
                          content={contentheart(getRestaurantFavorito, refetch)}
                          trigger="click"
                          placement="bottom"
                        >
                          <Badge
                            count={getRestaurantFavoritoCount}
                            overflowCount={9}
                          >
                            <a className="subItems-img">
                              {loading ? (
                                <Spin size="small" />
                              ) : (
                                <HeartOutlined
                                  style={{ fontSize: 30, color: "#ffcb40" }}
                                />
                              )}
                            </a>
                          </Badge>
                        </Popover>
                      );
                    }}
                  </Query>
                  <a href="/profile" className="subItems-img">
                    <Avatar
                      src={IMAGES_PATH + userDatas.avatar}
                      style={{ marginTop: -10 }}
                    />
                  </a>
                  <a href="/profile" className="subItems">
                    {userDatas ? userDatas.nombre : ""}{" "}
                    {userDatas ? userDatas.apellidos : ""}
                  </a>
                </div>
                <div></div>
                <div className="RespMenu">
                  {!userDatas ? (
                    <HeaderResp data={userDatas ? userDatas : ""} />
                  ) : null}
                  <div style={{ textAlign: "center" }}>
                    <Query query={GET_NOTIFICATION} variables={{ Id: id }}>
                      {({ loading, error, data, refetch }) => {
                        refetch = refetch;
                        if (error)
                          return console.log(
                            "error getting user detail: ",
                            error
                          );

                        let notificaciones = [];
                        let contentnotiCount = 0;
                        if (data && data.getNotifications.notifications) {
                          notificaciones = data.getNotifications.notifications;
                          contentnotiCount =
                            data.getNotifications.notifications.length;
                        }
                        return (
                          <Badge count={contentnotiCount} overflowCount={9}>
                            <Popover
                              placement="bottom"
                              title={text}
                              content={content(notificaciones, refetch)}
                              trigger="click"
                              className="Box-notification"
                            >
                              <a href="/" className="subItems-img">
                                {loading ? (
                                  <Spin size="small" />
                                ) : (
                                  <BellOutlined style={{ fontSize: 30 }} />
                                )}
                              </a>
                            </Popover>
                          </Badge>
                        );
                      }}
                    </Query>
                    <Query query={RESTAURANT_FAVORITO} variables={{ id: id }}>
                      {({ loading, error, data, refetch }) => {
                        if (error)
                          return console.log(
                            "error getting user detail: ",
                            error
                          );

                        let getRestaurantFavorito = [];
                        let getUsuarioFavoritoCount = 0;
                        if (
                          data &&
                          data.getRestaurantFavorito &&
                          data.getRestaurantFavorito.list
                        ) {
                          getRestaurantFavorito =
                            data.getRestaurantFavorito.list;

                          refetch();

                          getUsuarioFavoritoCount =
                            data.getRestaurantFavorito.list.length;
                        }
                        return (
                          <Popover
                            content={contentheart(
                              getRestaurantFavorito,
                              refetch
                            )}
                            trigger="click"
                            placement="bottom"
                          >
                            <Badge
                              count={getUsuarioFavoritoCount}
                              overflowCount={9}
                            >
                              <a className="subItems-img">
                                {loading ? (
                                  <Spin size="small" />
                                ) : (
                                  <HeartOutlined
                                    style={{ fontSize: 30, color: "#ffcb40" }}
                                  />
                                )}
                              </a>
                            </Badge>
                          </Popover>
                        );
                      }}
                    </Query>

                    <a href="/profile" className="subItems-img">
                      <Avatar
                        src={IMAGES_PATH + userDatas.avatar}
                        style={{ marginTop: -15 }}
                      />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          );
        }
      }}
    </Query>
  );
};

const UsuarioNoAutenticado = () => {
  return (
    <div className="nav">
      <div className="ContainerNav">
        <div>
          <a href="/">
            <div className="logo" />
          </a>
        </div>

        <div className="items">
          <a href="/restaurant" className="btn-custom-secundary">
            <HomeOutlined /> ¿Tienes un restaurante?
          </a>
          <a href="/register" className="btn-custom-secundary">
            Regístrarme
          </a>
          <a href="/login" className="btn-custom-ptimary">
            Iniciar sesión
          </a>
        </div>

        <div className="RespMenu">
          <HeaderResp />
        </div>
      </div>
    </div>
  );
};

const text = <span>Notificaciones</span>;

const content = (notificaciones, refetch) => (
  <div className="notifications">
    {notificaciones && notificaciones.length > 0 ? (
      <List
        className="ListNot"
        itemLayout="horizontal"
        dataSource={notificaciones}
        renderItem={(item) => {
          let title = "";
          let description = "";
          let avatar = "";

          switch (item.type) {
            case "accept_order":
              title = "Pedido confirmado";
              description = `Genial ${item.Restaurant.title} ha confirmado tu pedido.`;
              avatar = item.Restaurant.logo;
              break;

            case "order_process":
              title = "Preparando tu pedido";
              description = `${item.Restaurant.title} está preparando tu pedido en la cocina.`;
              avatar = item.Restaurant.logo;
              break;

            case "finish_order":
              title = "Pedido listo para recojer";
              description = `Tu pedido está listo para recojer en ${item.Restaurant.title}`;
              avatar = item.Restaurant.logo;
              break;

            case "valored_order":
              title = "Hora de dar tu opinión";
              description = `Valora tu experiencia con ${item.Restaurant.title} y tambien opina sobre los platos que compraste para ayudar a otros clientes a decidir mejor`;
              avatar = item.Restaurant.logo;
              break;

            case "reject_order":
              title = "Pedido Rechazado";
              description = `Upps ${item.Restaurant.title} no ha podido realizar tu pedido lo sentimos.`;
              avatar = item.Restaurant.logo;
              break;

            case "resolution_order":
              title = "Pedido en reclamacion";
              description = `Tu pedido está en proceso de reclamación sentimos los inconvenientes.`;
              avatar = item.Restaurant.logo;
              break;
          }

          return (
            <Mutation
              mutation={READ_NOTIFICATION}
              variables={{ notificationId: item._id }}
            >
              {(readNotification) => {
                return (
                  <List.Item>
                    <Badge count={!item.read} dot>
                      <List.Item.Meta
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          readNotification({
                            variables: { notificationId: item._id },
                          }).then(async ({ data: res }) => {
                            if (
                              res &&
                              res.readNotification &&
                              res.readNotification.success
                            ) {
                              message.success(
                                "Notificación marcada como leida"
                              );
                              refetch();
                              console.log(res.readNotification.message);
                            } else if (
                              res &&
                              res.readNotification &&
                              !res.readNotification.success
                            )
                              message.error("Hay un error intentalo de nuevo");
                            console.log(res.readNotification.message);
                          });
                        }}
                        avatar={<Avatar src={avatar} />}
                        title={title}
                        description={description}
                      />
                      <div
                        style={{
                          marginTop: 5,
                          marginLeft: 47,
                          flexDirection: "column",
                          cursor: "pointer",
                        }}
                      >
                        <ClockCircleOutlined
                          style={{ color: "#ddd", marginRight: 5 }}
                          type="clock-circle"
                        />
                        <span style={{ color: "#ddd" }}>
                          {moment(Number(item.created_at)).fromNow()}
                        </span>
                        <EyeOutlined
                          style={{ color: "#ddd", marginLeft: 10 }}
                          type="eye"
                        />{" "}
                        <span style={{ color: "#ddd", marginLeft: 5 }}>
                          Marcar como visto
                        </span>
                      </div>
                    </Badge>
                  </List.Item>
                );
              }}
            </Mutation>
          );
        }}
      />
    ) : (
      <Empty
        image={noti}
        imageStyle={{
          height: 60,
        }}
        description={<span>Estás al día, no tienes notificaciones.</span>}
      ></Empty>
    )}
  </div>
);

const contentheart = (getRestaurantFavorito, refetch) => {
  refetch();
  var latitude = null;
  var longitude = null;

  function getGeoLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          latitude = position.coords.latitude;
          longitude = position.coords.longitude;
        },
        (error) => console.log("geolocation error", error)
      );
    } else {
      console.log("this browser not supported HTML5 geolocation API");
    }
  }

  getGeoLocation();
  return (
    <div className="contenedor">
      <h4>Mi Lista de deseos</h4>
      {getRestaurantFavorito.length > 0 ? (
        getRestaurantFavorito.map((listItem, i) => {
          return (
            <CardRestaurant
              key={i}
              res={listItem.restaurant}
              refetch={refetch}
              coordetate={latitude}
              coordenateone={longitude}
            />
          );
        })
      ) : (
        <div>
          <Empty
            image={fav}
            imageStyle={{
              height: 60,
            }}
            description={
              <span>No tienes restaurantes en la lista de deseos.</span>
            }
          ></Empty>
        </div>
      )}
    </div>
  );
};

export default Headers;

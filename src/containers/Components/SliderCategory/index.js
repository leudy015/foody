import React from "react";
import Slider from "react-slick";
import { Query } from "react-apollo";
import { CATEGORY } from "../../GraphQL/query";
import "./index.css";

export default function Sliders() {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 7,
    slidesToScroll: 2,
    autoplay: true,
    swipeToSlide: false,
    speed: 3000,
    className: "category-preset-slider",
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <div>
      <Query query={CATEGORY}>
        {(response) => {
          if (response.loading) {
            return null;
          }

          if (response) {
            const cat =
              response && response.data && response.data.getCategory
                ? response.data.getCategory.data
                : [];
            return (
              <div>
                <Slider {...settings}>
                  {cat.map((c, i) => {
                    return (
                      <div key={i}>
                        <img
                          src={c.image}
                          alt={c.title}
                          className="category-preset"
                        />
                        <h3 style={{ marginLeft: 25 }} className="cat-name">
                          {c.title}
                        </h3>
                      </div>
                    );
                  })}
                </Slider>
              </div>
            );
          }
        }}
      </Query>
    </div>
  );
}

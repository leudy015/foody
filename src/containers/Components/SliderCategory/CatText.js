import React, { useState } from "react";
import { Query } from "react-apollo";
import { CATEGORY } from "../../GraphQL/query";
import "./index.css";

export default function Sliders() {
  const [selected, setSelected] = useState("5f25f836e63b4e09048aa2d3");
  return (
    <Query query={CATEGORY}>
      {(response) => {
        if (response.loading) {
          return null;
        }

        if (response) {
          const cat =
            response && response.data && response.data.getCategory
              ? response.data.getCategory.data
              : [];
          return (
            <>
              {cat.map((c, i) => {
                return (
                  <span
                    onClick={() => setSelected(c._id)}
                    key={i}
                    className={
                      selected === c._id ? "cat_item-seleted" : "cat_item"
                    }
                  >
                    {c.title}
                  </span>
                );
              })}
            </>
          );
        }
      }}
    </Query>
  );
}

import React, { useState } from "react";
import { GET_MENU } from "../../GraphQL/query";
import { Query } from "react-apollo";
import { StarFilled } from "@ant-design/icons";
import { Popover, Tag, Modal } from "antd";
import Complemento from "./complementos";
import Opiniones from "./opiniones";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import "./index.css";

export default function MenuRestaurant(props) {
  const [showModal, setShowModal] = useState(false);
  const [dataModal, setDataModal] = useState(null);

  const show = (datas) => {
    setShowModal(true);
    setDataModal(datas);
  };

  const hide = () => {
    setShowModal(false);
    setDataModal(null);
  };

  return (
    <Query query={GET_MENU} variables={{ id: props.id }}>
      {(response) => {
        if (response.loading) {
          return (
            <div className="plato__contenedor">
              <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
              </SkeletonTheme>
              <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
                <Skeleton
                  height={100}
                  width="100%"
                  style={{ marginBottom: 20 }}
                />
              </SkeletonTheme>
            </div>
          );
        }

        if (response) {
          const menus =
            response && response.data && response.data.getMenu
              ? response.data.getMenu.list
              : [];
          response.refetch();
          return (
            <>
              {menus.map((c, i) => {
                return (
                  <div className="menu__container" key={i}>
                    <h1 id={c.title}>{c.title}</h1>
                    <p style={{ marginBottom: 30 }} className="subtitle">
                      {c.subtitle}
                    </p>

                    <div className="plato__contenedor">
                      {c.platos.map((p, i) => {
                        let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
                        p.opiniones.forEach((start) => {
                          if (start.rating === 1) rating["1"] += 1;
                          else if (start.rating === 2) rating["2"] += 1;
                          else if (start.rating === 3) rating["3"] += 1;
                          else if (start.rating === 4) rating["4"] += 1;
                          else if (start.rating === 5) rating["5"] += 1;
                        });

                        const ar =
                          (5 * rating["5"] +
                            4 * rating["4"] +
                            3 * rating["3"] +
                            2 * rating["2"] +
                            1 * rating["1"]) /
                          p.opiniones.length;
                        let averageRatings = 0;
                        if (p.opiniones.length) {
                          averageRatings = Number(ar.toFixed(1));
                        }

                        let consideracion = "";
                        let color = "#FFCB40";

                        switch (averageRatings) {
                          case 0:
                            consideracion = "Opiniones";
                            color = "#FFA500";
                            break;

                          case 1:
                          case 1.1:
                          case 1.2:
                          case 1.3:
                          case 1.4:
                          case 1.5:
                          case 1.5:
                          case 1.6:
                          case 1.7:
                          case 1.8:
                          case 1.9:
                            consideracion = "Mala";
                            color = "#F5365C";
                            break;

                          case 2:
                          case 2.1:
                          case 2.2:
                          case 2.3:
                          case 2.4:
                          case 2.5:
                          case 2.5:
                          case 2.6:
                          case 2.7:
                          case 2.8:
                          case 2.9:
                            consideracion = "Regular";
                            color = "#FFFF00";
                            break;

                          case 3:
                          case 3.1:
                          case 3.2:
                          case 3.3:
                          case 3.4:
                          case 3.5:
                          case 3.5:
                          case 3.6:
                          case 3.7:
                          case 3.8:
                          case 3.9:
                            consideracion = "Buena";
                            color = "rgb(58,85,160)";
                            break;

                          case 4:
                          case 4.1:
                          case 4.2:
                          case 4.3:
                          case 4.4:
                          case 4.5:
                          case 4.5:
                          case 4.6:
                          case 4.7:
                          case 4.8:
                          case 4.9:
                            consideracion = "Exelente";
                            color = "#95ca3e";
                            break;

                          case 5:
                            consideracion = "Exelente";
                            color = "#95ca3e";
                            break;
                        }
                        return (
                          <div
                            className="plato-card"
                            key={i}
                            onClick={() => show(p)}
                          >
                            <div>
                              <div style={{ display: "flex" }}>
                                <h4>{p.title}</h4>
                                {p.news ? (
                                  <Tag
                                    style={{ marginLeft: 15, height: 22 }}
                                    color="lime"
                                  >
                                    Nuevo
                                  </Tag>
                                ) : null}
                              </div>
                              <p>{p.ingredientes}</p>
                              <div style={{ display: "flex" }}>
                                <p style={{ fontWeight: 600, color: "black" }}>
                                  {Number(p.price).toFixed(2)}€
                                </p>
                                {p.oferta ? (
                                  <p
                                    style={{
                                      fontWeight: 200,
                                      color: "#95ca3e",
                                      marginLeft: 15,
                                    }}
                                  >
                                    Oferta
                                  </p>
                                ) : null}
                                {p.popular ? (
                                  <p
                                    style={{
                                      fontWeight: 200,
                                      color: "#FFA500",
                                      marginLeft: 15,
                                    }}
                                  >
                                    Popular
                                  </p>
                                ) : null}
                              </div>
                              <Popover
                                content={
                                  <div>
                                    <p>
                                      Aquí puede ver opiniones de clientes sobre
                                      este plato
                                    </p>
                                  </div>
                                }
                              >
                                <p
                                  style={{
                                    fontSize: 10,
                                    color: color,
                                    paddingTop: 4,
                                  }}
                                >
                                  <StarFilled /> {averageRatings}{" "}
                                  {consideracion} ({p.opiniones.length})
                                </p>
                              </Popover>
                            </div>
                            <img src={p.imagen} alt={p.title} />
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
              {dataModal ? (
                <Modal
                  title={dataModal.title}
                  visible={showModal}
                  footer={false}
                  onCancel={hide}
                >
                  <div className="modal_contents">
                    <img src={dataModal.imagen} alt={dataModal.title} />
                    <h2>{dataModal.title}</h2>
                    <p>{dataModal.ingredientes}</p>
                    <div style={{ display: "flex" }}>
                      <p style={{ paddingTop: 5, color: "black" }}>
                        {dataModal.price}€
                      </p>
                      {dataModal.oferta ? (
                        <p
                          style={{
                            fontWeight: 200,
                            color: "#95ca3e",
                            marginLeft: 15,
                            paddingTop: 5,
                          }}
                        >
                          Oferta
                        </p>
                      ) : null}
                      {dataModal.popular ? (
                        <p
                          style={{
                            fontWeight: 200,
                            color: "#FFA500",
                            marginLeft: 15,
                            paddingTop: 5,
                          }}
                        >
                          Popular
                        </p>
                      ) : null}
                    </div>

                    <div>
                      <Complemento
                        id={dataModal._id}
                        acompañante={[]}
                        refetch={response.refetch}
                        hide={hide}
                        precio={dataModal.price}
                        plato={dataModal}
                        restaurant={props.id}
                      />
                    </div>
                    <div style={{ marginTop: 40 }}>
                      <Opiniones id={dataModal._id} plato={dataModal.title} />
                    </div>
                  </div>
                </Modal>
              ) : null}
            </>
          );
        }
      }}
    </Query>
  );
}

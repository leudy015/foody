import React from "react";
import { GET_OPINIONES } from "../../GraphQL/query";
import { Query } from "react-apollo";
import { StarFilled } from "@ant-design/icons";
import { IMAGES_PATH } from "../../Utils/UrlConfig";
import { Avatar } from "antd";
import moment from "moment";

import "./opiniones.css";

const NoData = require("../../Assets/images/nocoment.png");

export default function Opiniones(props) {
  const { id, plato } = props;
  return (
    <Query query={GET_OPINIONES} variables={{ id: id }}>
      {(response) => {
        if (response.loading) {
          return null;
        }
        if (response) {
          response.refetch();
          const opinion =
            response && response.data && response.data.getOpinion
              ? response.data.getOpinion.data
              : [];
          let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
          opinion.forEach((start) => {
            if (start.rating === 1) rating["1"] += 1;
            else if (start.rating === 2) rating["2"] += 1;
            else if (start.rating === 3) rating["3"] += 1;
            else if (start.rating === 4) rating["4"] += 1;
            else if (start.rating === 5) rating["5"] += 1;
          });

          const ar =
            (5 * rating["5"] +
              4 * rating["4"] +
              3 * rating["3"] +
              2 * rating["2"] +
              1 * rating["1"]) /
            opinion.length;
          let averageRating = 0;
          if (opinion.length) {
            averageRating = Number(ar.toFixed(1));
          }

          let consideracion = "";
          let color = "#FFCB40";

          switch (averageRating) {
            case 0:
              consideracion = "Sin opiniones";
              color = "#FFA500";
              break;

            case 1:
            case 1.1:
            case 1.2:
            case 1.3:
            case 1.4:
            case 1.5:
            case 1.5:
            case 1.6:
            case 1.7:
            case 1.8:
            case 1.9:
              consideracion = "Mala";
              color = "#F5365C";
              break;

            case 2:
            case 2.1:
            case 2.2:
            case 2.3:
            case 2.4:
            case 2.5:
            case 2.5:
            case 2.6:
            case 2.7:
            case 2.8:
            case 2.9:
              consideracion = "Regular";
              color = "#FFFF00";
              break;

            case 3:
            case 3.1:
            case 3.2:
            case 3.3:
            case 3.4:
            case 3.5:
            case 3.5:
            case 3.6:
            case 3.7:
            case 3.8:
            case 3.9:
              consideracion = "Buena";
              color = "rgb(58,85,160)";
              break;

            case 4:
            case 4.1:
            case 4.2:
            case 4.3:
            case 4.4:
            case 4.5:
            case 4.5:
            case 4.6:
            case 4.7:
            case 4.8:
            case 4.9:
              consideracion = "Exelente";
              color = "#95ca3e";
              break;

            case 5:
              consideracion = "Exelente";
              color = "#95ca3e";
              break;
          }

          return (
            <div className="container__Opiniones">
              <h3>Opiniones</h3>
              <div
                style={{
                  display: "flex",
                  marginTop: 20,
                  borderBottomWidth: 1,
                  borderBlockColor: "#f4f5f5",
                }}
              >
                <StarFilled style={{ fontSize: 32, color: color }} />
                <div style={{ marginLeft: 10 }}>
                  <p style={{ fontSize: 18, fontWeight: 700, color: "black" }}>
                    {plato}
                  </p>
                  <p style={{ color: color }}>
                    {averageRating} {consideracion} ({opinion.length})
                  </p>
                </div>
              </div>
              {opinion.length === 0 ? (
                <div className="No_opinion">
                  <img src={NoData} alt={plato} style={{ width: 250 }} />
                  <p>Aún no hay opiniones para este plato</p>
                </div>
              ) : null}
              {opinion.map((o, i) => {
                let consideracions = "";
                let colorr = "#FFCB40";
                switch (o.rating) {
                  case 0:
                    consideracions = "Sin opiniones";
                    colorr = "#FFA500";
                    break;

                  case 1:
                    consideracions = "Mala";
                    colorr = "#F5365C";
                    break;

                  case 2:
                    consideracions = "Regular";
                    colorr = "#FFFF00";
                    break;

                  case 3:
                    consideracions = "Buena";
                    colorr = "rgb(58,85,160)";
                    break;

                  case 4:
                    consideracions = "Exelente";
                    colorr = "#95ca3e";
                    break;

                  case 5:
                    consideracions = "Exelente";
                    colorr = "#95ca3e";
                    break;
                }
                return (
                  <div key={i}>
                    <div className="opinion">
                      <div className="info-client">
                        <Avatar
                          src={IMAGES_PATH + o.Usuario.avatar}
                          size={40}
                        />
                        <div style={{ marginLeft: 10 }}>
                          <h4>
                            {o.Usuario.name} {o.Usuario.lastName}
                          </h4>
                          <p>{moment(o.created_at).fromNow()}</p>
                        </div>
                      </div>
                      <div className="val-client">
                        <p style={{ color: colorr }}>
                          <StarFilled style={{ fontSize: 16, color: colorr }} />{" "}
                          {o.rating} {consideracions}
                        </p>
                      </div>
                    </div>
                    <div style={{ marginLeft: 50, marginTop: 15 }}>
                      <p style={{ lineHeight: 1.3 }}>{o.comment}</p>
                    </div>
                  </div>
                );
              })}
            </div>
          );
        }
      }}
    </Query>
  );
}

import React, { useState } from "react";
import { GET_ACOMPANANTE } from "../../GraphQL/query";
import { CREATE_CARD_ITEMS } from "../../GraphQL/mutation";
import { Query, useMutation } from "react-apollo";
import {
  CheckCircleFilled,
  PlusCircleFilled,
  MinusCircleFilled,
} from "@ant-design/icons";
import { message, Button } from "antd";
import { withRouter } from "react-router-dom";

import "./complementos.css";

function trunc(x, posiciones = 0) {
  var s = x.toString();
  var l = s.length;
  var decimalLength = s.indexOf(".") + 1;
  var numStr = s.substr(0, decimalLength + posiciones);
  return Number(numStr);
}

function Complementos(props) {
  const {
    id,
    acompanante,
    cantidad,
    refetch,
    hide,
    precio,
    plato,
    restaurant,
    history,
  } = props;
  const [comple, setComple] = useState(acompanante ? acompanante : []);
  const [count, setCount] = useState(cantidad ? cantidad : 1);
  const [acomp, setAcomp] = useState(0);
  const [extra, setExtra] = useState(0);
  const [createItemCard] = useMutation(CREATE_CARD_ITEMS);

  const user = localStorage.getItem("id");
  const token = localStorage.getItem("token");

  const total = Number.parseFloat(precio) * count;

  const totalFinaly = total + Number(extra);

  const rescantidad = () => {
    if (count < 2) {
      message.warning("El mínimo que puede seleccionar es 1");
    } else {
      setCount(count - 1);
    }
  };

  const sumcantidad = () => {
    if (count > 9) {
      message.warning("El máximo que puede seleccionar es 10");
    } else {
      setCount(count + 1);
    }
  };

  function removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);
    if (i !== -1) {
      arr.splice(i, 1);
      setComple(comple.concat());
    }
  }

  const onSelectedItemsChange = (selectedItems, price) => {
    if (comple.includes(selectedItems)) {
      removeItemFromArr(comple, selectedItems);
      setExtra(extra - price);
    } else {
      if (comple.length === acomp) {
        message.warning("Upps Has seleccionado el número máximo de opciones");
      } else {
        setComple(comple.concat(selectedItems));
        setExtra(extra + price);
      }
    }
  };

  const createItemCards = (userId, restaurant, platoID, datas) => {
    if (!token) {
      message.warning("Debes iniciar sesión para continuar");
      history.push(`/login?id=${restaurant}`);
      return null;
    } else {
      const plato = {
        _id: datas._id ? datas._id : datas.id,
        title: datas.title,
        ingredientes: datas.ingredientes,
        price: datas.price,
        imagen: datas.imagen,
        restaurant: datas.restaurant,
        menu: datas.menu,
        oferta: datas.oferta,
        popular: datas.popular,
        cant: count,
        anadidoCart: true,
      };
      const cardInput = {
        userId: userId,
        restaurant: restaurant,
        plato: plato,
        platoID: platoID,
        total: `${totalFinaly}`,
        extra: `${extra}`,
        complementos: comple,
      };
      console.log(cardInput);
      refetch();
      createItemCard({ variables: { input: cardInput } })
        .then(async () => {
          refetch();
          message.success("Articulo añadido al carrito de la compra");
          hide();
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        })
        .catch((err) => {
          console.log("err", err);
          message.success("Algo va mal intentalo de nuevo por favor");
        });
    }
  };

  return (
    <Query query={GET_ACOMPANANTE} variables={{ id: id }}>
      {(response) => {
        if (response.loading) {
          return null;
        }

        if (response) {
          const resp =
            response && response.data && response.data.getAcompanante
              ? response.data.getAcompanante.list
              : [];
          setAcomp(resp.length);
          response.refetch();
          const len = resp ? resp.length : 0;
          return (
            <div className="container-complementos">
              {resp.map((a, i) => {
                return (
                  <div className="complement" key={i}>
                    <div className="complement__title">
                      <h3>{a.name}</h3>
                    </div>
                    <div className="childerm">
                      {a.children.map((c, i) => {
                        return (
                          <div
                            key={i}
                            className="list-chill"
                            onClick={() =>
                              onSelectedItemsChange(c.name, Number(c.price))
                            }
                          >
                            <p>
                              {c.name} {c.price ? `+ (${c.price}) €` : null}
                            </p>
                            {comple.map((s, i) =>
                              c.name === s ? (
                                <CheckCircleFilled
                                  key={i}
                                  style={{
                                    marginLeft: "auto",
                                    color: "#FFCB40",
                                    fontSize: 18,
                                  }}
                                />
                              ) : null
                            )}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
              <div className="sum-cantidad">
                <MinusCircleFilled
                  style={{ fontSize: 26, marginRight: 20, color: "#ffcb40" }}
                  onClick={() => rescantidad()}
                />
                {count}
                <PlusCircleFilled
                  style={{ fontSize: 26, marginLeft: 20, color: "#ffcb40" }}
                  onClick={() => sumcantidad()}
                />
              </div>

              <Button
                type="primary"
                disabled={comple.length === len ? false : true}
                style={{ width: "100%", height: 60 }}
                onClick={() => createItemCards(user, restaurant, id, plato)}
              >
                <p
                  style={{
                    paddingTop: 16,
                    fontWeight: 700,
                    fontSize: 16,
                    color: "white",
                  }}
                >
                  Total {trunc(totalFinaly, 2).toFixed(2)}€
                </p>
              </Button>
            </div>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(Complementos);

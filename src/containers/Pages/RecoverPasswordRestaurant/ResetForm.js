import React from "react";
import { Form, Input } from "antd";
import "../RecoverPassword/Forgot.css";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import axios from "axios";

const ResetForm = (props) => {
  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={(values) => {
        const handleSubmit = () => {
          const url = LOCAL_API_URL + "/resetPassword-restaurant";
          axios
            .post(url, {
              password: values.password,
              email: props.email,
              token: props.token,
            })
            .then((res) => {
              window.location.href("https://restaurant.foodyapp.es");
              console.log(res.data);
            })
            .catch((err) => {
              console.log("error:", err);
            });
        };
        handleSubmit();
      }}
    >
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: "Por favor ingrese una contraseña!",
          },
        ]}
        hasFeedback
      >
        <Input.Password className="form-control" placeholder="Contraseña" />
      </Form.Item>

      <Form.Item
        name="confirm"
        dependencies={["password"]}
        hasFeedback
        rules={[
          {
            required: true,
            message: "Por favor consfirme su contraseña!",
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }

              return Promise.reject(
                "Las dos contraseñas que ingresaste no coinciden!"
              );
            },
          }),
        ]}
      >
        <Input.Password className="form-control" placeholder="Contraseña" />
      </Form.Item>

      <Form.Item>
        <button
          shape="round"
          type="primary"
          htmlType="submit"
          className="btn-btn-primary"
        >
          Enviar
        </button>
      </Form.Item>
    </Form>
  );
};

export default ResetForm;

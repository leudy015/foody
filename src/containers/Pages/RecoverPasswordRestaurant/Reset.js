import React, { useEffect, useState } from "react";
import "../Login/Login.css";
import ReserForm from "./ResetForm";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import axios from "axios";
import { withRouter } from "react-router-dom";
const ResetClient = (props) => {
  const [resut, setResult] = useState({
    email: "",
    isValid: false,
    set: false,
  });
  // eslint-disable-next-line
  const parametro = props.match.params;

  useEffect(() => {
    // console.log(this.props.match.params.token);
    const url = LOCAL_API_URL + "/tokenValidation-restaurant";
    axios
      // eslint-disable-next-line
      .post(url, parametro)
      .then((res) => {
        console.log(res.data);
        setResult({
          isValid: res.data.isValid,
          email: res.data.email,
          set: true,
        });
      })
      .catch((err) => {
        console.log("err:", err);
      });
  }, []);

  return (
    <div className="containers">
      <div className="Containerformm">
        {resut.isValid && (
          // eslint-disable-next-line
          <ReserForm email={resut.email} token={props.match.params.token} />
        )}
        {!resut.isValid && resut.set && (
          <p>Token no valido intentalo de nuevo.</p>
        )}
      </div>
    </div>
  );
};

export default withRouter(ResetClient);

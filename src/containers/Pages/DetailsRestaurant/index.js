import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { RESTAURANT_ID, GET_MENU } from "../../GraphQL/query";
import {
  ELIMINAR_RESTAURANT_FAVORITE,
  ANADIR_RESTAURANT_FAVORITE,
} from "../../GraphQL/mutation";
import { Query, useMutation } from "react-apollo";
import {
  Button,
  Breadcrumb,
  Rate,
  Affix,
  message,
  Modal,
  DatePicker,
  Space,
  TimePicker,
  Tag,
} from "antd";
import {
  EnvironmentOutlined,
  HeartOutlined,
  HeartFilled,
  CheckCircleFilled,
  TagOutlined,
  PercentageOutlined,
} from "@ant-design/icons";
import MenuRestaurant from "../../Components/Menu";
import ShoppintCart from "../../Components/ShoppingCart";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import Valoraciones from "../../Components/Opiniones";
import Mapa from "../../Components/Maps";
import "moment/locale/es";
import locale from "antd/es/locale/es_ES";

import "./index.css";

const opciones = [
  {
    id: 1,
    title: "Comer aquí",
    icon: "isv",
    type: "AntDesign",
  },
  {
    id: 2,
    title: "Recoger en 25 min",
    icon: "shopping-bag",
    type: "Feather",
  },
  {
    id: 3,
    title: "Programar recogida",
    icon: "clockcircleo",
    type: "AntDesign",
  },
];

function DetailsRestaurant(props) {
  const [distacia, setDistancia] = useState(1200);
  const [time, setTime] = useState("5 min");
  const [title, setTitle] = useState("");
  const [selected, setSelected] = useState("Recoger en 25 min");
  const [date, setDate] = useState(new Date());
  const [shoModal, setShoModal] = useState(false);
  const [titls, setTitls] = useState("");
  const [contentModal, setcontentModal] = useState(null);
  const [crearFavorito] = useMutation(ANADIR_RESTAURANT_FAVORITE);
  const [eliminarFavorito] = useMutation(ELIMINAR_RESTAURANT_FAVORITE);

  const user = localStorage.getItem("id");

  useEffect(() => {
    document.title = `${title}`;
  }, [title]);

  const id = props.match.params.id;

  function onChangeDate(date, dateString) {
    console.log(date, dateString);
    setDate(dateString);
  }

  function onChangeTime(time, timeString) {
    console.log(timeString);
    setDate(date + "T" + timeString + ".000Z");
  }

  const rec = require("../../Assets/images/cupon.png");
  var latitude = null;
  var longitude = null;

  function getGeoLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          latitude = position.coords.latitude;
          longitude = position.coords.longitude;
        },
        (error) => console.log("geolocation error", error)
      );
    } else {
      console.log("this browser not supported HTML5 geolocation API");
    }
  }

  getGeoLocation();

  const usuario = localStorage.getItem("id");

  const anadirFavorite = (restaurant, id, refetch) => {
    if (!usuario) {
      message.warning("Debes iniciar sesión para añadir a favorito");
      setTimeout(() => {
        props.history.push(`/login?id=${restaurant}`);
      }, 2000);
    } else {
      crearFavorito({ variables: { restaurantID: restaurant, usuarioId: id } })
        .then(() => {
          message.success("Restaurante añadido a la lista de deseos");
          refetch();
        })
        .catch((err) => {
          message.error("Algo salió mal intentalo de nuevo por favot");
          console.log(err);
        });
    }
  };

  const eliminarFavorite = (restaurant, refetch) => {
    eliminarFavorito({ variables: { id: restaurant } })
      .then(() => {
        message.success("Restaurante eliminado a la lista de deseos");
        refetch();
      })
      .catch((err) => {
        message.error("Algo salió mal intentalo de nuevo por favot");
        console.log(err);
      });
  };

  function SetValoraciones(id, title) {
    return (
      <div style={{ padding: 20 }}>
        <Valoraciones id={id} title={title} />
      </div>
    );
  }

  function SetInfo(data) {
    console.log("info", data);
    return (
      <div style={{ padding: 20 }}>
        <Mapa
          lat={data.coordetate}
          lgn={data.coordenateone}
          width="100%"
          height={200}
          title={data.title}
        />
        <div className="info-rest">
          <img src={data.logo} alt={data.title} />
          <div style={{ marginLeft: 10 }}>
            <h2>{data.title}</h2>
            <p>
              {data.categoryName} · {data.type} . A {distacia / 1000} Km ·{" "}
              {time} · pedido minimo {data.minime}€
            </p>
          </div>
        </div>

        <div style={{ marginTop: 30, marginBottom: 40 }}>
          <h3>¿Pregunta sobre alérgenos?</h3>

          <p>
            Pregunta al restaurante por los ingredientes y procesos de cocinado
          </p>
        </div>
        <a href={`tel:${data.phone}`}>Llamar a {data.title}</a>
      </div>
    );
  }

  function setSelectedOpcion() {
    return (
      <div style={{ padding: 20 }}>
        {opciones.map((d, i) => {
          return (
            <ul
              key={i}
              style={{
                listStyle: "none",
                borderBottomWidth: 2,
                borderBottomColor: "#f4f5f5",
              }}
            >
              <li
                style={{
                  display: "flex",
                }}
              >
                <p
                  onClick={() => {
                    setSelected(d.title);
                    setShoModal(false);
                  }}
                  style={{ cursor: "pointer" }}
                >
                  {d.title}
                </p>
                {d.title === selected ? (
                  <CheckCircleFilled
                    style={{
                      marginLeft: "auto",
                      color: "#FFCB40",
                      fontSize: 18,
                    }}
                  />
                ) : null}
              </li>
            </ul>
          );
        })}
      </div>
    );
  }

  const handleModal = (type, id, datas, title) => {
    switch (type) {
      case "Opiniones de clientes":
        setTitls("Opiniones de clientes");
        setcontentModal(SetValoraciones(id, title));
        setShoModal(true);
        break;

      case "Info del restaurante":
        setTitls("Info del restaurante");
        setcontentModal(SetInfo(datas));
        setShoModal(true);
        break;

      case "Selecciona una opción":
        setTitls("Selecciona una opción");
        setcontentModal(setSelectedOpcion);
        setShoModal(true);
        break;

      default:
        break;
    }
  };

  return (
    <Query query={RESTAURANT_ID} variables={{ id: id }}>
      {(response) => {
        if (response.loading) {
          return (
            <>
              <div className="header__contenedor" style={{ paddingTop: 130 }}>
                <div className="uno">
                  <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                    <h1>
                      <Skeleton count={1} />
                    </h1>
                    <p>
                      <Skeleton count={2} width="60%" />
                    </p>
                    <p>
                      <Skeleton count={3} width="95%" />
                    </p>
                    <p>
                      <Skeleton count={2} width="60%" />
                    </p>
                    <p>
                      <Skeleton count={1} width="40%" />
                    </p>
                    <p>
                      <Skeleton count={2} width="70%" />
                    </p>
                  </SkeletonTheme>
                </div>
                <div className="dos">
                  <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                    <Skeleton
                      height={200}
                      width="100%"
                      style={{ marginBottom: 20 }}
                    />
                    <p>
                      <Skeleton count={3} width="100%" />
                    </p>
                  </SkeletonTheme>
                </div>
              </div>
              <div className="body__details">
                <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                  <p>
                    <Skeleton count={3} width="100%" />
                  </p>
                  <p>
                    <Skeleton count={3} width="60%" />
                  </p>
                  <p>
                    <Skeleton count={2} width="40%" />
                  </p>
                </SkeletonTheme>
              </div>
            </>
          );
        }

        if (response) {
          const res =
            response && response.data && response.data.getRestaurantForID
              ? response.data.getRestaurantForID.data
              : [];
          response.refetch();
          setTitle(res.title);
          const SetUbicacion = () => {
            let apiUrlWithParams = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${latitude},${longitude}&destinations=${res.coordetate},${res.coordenateone}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
            fetch(apiUrlWithParams)
              .then((response) => response.json())
              .then((data) => {
                const datos = data.rows[0].elements[0].distance;
                const ti = data.rows[0].elements[0].duration;
                setDistancia(datos.value);
                setTime(ti.text);
              })
              .catch((error) => {
                console.log("error in getting lat, lng: ", error);
              });
          };

          SetUbicacion();

          let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
          res.Valoracion.forEach((start) => {
            if (start.value === 1) rating["1"] += 1;
            else if (start.value === 2) rating["2"] += 1;
            else if (start.value === 3) rating["3"] += 1;
            else if (start.value === 4) rating["4"] += 1;
            else if (start.value === 5) rating["5"] += 1;
          });

          const ar =
            (5 * rating["5"] +
              4 * rating["4"] +
              3 * rating["3"] +
              2 * rating["2"] +
              1 * rating["1"]) /
            res.Valoracion.length;
          let averageRating = 0;
          if (res.Valoracion.length) {
            averageRating = Number(ar.toFixed(1));
          }

          let consideracion = "";
          let color = "#FFCB40";

          switch (averageRating) {
            case 0:
              consideracion = "Sin valoraciones";
              color = "#FFA500";
              break;

            case 1:
            case 1.1:
            case 1.2:
            case 1.3:
            case 1.4:
            case 1.5:
            case 1.5:
            case 1.6:
            case 1.7:
            case 1.8:
            case 1.9:
              consideracion = "Mala";
              color = "#F5365C";
              break;

            case 2:
            case 2.1:
            case 2.2:
            case 2.3:
            case 2.4:
            case 2.5:
            case 2.5:
            case 2.6:
            case 2.7:
            case 2.8:
            case 2.9:
              consideracion = "Regular";
              color = "#FFFF00";
              break;

            case 3:
            case 3.1:
            case 3.2:
            case 3.3:
            case 3.4:
            case 3.5:
            case 3.5:
            case 3.6:
            case 3.7:
            case 3.8:
            case 3.9:
              consideracion = "Buena";
              color = "#1a73e8";
              break;

            case 4:
            case 4.1:
            case 4.2:
            case 4.3:
            case 4.4:
            case 4.5:
            case 4.5:
            case 4.6:
            case 4.7:
            case 4.8:
            case 4.9:
              consideracion = "Exelente";
              color = "#95ca3e";
              break;

            case 5:
              consideracion = "Exelente";
              color = "#95ca3e";
              break;
          }

          return (
            <>
              <div className="container__details">
                <div className="header__container">
                  <div className="header__contenedor">
                    <div className="uno">
                      <Breadcrumb className="pagin">
                        <Breadcrumb.Item>
                          <a href="/" style={{ color: "#ffcb40" }}>
                            Inicio
                          </a>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                          <a href="" style={{ color: "#ffcb40" }}>
                            Categorías
                          </a>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                          <a href="/search/" style={{ color: "#ffcb40" }}>
                            Restaurantes
                          </a>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{res.title}</Breadcrumb.Item>
                      </Breadcrumb>
                      <div className="info__content">
                        <h1>{res.title}</h1>
                        <div
                          style={{
                            color: "gray",
                            fontWeight: 200,
                            display: "flex",
                          }}
                          className="valo"
                        >
                          <div style={{ color: color }}>
                            <Rate
                              disabled
                              defaultValue={averageRating}
                              style={{ color: color }}
                            />{" "}
                            {averageRating} {consideracion} (
                            {res.Valoracion.length}) Valoraciones
                          </div>

                          <Button
                            onClick={() =>
                              handleModal(
                                "Opiniones de clientes",
                                res._id,
                                "",
                                res.title
                              )
                            }
                            type="link"
                            style={{ alignSelf: "center" }}
                          >
                            Ver todas
                          </Button>
                        </div>

                        <div>
                          <p style={{ color: "gray", fontWeight: 200 }}>
                            {res.type} · {res.categoryName} · Abierto hasta{" "}
                            {res.cierre} · {res.minime},00 € mínimo · A{" "}
                            {distacia / 1000} km · {time} · {res.address.calle},{" "}
                            {res.address.numero}, {res.address.ciudad},{" "}
                            {res.address.codigoPostal}
                          </p>
                        </div>

                        <div>
                          <p style={{ color: "gray", fontWeight: 200 }}>
                            {res.description}
                          </p>
                        </div>

                        <div style={{ display: "flex", marginTop: 50 }}>
                          <EnvironmentOutlined style={{ fontSize: 28 }} />
                          <div style={{ marginLeft: 15 }}>
                            <h4 style={{ lineHeight: 0, fontWeight: 700 }}>
                              Info. del restaurante
                            </h4>
                            <Button
                              onClick={() =>
                                handleModal("Info del restaurante", "", res)
                              }
                              type="link"
                              style={{ marginLeft: -16 }}
                            >
                              Alérgenos y mucho más
                            </Button>
                          </div>
                        </div>
                      </div>
                      <div style={{ display: "flex", marginTop: 20 }}>
                        <Tag icon={<TagOutlined />} color="success">
                          Oferta
                        </Tag>
                        <Tag icon={<PercentageOutlined />} color="error">
                          Descuentos especiales
                        </Tag>
                      </div>
                    </div>
                    <div className="dos">
                      <img
                        src={res.image}
                        alt={res.title}
                        className="imagen-rest"
                      />
                      <div className="selec-opcion">
                        <img src={rec} alt="Recoger" className="selec-img" />
                        <div>
                          <h3>{selected}</h3>
                          <p style={{ color: "gray" }}>Burgos</p>
                        </div>
                        <Button
                          onClick={() => handleModal("Selecciona una opción")}
                          type="link"
                          style={{ marginLeft: "auto", alignSelf: "center" }}
                        >
                          Cambiar
                        </Button>
                      </div>
                      {selected === "Programar recogida" ? (
                        <div className="time-selec">
                          <h4>Selecciona una fecha y hora</h4>
                          <Space direction="horizontal">
                            <DatePicker
                              onChange={onChangeDate}
                              locale={locale}
                              placeholder="Selecciona un fecha"
                            />
                            <TimePicker
                              onChange={onChangeTime}
                              locale={locale}
                              placeholder="Selecciona un ahora"
                            />
                          </Space>
                        </div>
                      ) : null}

                      <div className="anadit-to-fav">
                        {res.anadidoFavorito ? (
                          <Button
                            danger
                            onClick={() =>
                              eliminarFavorite(res._id, response.refetch)
                            }
                            icon={<HeartFilled />}
                            type="dashed"
                            style={{ marginTop: 30, width: "100%", height: 60 }}
                          >
                            Eliminar de favoritos
                          </Button>
                        ) : (
                          <Button
                            danger
                            onClick={() =>
                              anadirFavorite(res._id, usuario, response.refetch)
                            }
                            icon={<HeartOutlined />}
                            type="dashed"
                            style={{ marginTop: 30, width: "100%", height: 60 }}
                          >
                            Añadir a favorito
                          </Button>
                        )}
                      </div>
                    </div>
                  </div>
                  <Affix
                    style={{
                      marginTop: 50,
                      marginBottom: 20,
                      backgroundColor: "white",
                    }}
                    offsetTop={75}
                    onChange={(affixed) => console.log(affixed)}
                  >
                    <div className="stiky-contenedor">
                      <div className="stiky-items">
                        <Query query={GET_MENU} variables={{ id: res._id }}>
                          {(response) => {
                            if (response.loading) {
                              return (
                                <SkeletonTheme
                                  color="#E1E9EE"
                                  highlightColor="#F2F8FC"
                                >
                                  <h1>
                                    <Skeleton
                                      count={1}
                                      style={{ borderRadius: 50 }}
                                    />
                                  </h1>
                                </SkeletonTheme>
                              );
                            }

                            if (response) {
                              const menus =
                                response &&
                                response.data &&
                                response.data.getMenu
                                  ? response.data.getMenu.list
                                  : [];

                              return (
                                <>
                                  {menus.map((c, i) => {
                                    return (
                                      <a
                                        key={i}
                                        className="cat_item-smoot"
                                        href={`#${c.title}`}
                                      >
                                        {c.title}
                                      </a>
                                    );
                                  })}
                                </>
                              );
                            }
                          }}
                        </Query>
                      </div>
                    </div>
                  </Affix>
                </div>
                <div className="body__details">
                  <div className="one">
                    <MenuRestaurant id={res._id} />
                  </div>
                  <div className="two">
                    <ShoppintCart
                      id={user}
                      restaurant={id}
                      tipo={selected}
                      date={date}
                      restaurants={res}
                    />
                  </div>
                </div>
              </div>
              <Modal
                title={titls}
                visible={shoModal}
                footer={false}
                onCancel={() => setShoModal(false)}
              >
                {contentModal}
              </Modal>
            </>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(DetailsRestaurant);

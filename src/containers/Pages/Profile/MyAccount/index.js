import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import {
  Form,
  Input,
  message,
  Avatar,
  Tooltip,
  Upload,
  Modal,
  Spin,
} from "antd";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import {
  PlusCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { Mutation, useMutation } from "react-apollo";
import { UPLOAD_FILE, ACTUALIZAR_USUARIO } from "../../../GraphQL/mutation";
import "./index.css";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

function MyAccount(props) {
  const { user, history } = props;
  const [city, setCity] = useState(user.city);
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [avatar, setAvatar] = useState(user.avatar);
  const [loadin, setLoadin] = useState(false);
  const [actualizarUsuario] = useMutation(ACTUALIZAR_USUARIO);

  const chagePhone = () => {
    window.location.href = `/verify-phone/${user._id}`;
  };

  const [form] = Form.useForm();

  useEffect(() => {
    function getGeoLocation() {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;

            setLatitude(latitude);
            setLongitude(longitude);

            let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
            fetch(apiUrlWithParams)
              .then((response) => response.json())
              .then((data) => {
                let cityFound = false;

                for (let index = 0; index < data.results.length; index++) {
                  for (
                    let i = 0;
                    i < data.results[index].address_components.length;
                    i++
                  ) {
                    if (
                      data.results[index].address_components[i].types.includes(
                        "locality"
                      )
                    ) {
                      setCity(
                        data.results[index].address_components[i].long_name
                      );
                      cityFound = true;
                    }

                    if (cityFound) break;
                  }

                  if (cityFound) break;
                }
              })
              .catch((error) => {
                console.log("error in google geocode api: ", error);
              });
          },
          (error) => console.log("geolocation error", error)
        );
      } else {
        console.log("this browser not supported HTML5 geolocation API");
      }
    }

    getGeoLocation();
  }, []);

  const uploadButton = (
    <Tooltip title="Añadir foto del perfil">
      <div>
        <PlusCircleOutlined />
        <div className="ant-upload-text">Añadir foto del perfil</div>
      </div>
    </Tooltip>
  );

  const cerrarSesionUsuario = () => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea cerrar sesión?",
      icon: <ExclamationCircleOutlined />,
      content: "Esperamos verte proto en Foody",
      onOk() {
        localStorage.removeItem("token");
        localStorage.removeItem("id");
        history.push("/login");
      },
      onCancel() {},
    });
  };

  return (
    <>
      {loadin ? (
        <div style={{ textAlign: "center" }}>
          <Spin size="large" />
        </div>
      ) : (
        <div>
          <Mutation mutation={UPLOAD_FILE}>
            {(singleUpload) => (
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                customRequest={async (data) => {
                  let imgBlob = await getBase64(data.file);
                  singleUpload({ variables: { imgBlob } })
                    .then((res) => {
                      setAvatar(
                        res && res.data && res.data.singleUpload
                          ? res.data.singleUpload.filename
                          : ""
                      );
                    })
                    .catch((error) => {
                      console.log("fs error: ", error);
                    });
                }}
              >
                {avatar ? (
                  <Tooltip title="Actualizar foto del perfil">
                    <Avatar
                      src={IMAGES_PATH + avatar}
                      size={95}
                      onClick={() => console.log("hola")}
                    />
                  </Tooltip>
                ) : null}

                {!avatar ? uploadButton : null}
              </Upload>
            )}
          </Mutation>
          <Form
            form={form}
            name="user"
            onFinish={(values) => {
              setLoadin(true);
              const input = {
                _id: user._id,
                name: values.name ? values.name : user.name,
                lastName: values.lastName ? values.lastName : user.lastName,
                email: values.email ? values.email : user.email,
                city: values.city ? values.city : city,
                avatar: avatar,
              };

              actualizarUsuario({ variables: { input: input } })
                .then((res) => {
                  if (res.data.actualizarUsuario) {
                    message.success("Datos actualizo con éxito");
                  }
                })
                .catch((err) => {
                  message.error(
                    "Algo salió mal vuelve a intentaorlo por favor"
                  );
                  console.log(err);
                })
                .finally(() => setLoadin(false));

              console.log(input);
            }}
            scrollToFirstError
          >
            <Form.Item name="name">
              <Input
                placeholder="Nombre"
                className="form-control"
                defaultValue={user.name}
              />
            </Form.Item>
            <Form.Item name="lastName">
              <Input
                placeholder="Apellidos"
                className="form-control"
                defaultValue={user.lastName}
              />
            </Form.Item>
            <Form.Item name="email">
              <Input
                placeholder="Email"
                className="form-control"
                defaultValue={user.email}
              />
            </Form.Item>
            <Form.Item name="city">
              <Input
                placeholder="Ciudad"
                className="form-control"
                defaultValue={city}
              />
            </Form.Item>
            <Form.Item name="telefono">
              <Input
                placeholder="Teléfono"
                className="form-control"
                defaultValue={user.telefono}
                onFocus={() => chagePhone()}
              />
            </Form.Item>
            <Form.Item>
              <button type="submit" className="btn-btn-primary">
                Guardar cambios
              </button>
            </Form.Item>
          </Form>
          <button
            onClick={() => cerrarSesionUsuario()}
            className="btn-btn-primary"
            style={{ color: "#f5365c", marginTop: -50 }}
          >
            Cerrar sesión
          </button>
        </div>
      )}
    </>
  );
}

export default withRouter(MyAccount);

import React, { useState } from "react";
import { Query, useMutation } from "react-apollo";
import { GET_ORDEN } from "../../../GraphQL/query";
import { CREATE_OPINIO, CREAR_VALORACIONES } from "../../../GraphQL/mutation";
import { Modal, Rate, Input, Spin, message, Progress } from "antd";
import moment from "moment";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import SimpleMap from "../../../Components/Maps";
import { withRouter } from "react-router-dom";

import "./index.css";

const Nodata = require("../../../Assets/images/order.png");

const { TextArea } = Input;

function Order(props) {
  const [visible, setVisible] = useState(false);
  const [dataOrder, setDataOrder] = useState(null);
  const [modalOpina, setModalOpina] = useState(false);
  const [dataOpina, setdataOpina] = useState(null);
  const [modalvalorar, setmodalvalorar] = useState(false);
  const [rate, setRate] = useState(0);
  const [rateValorar, setRateValorar] = useState(0);
  const [opinion, setOpinion] = useState("");
  const [comment, setComment] = useState("");
  const [loadinOpi, setLoadinOpi] = useState(false);
  const [loadinVal, setLoadinVal] = useState(false);
  const [createOpinion] = useMutation(CREATE_OPINIO);
  const [crearValoracion] = useMutation(CREAR_VALORACIONES);
  const { res, history } = props;

  const desc = ["Terrible", "Malo", "Normal", "Buena", "Estupenda"];

  const handleChange = (value) => {
    setRate(value);
  };

  const handleChangeValorar = (value) => {
    setRateValorar(value);
  };

  ////////////Detalles

  const showModal = (datas) => {
    setVisible(true);
    setDataOrder(datas);
  };

  const handleCancel = () => {
    setVisible(false);
    setDataOrder(null);
  };

  /////// Opinion

  const showModalOpina = (data) => {
    setModalOpina(true);
    setdataOpina(data);
  };

  const handleCancelOpina = () => {
    setModalOpina(false);
    setdataOpina(null);
  };

  ////////valorar

  const showModalValora = () => {
    setmodalvalorar(true);
  };

  const handleCancelValorar = () => {
    setmodalvalorar(false);
  };

  const valorarRestaurant = (id) => {
    setLoadinVal(true);
    if (rateValorar === 0 && comment === "") {
      message.warning(
        "Debes añadir una puntuación y un comentario para continuar"
      );
      setLoadinVal(false);
    } else {
      const input = {
        user: res._id,
        comment: comment,
        value: rateValorar,
        restaurant: id,
      };

      crearValoracion({ variables: { input: input } })
        .then((res) => {
          console.log(res);
          if (res.data.crearValoracion) {
            message.success("Tu valoración ha sido añadida con éxito");
            handleCancelValorar();
          }
        })
        .catch((error) => {
          console.log(error);
          message.error("Hubo un error intentalo de nuevo por favor");
        })
        .finally(() => setLoadinVal(false));
    }
  };

  const opinarPlato = (id) => {
    setLoadinOpi(true);
    if (rate === 0 && opinion === "") {
      message.warning(
        "Debes añadir una puntuación y un comentario para continuar"
      );
      setLoadinOpi(false);
    } else {
      const input = {
        plato: id,
        comment: opinion,
        rating: rate,
        user: res._id,
      };
      createOpinion({ variables: { input: input } })
        .then((res) => {
          if (res.data.createOpinion.success) {
            message.success("Tu opinión ha sido añadida con éxito");
            handleCancelOpina();
          }
        })
        .catch((error) => {
          console.log(error);
          message.error("Hubo un error intentalo de nuevo por favor");
        })
        .finally(() => setLoadinOpi(false));
    }
  };

  return (
    <Query query={GET_ORDEN} variables={{ usuarios: res._id }}>
      {(response) => {
        if (response.error) {
          return console.log(response.error);
        }
        if (response.loading) {
          return (
            <>
              <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                <div>
                  <Skeleton
                    height={140}
                    width="100%"
                    style={{ borderRadius: 5, marginBottom: 20 }}
                  />
                  <Skeleton
                    height={140}
                    width="100%"
                    style={{ borderRadius: 5, marginBottom: 20 }}
                  />
                  <Skeleton
                    height={140}
                    width="100%"
                    style={{ borderRadius: 5, marginBottom: 20 }}
                  />
                  <Skeleton
                    height={140}
                    width="100%"
                    style={{ borderRadius: 5, marginBottom: 20 }}
                  />
                  <Skeleton
                    height={140}
                    width="100%"
                    style={{ borderRadius: 10, marginBottom: 20 }}
                  />
                </div>
              </SkeletonTheme>
            </>
          );
        }
        if (response) {
          const datos =
            response && response.data
              ? response.data.getOrderByUsuario
              : response.data.getOrderByUsuario.list;

          return (
            <>
              {datos.list.length === 0 ? (
                <div style={{ textAlign: "center" }}>
                  <img src={Nodata} alt="Card" style={{ width: 300 }} />
                  <p style={{ marginTop: -50 }}>
                    Aun no has realizado ningún pedido
                  </p>
                </div>
              ) : (
                <>
                  {datos.list.map((da, i) => {
                    return (
                      <div
                        className={
                          da.status === "active"
                            ? "card_order_active"
                            : "card_order"
                        }
                        key={i}
                      >
                        <img
                          src={da.platos[0].plato.imagen}
                          alt={da.restaurants.title}
                          className="img__orden"
                        />
                        <div className="info_order">
                          {da.platos.map((e, i) => {
                            return (
                              <div key={i}>
                                <h4>
                                  {" "}
                                  <span>{e.plato.cant} × </span>
                                  {e.plato.title}
                                </h4>
                                <p>{da.estado}</p>
                                {e.complementos.map((com, i) => {
                                  return <h6 key={i}>{com}</h6>;
                                })}
                              </div>
                            );
                          })}
                          <button onClick={() => showModal(da)}>
                            Detalles
                          </button>
                          <button
                            onClick={() =>
                              history.push(
                                `/details-restaurant/${da.restaurant}`
                              )
                            }
                          >
                            Repetir
                          </button>
                        </div>
                        <div className="details_order">
                          <p>{da.total}€</p>
                        </div>
                        {da.status != "success" ? (
                          <Progress
                            style={{ marginTop: 15 }}
                            strokeColor={{
                              from: "#ffcb40",
                              to: "#95ca3e",
                            }}
                            percent={Number(da.progreso)}
                            status={da.status}
                          />
                        ) : null}
                      </div>
                    );
                  })}
                </>
              )}

              {dataOrder ? (
                <Modal
                  footer={false}
                  title={dataOrder.restaurants.title}
                  visible={visible}
                  onCancel={handleCancel}
                >
                  <img
                    src={dataOrder.platos[0].plato.imagen}
                    alt={dataOrder.restaurants.title}
                    className="img__details"
                  />

                  <div className="conten-modal-details">
                    <div className="titulos">
                      <h3>Tu foody</h3>
                      <p>
                        {dataOrder.platos.length} producto de{" "}
                        {dataOrder.restaurants.title}
                      </p>
                    </div>

                    <div className="estado">
                      <h3>{dataOrder.estado}</h3>
                      <p>{moment(dataOrder.created_at).format("lll")}</p>
                      <span> ID de foody {dataOrder.id.slice(0, 8)}</span>
                    </div>

                    <div className="titulos">
                      <h3>Recogida</h3>
                      <p>{moment(dataOrder.created_at).format("lll")}</p>
                    </div>

                    {dataOrder.platos.map((h, i) => {
                      return (
                        <div className="card_order" key={i}>
                          <img
                            src={h.plato.imagen}
                            alt="hola"
                            className="img__orden"
                          />
                          <div className="info_order">
                            <div key={i}>
                              <h4>
                                {" "}
                                <span>{h.plato.cant} × </span>
                                {h.plato.title}
                              </h4>
                              <h6>{h.plato.ingredientes}</h6>
                              <p>{h.plato.price}€</p>
                            </div>
                            <button
                              style={{ minWidth: 120 }}
                              onClick={() => showModalOpina(h)}
                            >
                              Opinar
                            </button>
                          </div>
                        </div>
                      );
                    })}

                    {/* Modal para Opinio */}

                    {dataOpina ? (
                      <Modal
                        footer={false}
                        title={dataOpina.plato.title}
                        visible={modalOpina}
                        onCancel={handleCancelOpina}
                      >
                        <SimpleMap
                          lat={dataOrder.restaurants.coordetate}
                          lgn={dataOrder.restaurants.coordenateone}
                          width="100%"
                          height={200}
                          title={dataOrder.restaurants.title}
                        />

                        <div style={{ marginTop: 30 }}>
                          <div className="_plato">
                            <img
                              src={dataOpina.plato.imagen}
                              alt={dataOpina.plato.title}
                            />
                            <div className="_ingredientes">
                              <h2>{dataOpina.plato.title}</h2>
                              <p>{dataOpina.plato.ingredientes}</p>
                            </div>
                          </div>

                          <div
                            style={{
                              padding: 20,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <h3>¿Qué te parecio {dataOpina.plato.title}?</h3>
                            <p>
                              Opina sobre este plato y ayuda a otros clientes
                              como tu a decidir mejor
                            </p>
                            <span style={{ marginBottom: 20 }}>
                              <Rate
                                tooltips={desc}
                                onChange={handleChange}
                                value={rate}
                                style={{ fontSize: 24 }}
                              />
                              {rate ? (
                                <span className="ant-rate-text">
                                  {desc[rate - 1]}
                                </span>
                              ) : (
                                ""
                              )}
                            </span>
                            <TextArea
                              rows={4}
                              onChange={(e) => setOpinion(e.target.value)}
                              style={{ marginBottom: 30 }}
                            />
                            <button
                              className="dejar_op"
                              onClick={() => opinarPlato(dataOpina.plato._id)}
                            >
                              {loadinOpi ? (
                                <Spin size="small" />
                              ) : (
                                "Dejar opinión"
                              )}
                            </button>
                          </div>
                        </div>
                      </Modal>
                    ) : null}

                    {/* Modal para opinion fin */}

                    {/* Modal para valoracion */}

                    <Modal
                      footer={false}
                      title={dataOrder.restaurants.title}
                      visible={modalvalorar}
                      onCancel={handleCancelValorar}
                    >
                      <SimpleMap
                        lat={dataOrder.restaurants.coordetate}
                        lgn={dataOrder.restaurants.coordenateone}
                        width="100%"
                        height={200}
                        title={dataOrder.restaurants.title}
                      />

                      <div style={{ marginTop: 30 }}>
                        <div className="_plato">
                          <img
                            src={dataOrder.restaurants.logo}
                            alt={dataOrder.restaurants.title}
                          />
                          <div className="_ingredientes">
                            <h2>{dataOrder.restaurants.title}</h2>
                            <p>{dataOrder.restaurants.categoryName}</p>
                          </div>
                        </div>

                        <div
                          style={{
                            padding: 20,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <h3>
                            ¿Cuál fue tu experiencia en{" "}
                            {dataOrder.restaurants.title}?
                          </h3>
                          <p>
                            Valora el servicio brindado por el restaurante para
                            ayudar a otros clientes como tu.
                          </p>
                          <span style={{ marginBottom: 20 }}>
                            <Rate
                              tooltips={desc}
                              onChange={handleChangeValorar}
                              value={rateValorar}
                              style={{ fontSize: 24 }}
                            />
                            {rateValorar ? (
                              <span className="ant-rate-text">
                                {desc[rateValorar - 1]}
                              </span>
                            ) : (
                              ""
                            )}
                          </span>
                          <TextArea
                            rows={4}
                            onChange={(e) => setComment(e.target.value)}
                            style={{ marginBottom: 30 }}
                          />
                          <button
                            className="dejar_op"
                            onClick={() => valorarRestaurant(dataOrder.id)}
                          >
                            {loadinVal ? (
                              <Spin size="small" />
                            ) : (
                              "Valorar restaurant"
                            )}
                          </button>
                        </div>
                      </div>
                    </Modal>

                    {/* Modal para valoracion fin */}

                    <div className="titulos" style={{ marginTop: 50 }}>
                      <h3>Resumen</h3>
                      <p>Total: ___________________ {dataOrder.total}€</p>
                      <h6>
                        Este total incluye descuento, propina al restaurante y
                        todo los cargos de transacción.
                      </h6>
                    </div>

                    <button
                      className="valorar"
                      onClick={() => showModalValora()}
                    >
                      Valorar restaurante
                    </button>
                  </div>
                </Modal>
              ) : null}
            </>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(Order);

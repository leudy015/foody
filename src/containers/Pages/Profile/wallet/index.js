import React, { useState, useEffect } from "react";
import { LOCAL_API_URL } from "../../../Utils/UrlConfig";
import "./index.css";
import { message, Popconfirm, Modal } from "antd";
import { DeleteOutlined, PlusCircleOutlined } from "@ant-design/icons";
import CheckoutForm from "./CardAdd";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const visa = require("../../../Assets/images/visa.jpg");
const mastercar = require("../../../Assets/images/master.png");
const americam = require("../../../Assets/images/americam.png");
const orther = require("../../../Assets/images/Card3.png");
const Nodata = require("../../../Assets/images/wallet.png");

export default function Wallet(props) {
  const [cards, setcards] = useState(null);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);

  const showModal = () => {
    setShow(true);
  };

  const hidenModal = () => {
    setShow(false);
  };

  const { user } = props;

  const getCard = async () => {
    setLoading(true);
    let res = await fetch(
      `${LOCAL_API_URL}/get-card?customers=${user.StripeID}`
    );
    const card = await res.json();
    setcards(card);
    if (res) {
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCard();
  }, []);

  const createclient = async (id, name, lastName, email) => {
    let res = await fetch(
      `${LOCAL_API_URL}/create-client?userID=${id}&nameclient=${
        name + lastName
      }&email=${email}`
    );
    const create = await res.json();
    console.log(create);
  };

  if (user.StripeID === null) {
    createclient(user._id, user.name, user.lastName, user.email);
    setLoading(false);
  }

  const deleteCard = async (StripeID, id) => {
    let res = await fetch(
      `${LOCAL_API_URL}/delete-card-web?customers=${StripeID}&cardID=${id}`
    );
    const deletec = await res.json();
    if (deletec ? deletec : false) {
      getCard();
      console.log("Tarjeta eliminada con éxito");
      message.success("Tarjeta eliminada con éxito");
    } else {
      message.error("Algo salio mal, intenlalo de nuevo por favor");
    }
  };

  const tarjetes = cards ? cards.data : [];
  return (
    <>
      <div className="add_card">
        <h3>Añadir tarjeta</h3>
        <PlusCircleOutlined
          onClick={() => showModal()}
          style={{
            marginLeft: "auto",
            color: "#FFCB40",
            fontSize: 28,
            cursor: "pointer",
          }}
        />
        <Modal
          title="Añadir tarjeta al wallet"
          visible={show}
          footer={false}
          onCancel={hidenModal}
        >
          <div className="conten-anadir">
            <CheckoutForm user={user} setShow={setShow} getCard={getCard} />
          </div>
        </Modal>
      </div>

      {loading ? (
        <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
          <div>
            <Skeleton
              height={100}
              width="100%"
              style={{ borderRadius: 5, marginBottom: 20 }}
            />
            <Skeleton
              height={100}
              width="100%"
              style={{ borderRadius: 5, marginBottom: 20 }}
            />
            <Skeleton
              height={100}
              width="100%"
              style={{ borderRadius: 5, marginBottom: 20 }}
            />
            <Skeleton
              height={100}
              width="100%"
              style={{ borderRadius: 5, marginBottom: 20 }}
            />
            <Skeleton
              height={100}
              width="100%"
              style={{ borderRadius: 5, marginBottom: 20 }}
            />
          </div>
        </SkeletonTheme>
      ) : (
        <>
          {tarjetes.length === 0 ? (
            <div style={{ textAlign: "center" }}>
              <img src={Nodata} alt="Card" style={{ width: 300 }} />
              <p style={{ marginTop: -50 }}>
                Aun no has añadido un metódo de pago
              </p>
            </div>
          ) : null}

          {tarjetes.map((ca, i) => {
            let images = "";
            let brand = "";
            switch (ca.card.brand) {
              case "visa":
                images = visa;
                brand = "Visa";
                break;
              case "mastercard":
                images = mastercar;
                brand = "Master Card";
                break;
              case "amex":
                images = americam;
                brand = "American Express";
                break;
              default:
                images = orther;
                brand = "Mi tarjeta";
            }
            return (
              <div className="tarjeta" key={i}>
                <img src={images} alt="Card" />
                <div>
                  <h3>{brand}</h3>
                  <p>**** **** **** {ca.card.last4}</p>
                </div>
                <Popconfirm
                  title="¿Estás seguro que deseas eliminar la tarjeta?"
                  onConfirm={() => deleteCard(ca.customer, ca.id)}
                  onCancel={() => console.log("cancelled")}
                  okText="Eliminar"
                  cancelText="Cancelar"
                >
                  <DeleteOutlined
                    style={{
                      marginLeft: "auto",
                      color: "#F5365C",
                      fontSize: 20,
                      cursor: "pointer",
                    }}
                  />
                </Popconfirm>
              </div>
            );
          })}
        </>
      )}
    </>
  );
}

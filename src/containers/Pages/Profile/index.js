import React, { useEffect } from "react";
import "./index.css";
import { withRouter } from "react-router-dom";
import Order from "./Pedidos";
import Wallet from "./wallet";
import MyAcconut from "./MyAccount";
import { Query, useMutation } from "react-apollo";
import { USER_DETAIL } from "../../GraphQL/query";
import { ELIMINAR_USUARIO } from "../../GraphQL/mutation";
import { MoreOutlined, ExclamationCircleOutlined } from "@ant-design/icons";
import { Dropdown, Menu, message, Modal } from "antd";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const Profile = (props) => {
  const [eliminarUsuario] = useMutation(ELIMINAR_USUARIO);
  const { history } = props;
  useEffect(() => {
    document.title = "Mi cuenta";
  }, []);

  const elimimarAccount = (id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar tu cuenta?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar tu cuenta se borran todos los datos de la misma como tarjetas de crédito pedidos y datos personales.",
      onOk() {
        eliminarCuenta(id);
      },
      onCancel() {},
    });
  };

  const eliminarCuenta = (id) => {
    eliminarUsuario({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarUsuario.success) {
          message.success("Cuenta eliminada con éxito");
          localStorage.removeItem("id");
          localStorage.removeItem("token");
          history.push("/login");
        } else {
          message.error("Algo salió mal vuelve a intentaorlo por favor");
        }
      })
      .catch((err) => {
        console.log(err);
        message.error("Algo salió mal vuelve a intentaorlo por favor");
      });
  };

  return (
    <Query query={USER_DETAIL}>
      {({ data, loading, refetch }) => {
        if (loading) {
          return (
            <div className="account-session" style={{ paddingTop: 200 }}>
              <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                <h1>
                  <Skeleton count={1} />
                </h1>
                <p>
                  <Skeleton count={2} width="60%" />
                </p>
                <p>
                  <Skeleton count={3} width="95%" />
                </p>
                <p>
                  <Skeleton count={2} width="60%" />
                </p>
                <p>
                  <Skeleton count={1} width="40%" />
                </p>
                <p>
                  <Skeleton count={2} width="70%" />
                </p>
              </SkeletonTheme>
            </div>
          );
        }
        if (data) {
          refetch();
          const user = data.getUsuario.data;
          const menu = (
            <Menu>
              <Menu.Item>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="mailto:info@foodyapp.es"
                >
                  Ayuda
                </a>
              </Menu.Item>

              <Menu.Item danger onClick={() => elimimarAccount(user._id)}>
                Eliminar cuenta
              </Menu.Item>
            </Menu>
          );

          return (
            <>
              {loading ? (
                <div>Loading</div>
              ) : (
                <div className="account-container">
                  <div className="account-session">
                    <div className="childrem">
                      <div
                        style={{
                          flexDirection: "row",
                          width: "100%",
                          display: "flex",
                        }}
                      >
                        <h2>Mis datos</h2>
                        <Dropdown overlay={menu}>
                          <MoreOutlined
                            style={{
                              marginLeft: "auto",
                              fontSize: 22,
                              cursor: "pointer",
                            }}
                            onClick={(e) => e.preventDefault()}
                          />
                        </Dropdown>
                      </div>
                      <MyAcconut user={user} />
                    </div>
                    <div className="childrem">
                      <h2>Wallet</h2>
                      <Wallet user={user} />
                    </div>
                  </div>
                  <div className="cont_order">
                    <div className="childrem">
                      <h2>Mis pedidos</h2>
                      <Order res={user} />
                    </div>
                  </div>
                </div>
              )}
            </>
          );
        }
        return null;
      }}
    </Query>
  );
};

export default withRouter(Profile);

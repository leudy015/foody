import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Query } from "react-apollo";
import { CATEGORY, RESTAURANT_SEARCH } from "../../GraphQL/query";
import { Checkbox, Button, Slider, Drawer, Modal, Affix } from "antd";
import { ControlOutlined, CheckCircleFilled } from "@ant-design/icons";
import Mapa from "../../Components/Maps";
import Sliders from "../../Components/SliderCategory";
import Cats from "../../Components/SliderCategory/CatText";
import CardRestaurant from "../../Components/CardRestaurant";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import "./index.css";

const Nodata = require("../../Assets/images/nodata.png");

const opciones = [
  {
    id: 1,
    title: "Comer aquí",
    icon: "isv",
    type: "AntDesign",
  },
  {
    id: 2,
    title: "Recoger en 25 min",
    icon: "shopping-bag",
    type: "Feather",
  },
  {
    id: 3,
    title: "Programar recogida",
    icon: "clockcircleo",
    type: "AntDesign",
  },
];

function Restaurant(props) {
  const { history } = props;
  const busqueda = props.match.params.search;
  const [city, setCity] = useState(null);
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [visible, setVisible] = useState(false);
  const [search, setSearch] = useState(busqueda);
  const [cat, setCat] = useState("");
  const [precio, setPrecio] = useState();
  const [showModal, setShowModal] = useState(false);
  const [selected, setSelected] = useState("Recoger en 25 min");

  function onChange(e) {
    setCat(e.target.value);
  }

  function setPrices(value) {
    setPrecio(value);
  }

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const deleteFilter = () => {
    setSearch("");
    setCat("");
    setPrecio();
  };

  function setSelectedOpcion() {
    return (
      <div style={{ padding: 20 }}>
        {opciones.map((d, i) => {
          return (
            <ul
              key={i}
              style={{
                listStyle: "none",
                borderBottomWidth: 2,
                borderBottomColor: "#f4f5f5",
              }}
            >
              <li
                style={{
                  display: "flex",
                }}
              >
                <p
                  onClick={() => {
                    setSelected(d.title);
                    setShowModal(false);
                  }}
                  style={{ cursor: "pointer" }}
                >
                  {d.title}
                </p>
                {d.title === selected ? (
                  <CheckCircleFilled
                    style={{
                      marginLeft: "auto",
                      color: "#FFCB40",
                      fontSize: 18,
                    }}
                  />
                ) : null}
              </li>
            </ul>
          );
        })}
      </div>
    );
  }

  useEffect(() => {
    function getGeoLocation() {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;

            setLatitude(latitude);
            setLongitude(longitude);

            let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
            fetch(apiUrlWithParams)
              .then((response) => response.json())
              .then((data) => {
                let cityFound = false;

                for (let index = 0; index < data.results.length; index++) {
                  for (
                    let i = 0;
                    i < data.results[index].address_components.length;
                    i++
                  ) {
                    if (
                      data.results[index].address_components[i].types.includes(
                        "locality"
                      )
                    ) {
                      setCity(
                        data.results[index].address_components[i].long_name
                      );
                      cityFound = true;
                    }

                    if (cityFound) break;
                  }

                  if (cityFound) break;
                }
              })
              .catch((error) => {
                console.log("error in google geocode api: ", error);
              });
          },
          (error) => console.log("geolocation error", error)
        );
      } else {
        console.log("this browser not supported HTML5 geolocation API");
      }
    }

    getGeoLocation();
  }, []);

  return (
    <>
      <div className="constainer__restaurant">
        <div className="constainer__restaurant_childern small">
          <div className="selec_meth">
            <Affix offsetTop={90} onChange={(affixed) => console.log(affixed)}>
              <div className="ahora">
                <div>
                  <p>{selected}</p>
                  <h3>{city}</h3>
                </div>
                <Button
                  onClick={() => setShowModal(true)}
                  type="link"
                  style={{ marginLeft: "auto", alignSelf: "center" }}
                >
                  Cambiar
                </Button>
              </div>
            </Affix>
          </div>
          <div className="selec_meth">
            <h4>Precio</h4>
            <Slider defaultValue={5} onChange={setPrices} />
          </div>

          <div className="selec_meth">
            <h4>Cerca de ti</h4>
            <Mapa
              lat={latitude}
              lgn={longitude}
              width="100%"
              height={150}
              title={city}
            />
          </div>

          <Query query={CATEGORY}>
            {(response) => {
              if (response.loading) {
                return null;
              }

              if (response) {
                const cat =
                  response && response.data && response.data.getCategory
                    ? response.data.getCategory.data
                    : [];
                return (
                  <div className="selec_meth">
                    <h4>Categorías</h4>
                    {cat.map((c, i) => {
                      return (
                        <div key={i}>
                          <Checkbox
                            onChange={onChange}
                            value={c._id}
                            style={{ marginBottom: 10, fontSize: 18 }}
                          >
                            {c.title}
                          </Checkbox>
                          <br />
                        </div>
                      );
                    })}
                  </div>
                );
              }
            }}
          </Query>
        </div>

        <div className="constainer__restaurant_childern">
          <div className="nav_bar_searc">
            <div className="search-result">
              <input
                type="text"
                placeholder="Platos, restaurantes o tipos de cocina"
                onChange={(e) => setSearch(e.target.value)}
                defaultValue={search}
              />
              <button onClick={() => history.push(`/search?search=${search}`)}>
                Buscar
              </button>
              <div className="filters">
                <ControlOutlined
                  onClick={showDrawer}
                  style={{
                    fontSize: 20,
                    paddingTop: 18,
                    color: "#ffcb40",
                  }}
                />
              </div>
            </div>
          </div>
          <div className="lis_cat">
            <Cats />
          </div>
          <div className="con-cat">
            <Sliders />
          </div>
          <Query
            query={RESTAURANT_SEARCH}
            variables={{
              search: search,
              city: city,
              category: cat,
              price: precio,
            }}
          >
            {(response) => {
              if (response.loading) {
                return (
                  <div className="container__cards">
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                  </div>
                );
              }

              if (response) {
                const res =
                  response && response.data && response.data.getRestaurantSearch
                    ? response.data.getRestaurantSearch.data
                    : [];
                return (
                  <>
                    <div
                      className={
                        res.length === 0
                          ? "container__cards-all"
                          : "container__cards"
                      }
                    >
                      {res.length === 0 ? (
                        <div className="nodata">
                          <img src={Nodata} alt="No Restaurant" />
                          <h1>
                            No hay restaurantes que coincidan con tus
                            preferencias
                          </h1>
                          <p>Inténtalo con uno o dos filtros menos</p>
                          <button onClick={() => deleteFilter()}>
                            Eliminar filtros
                          </button>
                        </div>
                      ) : null}

                      {res.map((da, i) => {
                        return (
                          <CardRestaurant
                            key={i}
                            res={da}
                            refetch={response.refetch}
                            coordetate={latitude}
                            coordenateone={longitude}
                          />
                        );
                      })}
                    </div>
                  </>
                );
              }
            }}
          </Query>
        </div>
      </div>
      <Modal
        zIndex={200}
        title="Elegir opción"
        visible={showModal}
        footer={false}
        onCancel={() => setShowModal(false)}
      >
        <div style={{ padding: 20 }}>
          <div className="selec_meth">{setSelectedOpcion()}</div>
        </div>
      </Modal>
      <Drawer
        title="Aplicar filtros"
        placement="left"
        width="100%"
        closable={true}
        onClose={onClose}
        visible={visible}
      >
        <div className="constainer__restaurant_childern movil-chil">
          <div className="selec_meth">
            <div className="ahora">
              <div>
                <p>{selected}</p>
                <h3>{city}</h3>
              </div>
              <Button
                onClick={() => {
                  setShowModal(true);
                  setVisible(false);
                }}
                type="link"
                style={{ marginLeft: "auto", alignSelf: "center" }}
              >
                Cambiar
              </Button>
            </div>
          </div>
          <div className="selec_meth">
            <h4>Precio</h4>
            <Slider defaultValue={5} onChange={setPrices} />
          </div>

          <div className="selec_meth">
            <h4>Cerca de ti</h4>
            <Mapa
              lat={latitude}
              lgn={longitude}
              width="100%"
              height={150}
              title={city}
            />
          </div>

          <Query query={CATEGORY}>
            {(response) => {
              if (response.loading) {
                return null;
              }

              if (response) {
                const cat =
                  response && response.data && response.data.getCategory
                    ? response.data.getCategory.data
                    : [];
                return (
                  <div className="selec_meth">
                    <h4>Categorías</h4>
                    {cat.map((c, i) => {
                      return (
                        <div key={i}>
                          <Checkbox
                            onChange={onChange}
                            value={c._id}
                            style={{ marginBottom: 10, fontSize: 18 }}
                          >
                            {c.title}
                          </Checkbox>
                          <br />
                        </div>
                      );
                    })}
                  </div>
                );
              }
            }}
          </Query>
        </div>
      </Drawer>
    </>
  );
}

export default withRouter(Restaurant);

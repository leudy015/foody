import React, { useEffect } from "react";
import { Result, Button } from "antd";
import { withRouter } from "react-router-dom";
import "./index.css";

function Error(props) {
  const { history } = props;

  useEffect(() => {
    document.title = "Error de pago";
  }, []);

  return (
    <div className="result_container">
      <Result
        status="error"
        title="Error con tu método de pago"
        subTitle="Algo fue mal con tu método de pago vuelve a intentarlo por favor"
        extra={[
          <Button type="primary" key="console" onClick={() => history.goBack()}>
            Volver a intentarlo
          </Button>,
        ]}
      />
    </div>
  );
}

export default withRouter(Error);

import React, { useEffect } from "react";
import { Result, Button } from "antd";

import "./index.css";

export default function Nofound() {
  useEffect(() => {
    document.title = "404 Pagina no encontrada";
  }, []);

  return (
    <div className="result_container">
      <Result
        status="404"
        title="404"
        subTitle="Lo sentimos, la página que visitaste no existe."
        extra={
          <Button type="primary" href="/">
            Volver al inicio
          </Button>
        }
      />
    </div>
  );
}

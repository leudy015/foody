import React, { useEffect } from "react";
import "./Forgot.css";
import NormalforgotForm from "./ForgotForm";

function ForgotClient() {
  useEffect(() => {
    document.title = "Recuperar contraseña";
  }, []);

  return (
    <div className="containers">
      <div className="Containerformm">
        <p>Olvidaste tu contraseña!</p>
        <NormalforgotForm />
      </div>
    </div>
  );
}

export default ForgotClient;

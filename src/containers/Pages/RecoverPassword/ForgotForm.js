import React, { useState } from "react";
import { Form, Input, message, Spin } from "antd";
import "./Forgot.css";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { useHistory } from "react-router-dom";

const NormalforgotForm = () => {
  const [loading, setloading] = useState(false);
  const history = useHistory();

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={(value) => {
        setloading(true);
        const handleSubmit = () => {
          const emails = value.email;
          if (emails) {
            fetch(`${LOCAL_API_URL}/forgotpassword?email=${emails}`)
              .then((res) => {
                console.log(res);
                if (res) {
                  setloading(false);
                  message.success("Mensaje enviado ve a tu bandeja de entrada");
                  history.push("/login");
                } else {
                  message.error("hay un problema con tu solicitud");
                  setloading(false);
                }
              })
              .catch((err) => {
                console.log("err:", err);
                setloading(false);
              });
          }
        };
        handleSubmit();
        console.log("dome");
      }}
    >
      {loading ? (
        <Spin style={{ marginBottom: 20 }} />
      ) : (
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Por favor ingresa tu email!" }]}
        >
          <Input placeholder="Email" className="form-control" />
        </Form.Item>
      )}

      <Form.Item>
        <button
          shape="round"
          type="primary"
          htmlType="submit"
          className="btn-res"
        >
          Enviar enlace
        </button>
      </Form.Item>

      <Form.Item>
        ¿Tienes una cuenta?{" "}
        <a style={{ color: "#ffcb40" }} href="/register">
          Regístrate ahora!
        </a>
      </Form.Item>
    </Form>
  );
};

export default NormalforgotForm;

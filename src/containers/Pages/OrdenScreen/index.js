import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import {
  Button,
  Space,
  DatePicker,
  TimePicker,
  Modal,
  Input,
  Switch,
  message,
  Spin,
} from "antd";
import {
  CheckCircleFilled,
  PlusCircleOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import moment from "moment";
import "moment/locale/es";
import locale from "antd/es/locale/es_ES";
import ShoppintCart from "../../Components/ShoppingCart";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { Query, useMutation } from "react-apollo";
import { USER_DETAIL } from "../../GraphQL/query";
import {
  CREAR_MODIFICAR_ORDEN,
  CREATE_NOTIFICATION,
} from "../../GraphQL/mutation";
import CheckoutForm from "../Profile/wallet/CardAdd";

import "./index.css";

const { TextArea } = Input;

const visa = require("../../Assets/images/visa.jpg");
const mastercar = require("../../Assets/images/master.png");
const americam = require("../../Assets/images/americam.png");
const orther = require("../../Assets/images/Card3.png");
const Nodata = require("../../Assets/images/wallet.png");
const paypal = require("../../Assets/images/paypal.png");

const opciones = [
  {
    id: 1,
    title: "Comer aquí",
    icon: "isv",
    type: "AntDesign",
  },
  {
    id: 2,
    title: "Recoger en 25 min",
    icon: "shopping-bag",
    type: "Feather",
  },
  {
    id: 3,
    title: "Programar recogida",
    icon: "clockcircleo",
    type: "AntDesign",
  },
];

function trunc(x, posiciones = 0) {
  var s = x.toString();
  var l = s.length;
  var decimalLength = s.indexOf(".") + 1;
  var numStr = s.substr(0, decimalLength + posiciones);
  return Number(numStr);
}

function OrdenScreen(props) {
  const { history } = props;
  const id = props.match.params.id;
  const options = props.match.params.options;
  const restauran = props.match.params.restaurant;
  const totales = props.match.params.total;
  const extras = props.match.params.extras;

  var option = "";
  switch (options) {
    case "Comer aquí":
      option = "Comer aquí";
      break;
    case "Recoger en 25 min":
      option = "Recoger en 25 min";
      break;
    default:
      option = "Recogída programada";
      break;
  }
  const [selected, setSelected] = useState(option);
  const [date, setDate] = useState(options);
  const [cupon, setCupon] = useState(false);
  const [cards, setcards] = useState(null);
  const [shoModalOpcion, setShoModalOpcion] = useState(false);
  const [show, setShow] = useState(false);
  const [StripeID, setStripeID] = useState(null);
  const [cardNumber, setCardNumber] = useState("");
  const [customer, setCustomer] = useState("");
  const [isTermino, setIsTermino] = useState(false);
  const [propina] = useState(0.5);
  const [dejarpro, setSejarpro] = useState(false);
  const [cubiertos, setCubiertos] = useState(false);
  const [nota, setNota] = useState("");
  const [Loading, setLoading] = useState(false);
  const [ordenID, setOrdenID] = useState(null);
  const [descuento, setDescuento] = useState({
    clave: "",
    descuento: 0,
    tipo: "",
  });

  const [crearModificarOrden] = useMutation(CREAR_MODIFICAR_ORDEN);
  const [createNotification] = useMutation(CREATE_NOTIFICATION);

  const messageTexteNeworden = `Has recibido un nuevo pedido, es hora de prepararlo`;

  const valorDescuento = descuento.descuento > 0 ? descuento.descuento : "";
  const tipo = descuento.tipo ? descuento.tipo : "";
  const subtotal = totales;
  let displayDescuento = `0€`;
  let descuentoFinal = 0;
  let total = trunc(subtotal, totales.length > 2 ? 2 : totales.length);
  if (valorDescuento && tipo) {
    switch (tipo) {
      case "dinero":
        displayDescuento = `${valorDescuento}€`;
        total = subtotal - valorDescuento;
        break;
      case "porcentaje":
        displayDescuento = `${valorDescuento}%`;
        descuentoFinal = valorDescuento / 100;
        total = subtotal - subtotal * descuentoFinal;
        break;
    }
  }

  const totals = dejarpro
    ? Number(total) + Number(propina) + Number(extras)
    : Number(total) + Number(extras);

  const Totalfinaly = trunc(
    totals,
    totales.length > 2 ? 2 : totales.length
  ).toFixed(2);

  const showModal = () => {
    setShow(true);
  };

  const hidenModal = () => {
    setShow(false);
  };

  const rec = require("../../Assets/images/cupon.png");

  useEffect(() => {
    document.title = "Completar pedido";
  }, []);

  const getCard = async () => {
    let res = await fetch(`${LOCAL_API_URL}/get-card?customers=${StripeID}`);
    const card = await res.json();
    setcards(card);
  };

  useEffect(() => {
    getCard();
  }, [StripeID]);

  const createclient = async (id) => {
    let res = await fetch(
      `${LOCAL_API_URL}/create-client?userID=${id}&nameclient=Invitado&email=info@foodyapp.es`
    );
    const create = await res.json();
    console.log(create);
  };

  const deleteCard = async (StripeID, id) => {
    let res = await fetch(
      `${LOCAL_API_URL}/delete-card-web?customers=${StripeID}&cardID=${id}`
    );
    const deletec = await res.json();
    if (deletec ? deletec : false) {
      getCard();
      console.log("Tarjeta eliminada con éxito");
      message.success("Tarjeta eliminada con éxito");
    } else {
      message.error("Algo salio mal, intenlalo de nuevo por favor");
    }
  };

  function onChangeCubiertos(checked) {
    setCubiertos(checked);
  }

  function onChangePropina(checked) {
    setSejarpro(checked);
  }

  function onChangeTerm(checked) {
    setIsTermino(checked);
  }

  function onChangeDate(date, dateString) {
    console.log(date, dateString);
    setDate(dateString);
  }

  function onChangeTime(time, timeString) {
    console.log(timeString);
    setDate(date + "T" + timeString + ".000Z");
  }

  function setSelectedOpcion() {
    return (
      <div style={{ padding: 20 }}>
        {opciones.map((d, i) => {
          return (
            <ul
              key={i}
              style={{
                listStyle: "none",
                borderBottomWidth: 2,
                borderBottomColor: "#f4f5f5",
              }}
            >
              <li
                style={{
                  display: "flex",
                }}
              >
                <p
                  onClick={() => {
                    setSelected(d.title);
                    setShoModalOpcion(false);
                  }}
                  style={{ cursor: "pointer" }}
                >
                  {d.title}
                </p>
                {d.title === selected ? (
                  <CheckCircleFilled
                    style={{
                      marginLeft: "auto",
                      color: "#FFCB40",
                      fontSize: 18,
                    }}
                  />
                ) : null}
              </li>
            </ul>
          );
        })}
      </div>
    );
  }

  const tarjetas = cards ? cards.data : [];

  const SendPushNotificationNeworden = (restaurantnotification) => {
    fetch(
      `${LOCAL_API_URL}/send-push-notification-restaurant?IdOnesignal=${restaurantnotification}&textmessage=${messageTexteNeworden}`
    ).catch((err) => console.log(err));
  };

  const createNotifications = (user, restaurant, usuario, ordenId) => {
    const NotificationInput = {
      user,
      restaurant,
      ordenId,
      usuario,
      type: "new_order",
    };
    console.log(NotificationInput);
    createNotification({
      variables: { input: NotificationInput },
    })
      .then(async (results) => {
        console.log("results", results);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  var time = "";
  switch (selected) {
    case "Comer aquí":
      time = "Comer aquí";
      break;
    case "Recoger en 25 min":
      time = "Recoger en 25 min";
      break;
    default:
      time = date;
      break;
  }

  const Crear_Orden = () => {
    setLoading(true);
    if (!cupon) {
      message.warning("Upps debes añadir un cupón");
      setCupon("");
      return null;
    } else {
      if (!isTermino) {
        setLoading(false);
        message.error(
          "Para continuar debes aceptas los terminos y condiciones"
        );
      } else {
        const input = {
          id: ordenID ? ordenID : null,
          restaurant: restauran,
          time: time,
          nota: nota,
          aceptaTerminos: isTermino,
          userID: id,
          cubiertos: cubiertos,
          propina: dejarpro,
          clave: cupon,
        };
        crearModificarOrden({
          variables: {
            input: input,
          },
        })
          .then(async (results) => {
            setOrdenID(results.data.crearModificarOrden.id);
            console.log("results", results);
            if (results) {
              if (results.data.crearModificarOrden.descuento === null) {
                message.error(
                  "Parece que el cupón no es válido intenta con otro código de descuento"
                );
                setCupon("");
                return null;
              }
              setDescuento(results.data.crearModificarOrden.descuento);
              setCupon("");
              message.success("Cupón añadido con éxito");
              setLoading(false);
            }
          })
          .catch((err) => {
            console.log("err", err);
            setLoading(false);
          });
      }
    }
  };

  const Guardar_Orden = (paypal) => {
    setLoading(true);
    if (!isTermino) {
      setLoading(false);
      message.error("Para continuar debes aceptas los terminos y condiciones");
    } else {
      const input = {
        id: ordenID ? ordenID : null,
        restaurant: restauran,
        time: time,
        nota: nota,
        aceptaTerminos: isTermino,
        userID: id,
        cubiertos: cubiertos,
        propina: dejarpro,
      };
      console.log("input", input);
      crearModificarOrden({
        variables: {
          input: input,
        },
      })
        .then(async (results) => {
          setOrdenID(results.data.crearModificarOrden.id);
          console.log("results", results);
          if (results) {
            setLoading(false);
            if (paypal) {
              window.location.href = `${LOCAL_API_URL}/paypal?price=${Totalfinaly}&order=${results.data.crearModificarOrden.id}&cart=${restauran}`;
            } else {
              let response = await fetch(
                `${LOCAL_API_URL}/payment-existing-card?customers=${customer}&order=${
                  results.data.crearModificarOrden.id
                }&card=${cardNumber}&amount=${
                  Totalfinaly.toFixed(2) * 100
                }&cart=${restauran}&total=${Totalfinaly}`
              );
              const procederalpago = await response.json();
              if (procederalpago.status === "succeeded") {
                setLoading(false);
                SendPushNotificationNeworden(
                  results.data.crearModificarOrden.restaurants.OnesignalID
                );
                createNotifications(
                  restauran,
                  restauran,
                  id,
                  results.data.crearModificarOrden.id
                );
                history.push(
                  `/payment-success/${results.data.crearModificarOrden.id}`
                );
              } else {
                setLoading(false);

                message.error(
                  "Hubo un error con tu método de pago vuelve a intentarlo por favor"
                );
              }
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
          setLoading(false);
          history.push(`/payment-error`);
        });
    }
  };

  return (
    <>
      <div className="container__orden">
        <div className="container__orden_childrem">
          <div className="items_childrem">
            <div style={{ display: "flex" }}>
              <img src={rec} alt="Recoger" className="selec-img" />
              <div style={{ marginLeft: 10 }}>
                <h3>{selected}</h3>
                {selected === "Programar recogida" ? (
                  <p style={{ color: "gray", lineHeight: 0 }}>
                    {selected === "Programar recogida"
                      ? moment(date).format("lll")
                      : "Seleccionar opción"}
                  </p>
                ) : null}

                {selected === "Recogída programada" ? (
                  <p style={{ color: "gray", lineHeight: 0 }}>
                    {selected === "Recogída programada"
                      ? moment(date).format("lll")
                      : "Seleccionar opción"}
                  </p>
                ) : null}
              </div>
              <Button
                onClick={() => setShoModalOpcion(true)}
                type="link"
                style={{ marginLeft: "auto", alignSelf: "center" }}
              >
                Cambiar
              </Button>
            </div>
            {selected === "Programar recogida" ? (
              <div style={{ marginTop: 15 }}>
                <h4>Selecciona una fecha y hora</h4>
                <Space direction="horizontal">
                  <DatePicker
                    onChange={onChangeDate}
                    locale={locale}
                    placeholder="Selecciona un fecha"
                  />
                  <TimePicker
                    onChange={onChangeTime}
                    locale={locale}
                    placeholder="Selecciona un ahora"
                  />
                </Space>
              </div>
            ) : null}
          </div>
          <div className="items_childrem">
            <h3>Añadir nota</h3>
            <TextArea
              rows={4}
              onChange={(e) => setNota(e.target.value)}
              placeholder="Por ejemplo: Sin pepinillo, No dejes quì informacòn relacionada con alergias. Siempre debes ponerte en contacto con el restaurante si tienes alguna alergia. Podrás encontrar informacón del restaurante el la pagina de perfil del mismo"
            />
          </div>
          <div className="items_childrem">
            <h3>¿Necesitas cubiertos?</h3>
            <div style={{ display: "flex" }}>
              <p>
                Ayúdamos a reducir los residuo: pide cubierto solo cuando lo
                necesites.
              </p>
              <Switch
                onChange={onChangeCubiertos}
                style={{ marginLeft: "auto" }}
              />
            </div>
          </div>
          <div className="items_childrem">
            <h3>Articulos</h3>
            <ShoppintCart id={id} restaurant={restauran} fromPedido={true} />
          </div>
          <div className="items_childrem">
            <h3>Apoya los restaurantes locales</h3>
            <div style={{ display: "flex" }}>
              <p>Haz tu compra local y apoya el comercio de tu barrio</p>
            </div>
          </div>
          <div className="items_childrem">
            <h3>¿Dejar propina?</h3>
            <div style={{ display: "flex" }}>
              <p>Dejar 0.50€ de propina en el restaurante.</p>
              <Switch
                onChange={onChangePropina}
                style={{ marginLeft: "auto" }}
              />
            </div>
          </div>
          <div className="items_childrem">
            <h3>Términos y condiciones</h3>
            <div style={{ display: "flex" }}>
              <p>
                Confirmo que he leído y acepto el aviso legal sobre protección
                de datos personales y los términos y condiciones.
              </p>
              <Switch onChange={onChangeTerm} style={{ marginLeft: "auto" }} />
            </div>
          </div>
          <div className="items_childrem">
            <h3>¿Tienes un cupón de decuento?</h3>
            {descuento.descuento ? (
              <div className="cupon_aplied">Cupón aplicado con éxito</div>
            ) : (
              <div style={{ display: "flex" }}>
                <Input
                  placeholder="Añadir cupón"
                  onChange={(e) => setCupon(e.target.value)}
                />
                <Button
                  type="dashed"
                  style={{ marginLeft: 10 }}
                  onClick={() => Crear_Orden()}
                >
                  Aplicar
                </Button>
              </div>
            )}
          </div>
          <div className="items_childrem">
            <h3>Resumen del pedido</h3>
            <div style={{ display: "flex" }}>
              <div>
                <p>Subtotal:</p>
                <p>Descuento:</p>
                <p>Extras:</p>
                <p>Propina:</p>
                <p>Total:</p>
              </div>
              <div style={{ marginLeft: "auto" }}>
                <p>{Number(totales).toFixed(2)}€</p>
                <p>{displayDescuento}</p>
                <p>{Number(extras).toFixed(2)}€</p>
                <p>{dejarpro ? propina.toFixed(2) : (0.0).toFixed(2)}€</p>
                <p>{Totalfinaly}€</p>
              </div>
            </div>
          </div>
          <div className="items_childrem">
            <h3>Método de pago</h3>
            <Query query={USER_DETAIL}>
              {({ data, refetch }) => {
                if (data) {
                  refetch();
                  const user = data.getUsuario.data;
                  setStripeID(user.StripeID);

                  if (user.StripeID === null) {
                    createclient(id);
                  }

                  return (
                    <>
                      <div style={{ display: "flex", marginBottom: 30 }}>
                        <h4>Añadir tarjeta</h4>
                        <PlusCircleOutlined
                          onClick={() => showModal()}
                          style={{
                            marginLeft: "auto",
                            color: "#FFCB40",
                            fontSize: 28,
                            cursor: "pointer",
                          }}
                        />
                      </div>
                      <Modal
                        title="Añadir tarjeta al wallet"
                        visible={show}
                        footer={false}
                        onCancel={hidenModal}
                      >
                        <div className="conten-anadir">
                          <CheckoutForm
                            user={user}
                            setShow={setShow}
                            getCard={getCard}
                          />
                        </div>
                      </Modal>

                      {tarjetas.length === 0 ? (
                        <div style={{ textAlign: "center" }}>
                          <img src={Nodata} alt="Card" style={{ width: 300 }} />
                          <p style={{ marginTop: -50 }}>
                            Aun no has añadido un metódo de pago
                          </p>
                        </div>
                      ) : null}

                      {tarjetas.map((t, i) => {
                        let images = "";
                        let brand = "";
                        switch (t.card.brand) {
                          case "visa":
                            images = visa;
                            brand = "Visa";
                            break;
                          case "mastercard":
                            images = mastercar;
                            brand = "Master Card";
                            break;
                          case "amex":
                            images = americam;
                            brand = "American Express";
                            break;
                          default:
                            images = orther;
                            brand = "Mi tarjeta";
                        }
                        return (
                          <div
                            className="card__payment"
                            key={i}
                            onClick={() => {
                              setCardNumber(t.id);
                              setCustomer(t.customer);
                            }}
                          >
                            <img src={images} alt={brand} />
                            <div
                              style={{
                                marginLeft: 15,
                                alignItems: "center",
                                marginTop: 20,
                              }}
                            >
                              <h3 style={{ lineHeight: 0 }}>
                                **** **** **** {t.card.last4}
                              </h3>
                              <p style={{ lineHeight: 1 }}>{brand}</p>
                            </div>
                            <div style={{ marginLeft: "auto" }}>
                              {t.id === cardNumber ? (
                                <CheckCircleFilled
                                  style={{
                                    color: "#FFCB40",
                                    fontSize: 18,
                                  }}
                                />
                              ) : null}
                              <DeleteOutlined
                                onClick={() => deleteCard(t.customer, t.id)}
                                style={{
                                  color: "#F5365C",
                                  fontSize: 18,
                                  marginLeft: 20,
                                  cursor: "pointer",
                                }}
                              />
                            </div>
                          </div>
                        );
                      })}
                    </>
                  );
                }
                return null;
              }}
            </Query>
          </div>

          <div className="items_childrem">
            <h3>Paypal</h3>
            <div className="card__payment" onClick={() => Guardar_Orden(true)}>
              <img src={paypal} alt="Paypal" />
              <div
                style={{
                  marginLeft: 15,
                  alignItems: "center",
                  marginTop: 20,
                }}
              >
                <h3 style={{ lineHeight: 0 }}>Continua con Paypal</h3>
                <p style={{ lineHeight: 1 }}>Paypal</p>
              </div>
            </div>
          </div>

          <a
            onClick={() => {
              if (cardNumber) {
                Guardar_Orden(false);
              } else {
                message.error(
                  "Debes seleccionar una de tus tarjetas o añadir una"
                );
              }
            }}
            className={cardNumber === "" ? "btn-finaly-inactive" : "btn-finaly"}
          >
            {Loading ? <Spin size="small" /> : "Realizar pedido"}
          </a>
        </div>
      </div>
      <Modal
        title="Selecciona una opción"
        visible={shoModalOpcion}
        footer={false}
        onCancel={() => setShoModalOpcion(false)}
      >
        {setSelectedOpcion()}
      </Modal>
    </>
  );
}

export default withRouter(OrdenScreen);

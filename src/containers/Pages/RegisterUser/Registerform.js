import React from "react";
import { Form, Input, Checkbox, message } from "antd";
import { NUEVO_USUARIO } from "../../GraphQL/mutation";
import { Mutation } from "react-apollo";
import { useHistory } from "react-router-dom";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import LoginForm from "../Login/Loginform";

const RegistrationForm = () => {
  const history = useHistory();
  const [form] = Form.useForm();
  return (
    <Mutation mutation={NUEVO_USUARIO}>
      {(crearUsuario) => {
        return (
          <Form
            form={form}
            name="register"
            onFinish={(values) => {
              const handleSubmit = async () => {
                await crearUsuario({
                  variables: {
                    input: {
                      name: values.name,
                      lastName: values.lastName,
                      email: values.email,
                      password: values.password,
                    },
                  },
                }).then(async ({ data: res }) => {
                  if (res && res.crearUsuario && res.crearUsuario.success) {
                    message.success(res.crearUsuario.message);
                    const datss =
                      res && res.crearUsuario && res.crearUsuario.data;
                    history.push(`/verify-phone/${datss._id}`);
                    if (datss) {
                      const createclient = async () => {
                        let res = await fetch(
                          LOCAL_API_URL +
                            `/create-client?userID=${datss._id}&nameclient=${
                              datss.name + datss.lastName
                            }&email=${datss.email}`
                        );
                        const deletec = await res.json();
                        console.log(deletec);
                      };
                      createclient();
                    } else {
                      return null;
                    }
                  } else if (
                    res &&
                    res.crearUsuario &&
                    !res.crearUsuario.success
                  )
                    message.error(res.crearUsuario.message);
                });
              };
              handleSubmit();
            }}
            scrollToFirstError
          >
            <Form.Item
              name="name"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese su nombre!",
                },
              ]}
            >
              <Input placeholder="Nombre" className="form-control" />
            </Form.Item>

            <Form.Item
              name="lastName"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese su apellidos!",
                },
              ]}
            >
              <Input placeholder="Apellidos" className="form-control" />
            </Form.Item>

            <Form.Item
              name="email"
              rules={[
                {
                  type: "email",
                  message: "Email incorrecto!",
                },
                {
                  required: true,
                  message: "Por favor ingrese su Email!",
                },
              ]}
            >
              <Input placeholder="Email" className="form-control" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese una contraseña!",
                },
              ]}
              hasFeedback
            >
              <Input.Password
                placeholder="Contraseña"
                className="form-control"
              />
            </Form.Item>

            <Form.Item
              name="confirm"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Por favor consfirme su contraseña!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      "Las dos contraseñas que ingresaste no coinciden!"
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                placeholder="Contraseña"
                className="form-control"
              />
            </Form.Item>

            <Form.Item
              name="agreement"
              valuePropName="checked"
              rules={[
                {
                  validator: (_, value) =>
                    value
                      ? Promise.resolve()
                      : Promise.reject(
                          "Por favor aceptes los términos y condiciones"
                        ),
                },
              ]}
            >
              <Checkbox>
                Acepto los términos y condiciones de Foody Pick Up{" "}
                <a style={{ color: "#ffcb40" }} href="/condiciones-de-uso">
                  Téminos y condiciones
                </a>
              </Checkbox>
            </Form.Item>
            <Form.Item>
              <button
                type="submit"
                className="btn-btn-primary"
                htmlType="submit"
              >
                ¡Regístrarme ahora!
              </button>
              <Form.Item>
                ¿tienes una cuenta?{" "}
                <a style={{ color: "#ffcb40" }} href="/login">
                  Inicia sesión ahora!
                </a>
              </Form.Item>
            </Form.Item>
            <LoginForm fromSignup={true} />
          </Form>
        );
      }}
    </Mutation>
  );
};

export default RegistrationForm;

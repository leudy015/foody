import React, { useEffect } from "react";
import "./Register.css";
import RegistrationForm from "./Registerform";

function RegisterClient() {
  useEffect(() => {
    document.title = "Regístrarme";
  }, []);

  return (
    <div className="containers">
      <div className="ContainerformRegister">
        <p>Bienvenido a Foody!</p>
        <RegistrationForm />
      </div>
    </div>
  );
}

export default RegisterClient;

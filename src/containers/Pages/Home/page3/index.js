import React from "react";

import "./index.css";

const img1 = require("./img1.jpg");
const img2 = require("./vegan.jpg");
const img3 = require("./im3.jpg");

export default function ContainerCard(props) {
  const { ciudad } = props;
  return (
    <div className="ContainerCard">
      <div className="ContainerCard_page3">
        <h1 className="afiction_conatiner_title">
          Las tendencias hoy en {ciudad ? ciudad : "tu ciudad"}
        </h1>
        <div className="Features">
          <div className="FeatureContainer">
            <img src={img1} alt="image1" />

            <div className="FeatureText">
              <h3>Hamburguesas: ¿Por qué triunfan?</h3>
              <p>
                Es raro encontrar a alguien a quien no le guste la hamburguesa.
                Es más, es el plato favorito de medio mundo, o al menos, se
                encuentra en el top 3 de comidas preferidas.
              </p>
            </div>
          </div>

          <div className="FeatureContainer">
            <img src={img2} alt="image1" />
            <div className="FeatureText">
              <h3>Mac and cheese</h3>
              <p>
                No todo en la cocina estadounidense son hamburguesas y
                sandwiches, uno de sus más clásicos platos es el Mac and cheese,
                un pudding de macarrones y queso tremendamente popular por
                aquellas tierras.
              </p>
            </div>
          </div>

          <div className="FeatureContainer">
            <img src={img3} alt="image1" />

            <div className="FeatureText">
              <h3>Cocido madrileño</h3>
              <p>
                Aunque hay varias modalidades de cocido, el conocido como
                madrileño tiene una forma tradicional a la hora de comerlo:
                empezar por una sabrosa sopa con fideos, luego garbanzos y
                verduras y acabar con carnes y embutidos.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

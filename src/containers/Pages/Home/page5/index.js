import React from "react";

import "./index.css";

export default function CardContact() {
  return (
    <div className="card_contact_container">
      <div className="card_contact_container_childerm">
        <h1 className="afiction_conatiner_title">Se parte de Foody®</h1>
        <div className="card_contact_container_items">
          <div className="card_contact_container_items_content">
            <img
              src="https://image.freepik.com/foto-gratis/repartidora-mascarilla-protectora-guantes-bolsa-comida-cafe-abriendo-puerta-al-edificio_114561-886.jpg"
              alt=""
            />
            <div className="info_container_card">
              <h1>Repartidores</h1>
              <h4>Total libertad</h4>
              <p>
                Muy pronto tendremos disponible nuestro servicio de reparto a
                domicilio, que espera para unirte a esta revolución.
              </p>
              <a>Muy pronto</a>
            </div>
          </div>
          <div className="card_contact_container_items_content">
            <img
              src="https://image.freepik.com/foto-gratis/cocinero-haciendo-cena-cocina-restaurante-alta-gama_110955-325.jpg"
              alt=""
            />
            <div className="info_container_card">
              <h1>Únete a nosotro</h1>
              <h4>Atrae nuevos clientes</h4>
              <p>
                Ponemos todos nuestros recursos y medios a tu servicio para
                ayudarte a reducir costes e impulsar tu negocio
              </p>
              <a href="/restaurant">Asociate a Foody®</a>
            </div>
          </div>
          <div className="card_contact_container_items_content">
            <img
              src="https://image.freepik.com/foto-gratis/mujer-gerente-dirigiendo-reunion-intercambio-ideas_58466-11714.jpg"
              alt=""
            />
            <div className="info_container_card">
              <h1>Se parte del equipo</h1>
              <h4>Ayudanos a crear esta realidad</h4>
              <p>
                Únete a nuestro equipo y empecemos a construir la mejor
                herramienta para digitalizar los restaurantes del mundo.
              </p>
              <a href="/team">Únete a Foody®</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

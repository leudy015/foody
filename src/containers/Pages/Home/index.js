import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import CookieConsent from "react-cookie-consent";
import "./index.css";

import Page1 from "./page1";
import Page2 from "./page2";
import Page3 from "./page3";
import Page4 from "./page4";
import Page5 from "./page5";

const slider = require("../../Assets/images/landing.png");

const Home = (props) => {
  const [city, setCity] = useState(null);
  const [search, setSearch] = useState("");
  const { history } = props;

  useEffect(() => {
    function getGeoLocation() {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;
            let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;

            fetch(apiUrlWithParams)
              .then((response) => response.json())
              .then((data) => {
                let cityFound = false;
                for (let index = 0; index < data.results.length; index++) {
                  for (
                    let i = 0;
                    i < data.results[index].address_components.length;
                    i++
                  ) {
                    if (
                      data.results[index].address_components[i].types.includes(
                        "locality"
                      )
                    ) {
                      setCity(
                        data.results[index].address_components[i].long_name
                      );
                      cityFound = true;
                    }

                    if (cityFound) break;
                  }

                  if (cityFound) break;
                }
              })
              .catch((error) => {
                console.log("error in google geocode api: ", error);
              });
          },
          (error) => console.log("geolocation error", error)
        );
      } else {
        console.log("this browser not supported HTML5 geolocation API");
      }
    }
    getGeoLocation();
  }, [city]);

  return (
    <div className="Constiner-home">
      <div className="Barner-home-full">
        <div className="Barner-home">
          <div>
            <div className="search">
              <h1>{`Los mejores restaurantes de ${
                city ? city : "tu ciudad"
              }`}</h1>
              <p>
                Haz tu pedido con la app y recógelo en minutos en los
                restaurantes de tu ciudad.
              </p>
              <input
                type="text"
                placeholder="Platos, restaurantes o tipos de cocina"
                onChange={(e) => setSearch(e.target.value)}
              />
              <button onClick={() => history.push(`/search/${search}`)}>
                Buscar
              </button>
            </div>
          </div>
          <div>
            <img src={slider} alt="Foody Pick Up" />
          </div>
        </div>
      </div>
      <Page2 />
      <Page3 ciudad={city} />
      <Page1 />
      <Page4 />
      <Page5 />
      <CookieConsent
        location="bottom"
        buttonText="Aceptar"
        cookieName="myAwesomeCookieName2"
        style={{
          background: "#eeeeee",
          color: "black",
        }}
        buttonStyle={{
          color: "white",
          fontSize: "14px",
          backgroundColor: "#ffcb40",
          borderRadius: 5,
          width: 150,
          outline: "none",
        }}
        expires={150}
      >
        Este sitio web utiliza cookies para garantizar que obtenga la mejor
        experiencia en nuestro sitio web.{" "}
        <span style={{ fontSize: "10px" }}>
          Más detalles en https://foodyapp.es/cookies
        </span>
      </CookieConsent>
    </div>
  );
};

export default withRouter(Home);

import React from "react";
import { ArrowRightOutlined } from "@ant-design/icons";

import "./index.css";

const burguer = require("./2882855.jpg");
const pizza = require("./pizza.jpg");
const vegan = require("./vegan.jpg");
const fruta = require("./fruta.jpg");
const sushi = require("./sushi.jpg");
const pollo = require("./pollo.jpg");
const pastas = require("./pastas.jpg");
const mexico = require("./mexico.jpg");
const share = require("./share.jpg");

export default function Afiction() {
  return (
    <div className="afiction_conatiner">
      <h1 className="afiction_conatiner_title">Los platos más populares</h1>
      <div className="afiction_item">
        <div>
          <img src={burguer} alt="Delicius" />
          <h1>Burger</h1>
          <p>
            Si amas las hamburguesas, préparate para adorar este listado.
            Escogimos las más ricas, jugosas y delirantes.
          </p>
          <a href="/search/burger">
            Ver Burgers deliciosas {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
        <div>
          <img src={pizza} alt="Delicius" />
          <h1>Pizza</h1>
          <p>
            Las mejores recetas de Pizza. La pizza consta de una masa fina con
            base de harina la cual se cubre con salsa de tomate.
          </p>
          <a href="/search/pizza">
            Ver La mejor pizza {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>

      <div className="afiction_item_left">
        <div>
          <img src={vegan} alt="Delicius" />
          <h1>Vegano</h1>
          <p>
            ¿No sabes qué comer esta semana? Aquí tienes una propuesta de
            desayunos, comidas y cenas para seguir una dieta saludable.
          </p>
          <a href="/search/vegano">
            Ver Comida saludable {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
        <div>
          <img src={fruta} alt="Delicius" />
          <h1>Frutas</h1>
          <p>Compra fruta y verdura con envío gratuito a domicilio.</p>
          <a href="/search/frutas">
            Ver Frutas y verduras {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>

      <div className="afiction_item">
        <div>
          <img src={sushi} alt="Delicius" />
          <h1>Sushi</h1>
          <p>
            Disfruta con platos de comida japonesa elaborados artesanalmente y
            preparados al momento.
          </p>
          <a href="/search/sushi">
            Ver Sushi esquisitos {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
        <div>
          <img src={pollo} alt="Delicius" />
          <h1>Pollo</h1>
          <p>
            Disfruta nuestro pollos crujiente en los mejores restaurantes de tu
            ciudad o pídelo ahora a domicilio.
          </p>
          <a href="/search/pollo">
            Ver Pollo crujiente {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>

      <div className="afiction_item_left">
        <div>
          <img src={pastas} alt="Delicius" />
          <h1>Pastas</h1>
          <p>
            Si hay algo de lo que pueden presumir los italianos es de su
            gastronomía y su buena cocina. Su pasta fresca es una delicia
          </p>
          <a href="/search/italiano">
            Ver Pastas Italiana {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
        <div>
          <img src={mexico} alt="Delicius" />
          <h1>Mexicanos</h1>
          <p>Las comida mexicana, tan ricas en contrastes y llenas de color.</p>
          <a href="/search/mexicano">
            Ver Mexicanos exclusivos {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>

      <div className="afiction_item_left_full">
        <div>
          <img src={share} alt="Delicius" />
          <h1>Para compartir</h1>
          <p>Los mejores pack para compartir con tus amigo.</p>
          <a href="/search/burger">
            Ver Para compartir {""}{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>
    </div>
  );
}

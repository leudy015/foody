import React from "react";
import "./index.css";
import {
  ClockCircleTwoTone,
  ShopTwoTone,
  ArrowRightOutlined,
  ShoppingTwoTone,
} from "@ant-design/icons";

const Page1 = () => {
  return (
    <div className="container-page1" id="ventajas">
      <div className="sub-container-page1">
        <h1 className="afiction_conatiner_title">Descubre nuestras ventajas</h1>
      </div>
      <div className="constainer-box">
        <div className="card-items">
          <div className="icon-sec">
            <ShopTwoTone twoToneColor="#ffcb40" style={{ fontSize: 25 }} />
          </div>
          <p>
            Se acabaron las esperas al comer, descubre restaurantes cercanos,
            pide con antelación y paga desde el móvil sin esperas.{" "}
          </p>
          <a href="/search/" className="link-footer">
            Descubrir restaurantes{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>

        <div className="card-items">
          <div className="icon-sec">
            <ShoppingTwoTone twoToneColor="#ffcb40" style={{ fontSize: 25 }} />
          </div>
          <p>
            Otra de las ventajas que puedes disfrutar con nuestra herramienta,
            es el poder realizar pedido para pasar a recoger ahorrando tiempo y
            dinero con nuestras ofertas exclusivas.
          </p>
          <a href="/search/" className="link-footer">
            Descubrir restaurantes{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>

        <div className="card-items">
          <div className="icon-sec">
            <ClockCircleTwoTone
              twoToneColor="#ffcb40"
              style={{ fontSize: 25 }}
            />
          </div>
          <p>
            Programa pedidos desde la app de Foody® y olvidate del resto solo
            pasa a recojer a la hora seleccionada.
          </p>
          <a href="/search/" className="link-footer">
            Descubrir restaurantes{" "}
            <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Page1;

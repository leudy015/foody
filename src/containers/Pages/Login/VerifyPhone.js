import React, { useState } from "react";
import "./Login.css";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import es from "react-phone-input-2/lang/es.json";
import { message } from "antd";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { useHistory, withRouter } from "react-router-dom";

function VerifyPhone(props) {
  const [value, setValue] = useState();
  const history = useHistory();
  const id = props.match.params.id;
  const prof = props.match.params.prof;

  //props.match.params.prof;

  const SendTextSMS = () => {
    if (value) {
      fetch(`${LOCAL_API_URL}/verify-phone?phone=${value}`)
        .then((res) => {
          console.log("res:  ", res);
          if (res.ok) {
            message.success("Código enviado con éxito");
            setValue("");
            if (prof) {
              history.push(`/details-professionals/${prof}`);
            } else {
              history.push(`/send-code/${id}/${value}`);
            }
          } else {
            message.warning(" Uups verifica tu número móvil");
          }
        })
        .catch((err) => {
          setValue("");
          console.log("err:  ", err);
          message.error(
            "Hubo un error con su solicitud intentelo de nuevo por favor"
          );
        });
    } else {
      message.warning("Debes añadir un número móvil para enviarte el código");
    }
  };

  return (
    <div className="containers">
      <div className="Phone">
        <p className="psol">Verifica tu teléfono</p>
        <div className="phoneinput">
          <PhoneInput
            placeholder="Ingresa tu número móvil"
            inputStyle={{
              borderWidth: 0,
              backgroundColor: "transparent",
              maxWidth: 500,
              width: "100%",
            }}
            dropdownStyle={{ borderWidth: 0 }}
            buttonStyle={{
              borderWidth: 0,
              backgroundColor: "#fafafa",
            }}
            country="es"
            localization={es}
            value={value}
            onChange={(phone) => setValue(phone)}
          />
          <a
            href
            className={value ? "btm-active" : "btm"}
            onClick={() => SendTextSMS()}
          >
            Enviar código
          </a>
        </div>
      </div>
    </div>
  );
}

export default withRouter(VerifyPhone);

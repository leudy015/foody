import React, { Component } from "react";
import "./socialLogin.css";
import { Redirect, withRouter } from "react-router-dom";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { GoogleLogin } from "react-google-login";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  LOCAL_API_URL,
} from "../../Utils/UrlConfig";
import { FacebookFilled, GoogleOutlined } from "@ant-design/icons";
import AppleLogin from "react-apple-login";
import { message } from "antd";

class ScocialLogin extends Component {
  state = {
    isAuthenticated: false,
    user: null,
    token: "",
  };

  onFailure = (error) => {
    console.log(error);
  };

  facebookResponse = (response) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    fetch(`${LOCAL_API_URL}/api/v1/auth/facebook`, options).then((r) => {
      //const token = r.headers.get("x-auth-token");
      r.json().then(async (user) => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  googleResponse = (response) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    fetch(`${LOCAL_API_URL}/api/v1/auth/google`, options).then((r) => {
      console.log(r);
      r.json().then((user) => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  appleResponse = async (response) => {
    message.warning(
      "Algo va mal con apple utiliza otro método para iniciar sesión"
    );
    /* console.log("respuesta de apple", response);
    const postData = {
      firstName: "Leudy",
      lastName: "Martes",
      email: "leooo@gmail.com",
      token: response.code,
    };

    await fetch(`${LOCAL_API_URL}/api/v1/auth/social/mobile`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postData),
    })
      .then(async (res) => {
        const data = await res.json();
        this.onLoginWithApple(data.nuevoUsuario.email, data.token);
        console.log("respuesta del servidor", data);
      })
      .catch((err) => {
        console.log(err);
        message.error(
          "Hubo un problema con su solicitud vuelve a intentarlo por favor"
        );
      }); */
  };

  onLoginWithApple = async (email, password) => {
    const input = {
      email,
      password,
    };
    let res = await fetch(`${LOCAL_API_URL}/api/user/auth`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(input),
    });
    if (res) {
      const user = await res.json();
      localStorage.setItem("token", user.data.token);
      localStorage.setItem("id", user.data.id);
      this.props.history.push("/profile");
    } else {
      message.error("Hubo un problema interno con el sistema");
    }
  };

  render() {
    if (this.state.isAuthenticated) {
      return <Redirect to="/profile" />;
    } else
      return (
        <div className="contLogin1">
          <div
            style={{
              marginBottom: 15,
              cursor: "pointer",
              backgroundColor: "black",
              width: "100%",
              borderRadius: 10,
            }}
          >
            <AppleLogin
              clientId="foody.web"
              redirectURI="https://foodyapp.es/login"
              scope="name email"
              designProp={{
                height: 50,
                width: 235,
                locale: "es_ES",
              }}
              callback={this.appleResponse}
            />
          </div>
          <FacebookLogin
            appId={FACEBOOK_APP_ID}
            autoLoad={false}
            fields="name,email,picture"
            callback={this.facebookResponse}
            render={(renderProps) => (
              <div className="socia" onClick={renderProps.onClick}>
                <button className="btn-twitter">
                  <FacebookFilled style={{ marginRight: 10, marginTop: 5 }} />
                  <span> Continua con facebook</span>
                </button>
              </div>
            )}
          />
          <GoogleLogin
            clientId={GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={this.googleResponse}
            onFailure={this.onFailure}
            render={(renderProps) => (
              <div
                className="socia"
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                <button className="btn-twitter">
                  <GoogleOutlined style={{ marginRight: 10, marginTop: 5 }} />
                  <span> Continua con Google</span>
                </button>
              </div>
            )}
          />
        </div>
      );
  }
}

export default withRouter(ScocialLogin);

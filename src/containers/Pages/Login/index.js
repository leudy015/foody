import React, { useEffect } from "react";
import NormalLoginForm from "./Loginform";
import "./Login.css";

function LoginClient() {
  useEffect(() => {
    document.title = "Iniciar sesión";
  }, []);

  return (
    <div className="containers">
      <div className="Containerform">
        <NormalLoginForm />
      </div>
    </div>
  );
}

export default LoginClient;

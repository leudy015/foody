import React, { useState } from "react";
import "react-phone-input-2/lib/style.css";
import { message } from "antd";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { useHistory, withRouter } from "react-router-dom";
import ReactCodeInput from "./react-verification-code-input";

import "./Login.css";

function SendCode(props) {
  const [value, setValue] = useState("");

  const history = useHistory();

  const phone = props.match.params.phone;
  const id = props.match.params.id;

  const SendTextSMS = () => {
    if (value) {
      fetch(
        `${LOCAL_API_URL}/verify-code?phone=${phone}&code=${value}&id=${id}`
      )
        .then((res) => {
          console.log("res:  ", res);
          if (res.ok) {
            message.success("Teléfono verificado con éxito");
            setValue("");
            history.push(`/login`);
          } else {
            message.warning(" Uups verifica tu número móvil");
          }
        })
        .catch((err) => {
          setValue("");
          console.log("err:  ", err);
          message.error(
            "Hubo un error con su solicitud intentelo de nuevo por favor"
          );
        });
    } else {
      message.warning(
        "Debes introducir el código que te hemos enviado al teléfono"
      );
    }
  };

  return (
    <div className="containers">
      <div className="Phone">
        <p className="psol">
          Introduce el código de verificación enviado al teléfono {""} +{phone}.
        </p>
        <div className="phoneinput">
          <ReactCodeInput
            fields={4}
            fieldWidth={40}
            fieldHeight={40}
            className="inpu"
            onChange={(values) => setValue(values)}
          />
          <a
            href
            className={value ? "btm-active" : "btm"}
            onClick={() => SendTextSMS()}
          >
            Verificar
          </a>
        </div>
      </div>
    </div>
  );
}

export default withRouter(SendCode);

import React, { Component, Fragment } from "react";
import { Input, Checkbox, message, Spin } from "antd";
import "./Login.css";
import { AUTENTICAR_USUARIO } from "../../GraphQL/mutation";
import { Mutation } from "react-apollo";
import { withRouter } from "react-router-dom";
import SocialLogin from "./socialLogin";

const initialState = {
  email: "",
  password: "",
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
    };
  }

  actualizarState = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  limpiarState = () => {
    this.setState({ ...initialState });
  };

  onCompletedLogin = async (res) => {
    console.log("res: ", res);
    if (!res.autenticarUsuario.success) {
      message.error(res.autenticarUsuario.message);
    } else {
      localStorage.setItem("token", res.autenticarUsuario.data.token);
      localStorage.setItem("id", res.autenticarUsuario.data.id);

      await this.props.refetch();

      // Limpiar state
      this.limpiarState();
      this.setState({ id: res.autenticarUsuario.data.id });

      const params = new URLSearchParams(this.props.location.search);
      const ProfID = params.get("ProfID");
      if (res.autenticarUsuario.data.verifyPhone) {
        if (ProfID) {
          message.success(res.autenticarUsuario.message);
          this.props.history.push(`/details-restaurant/${ProfID}`);
        } else {
          message.success(res.autenticarUsuario.message);
          this.props.history.push("/profile");
        }
      } else {
        if (ProfID) {
          this.props.history.push(
            `/verify-phone/${res.autenticarUsuario.data.id}/${ProfID}`
          );
        }
        this.props.history.push(
          `/verify-phone/${res.autenticarUsuario.data.id}`
        );
      }
    }
  };

  iniciarSesion = (e, usuarioAutenticar) => {
    e.preventDefault();

    usuarioAutenticar().then(async ({ data: res }) => {
      if (res.autenticarUsuario.success) {
        localStorage.setItem("token", res.autenticarUsuario.data.token);
        localStorage.setItem("id", res.autenticarUsuario.data.id);

        // Limpiar state
        this.limpiarState();

        // Redireccionar el contenido
        const params = new URLSearchParams(this.props.location.search);
        const ProfID = params.get("id");
        if (ProfID) {
          message.success(res.autenticarUsuario.message);
          this.props.history.push(`/details-restaurant/${ProfID}`);
        } else {
          message.success(res.autenticarUsuario.message);
          this.props.history.push("/profile");
        }
      } else {
        message.error(res.autenticarUsuario.message);
      }
    });
  };

  logon = async (email, password) => {
    this.setState(
      {
        email: email,
        password: password,
      },
      () => {
        document.getElementById("loginSubmit").click();
      }
    );
  };
  validarForm = () => {
    const { email, password } = this.state;

    const noValido = !email || !password;
    console.log(noValido);
    return noValido;
  };

  render() {
    console.log(this.props);
    const { email, password } = this.state;

    return (
      <div className={this.props.fromSignup ? "" : "register-page-content"}>
        <Fragment>
          <Mutation
            mutation={AUTENTICAR_USUARIO}
            variables={{ email, password }}
          >
            {(usuarioAutenticar, { loading, error, data }) => {
              if (loading) {
                return (
                  <div className={this.props.fromSignup ? "" : "page-loader"}>
                    <Spin size={this.props.fromSignup ? "" : "large"}></Spin>
                  </div>
                );
              }

              return (
                <div className={this.props.fromSignup ? "" : "containerLogin"}>
                  <div className={this.props.fromSignup ? "" : "contLogin"}>
                    <form
                      onSubmit={(e) => this.iniciarSesion(e, usuarioAutenticar)}
                      className="col-md-8"
                      style={{
                        display: this.props.fromSignup ? "none" : "block",
                      }}
                    >
                      <p className="psol">Iniciar sesión</p>

                      <div className="form-group">
                        <Input
                          onChange={this.actualizarState}
                          value={email}
                          type="text"
                          name="email"
                          className="form-control"
                          placeholder="Correo electrónico"
                        />
                      </div>

                      <div className="form-group">
                        <Input.Password
                          onChange={this.actualizarState}
                          value={password}
                          type="password"
                          name="password"
                          className="form-control"
                          placeholder="Contraseña"
                        />
                      </div>

                      <div className="forgot">
                        <Checkbox>Recordarme</Checkbox>
                        <a
                          className="login-form-forgot"
                          href="/forgot-password"
                        >
                          ¿Lo olvidaste?
                        </a>
                      </div>
                      <div className="form-group">
                        <button
                          disabled={loading || this.validarForm()}
                          type="submit"
                          className="btn-btn-primary"
                          id="loginSubmit"
                        >
                          Iniciar sesión
                        </button>
                      </div>

                      <div>
                        ¿No tienes una cuenta?{" "}
                        <a className="forgot" href="/register">
                          Regístrate ahora!
                        </a>
                      </div>
                    </form>

                    <SocialLogin logon={this.logon} fromSignup={true} />
                  </div>
                </div>
              );
            }}
          </Mutation>
        </Fragment>
      </div>
    );
  }
}

export default withRouter(Login);

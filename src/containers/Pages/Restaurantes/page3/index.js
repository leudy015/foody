import React from "react";

import "./index.css";

const img = require("./img.svg");
const img1 = require("./img1.svg");
const img2 = require("./img2.svg");

export default function ComoFunciona() {
  return (
    <div className="container_qa">
      <h1 className="restaurant_title">¿Cómo funciona Foody®?</h1>
      <div className="container_qa_childrem">
        <div className="container_qa_item">
          <div>
            <h1>Une tu restaurante</h1>
            <img src={img} alt="" />
            <p>
              Envianos tu solicitud y un especialista te contactará para
              ayudarte con todo el proceso de digitalización de tu negocio
            </p>
          </div>
          <div>
            <h1>Prepára tu negocio</h1>
            <img src={img2} alt="" />
            <p>
              Sube tu menú, si necesitas programa una sesión de fotos y te
              preparamos tu menú digital
            </p>
          </div>
          <div>
            <h1>Empieza a vender</h1>
            <img src={img1} alt="" />
            <p>
              Llega a nuevos clientes con Foody® Pick Up, facilita el proceso de
              pedido y añade canales de venta a tu restaurante
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

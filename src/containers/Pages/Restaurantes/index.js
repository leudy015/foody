import React, { useEffect, useState } from "react";
import Page1 from "./page1";
import Page2 from "./page2";
import Page3 from "../Home/page4";
import Page4 from "./page3";
import { Modal, Input, message } from "antd";
import { UserOutlined, MailOutlined, PhoneOutlined } from "@ant-design/icons";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";

import "./index.css";

const barner = require("./barner.svg");

export default function Restaurantes() {
  const [visible, setVisible] = useState(false);
  const [dataForm, setDataForm] = useState(SetElements());

  const onChage = (e) => {
    setDataForm({
      ...dataForm,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    document.title = "Restaurantes";
  }, []);

  const sendInfo = async () => {
    if (dataForm) {
      await fetch(`${LOCAL_API_URL}/contact-for-restaurant`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataForm),
      })
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
          message.error(
            "Error al enviar tu solicitud vuelve a intentarlo por favor"
          );
        })
        .finally(() => {
          message.success("Solicitud enviada con éxito");
          setVisible(false);
        });
    } else {
      message.warning("Debes completar los campos para continuar");
    }
  };

  return (
    <>
      <div className="containers_restaurantes">
        <div className="Barner-restaurant-full">
          <div className="hero">
            <div>
              <div className="titles">
                <h1>Pedidos online sin comisiones</h1>
                <p>
                  Empieza a aceptar pedidos de recogida o entrega sin contacto
                  con la potente herramienta de Foody®.
                </p>
                <button onClick={() => setVisible(true)}>Unirme ahora</button>
              </div>
            </div>
            <div>
              <img src={barner} alt="Foody Pick Up" className="barners" />
            </div>
          </div>
        </div>
        <Page1 />
        <Page2 />
        <Page3 />
        <Page4 />
        <div className="logi_cont">
          <div className="inf">
            <h1>Ya eres parte de Foody® for Restaurants</h1>
            <p>
              Ve a tu panel de control donde podrás gestionar tus pedidos, menú
              y restaurantes.
            </p>
            <a href="https://restaurant.foodyapp.es/">Iniciar sesión</a>
          </div>
        </div>
      </div>
      <Modal
        title="Solicitud de admisión"
        visible={visible}
        onOk={() => sendInfo()}
        okText="Enviar solicitud"
        cancelText="Cancelar"
        onCancel={() => setVisible(false)}
      >
        <div style={{ margin: 20 }}>
          <Input
            name="name"
            placeholder="Nombre"
            style={{ marginBottom: 20 }}
            prefix={<UserOutlined />}
            onChange={onChage}
          />
          <Input
            name="email"
            placeholder="Email"
            style={{ marginBottom: 20 }}
            prefix={<MailOutlined />}
            onChange={onChage}
          />
          <Input
            name="phone"
            placeholder="Teléfono"
            style={{ marginBottom: 20 }}
            prefix={<PhoneOutlined />}
            onChange={onChage}
          />
        </div>
      </Modal>
    </>
  );
}

function SetElements() {
  return {
    name: "",
    email: "",
    phone: "",
  };
}

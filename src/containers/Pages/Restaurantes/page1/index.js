import React from "react";

import "./index.css";

const img = require("./item.svg");
const img1 = require("./item1.svg");
const img2 = require("./item2.svg");

export default function Page() {
  return (
    <div className="container_page1">
      <h1 className="restaurant_title">¿Por qué elegir Foody®?</h1>
      <div className="page1_childrem">
        <div>
          <img src={img1} alt="" />
          <h3>Aumenta tus ventas</h3>
          <p>
            Con la estrategia de marketing de nuestra plataforma, verás aumentar
            el volumen de pedidos de tu restaurante.
          </p>
        </div>
        <div>
          <img src={img2} alt="" />
          <h3>Encuentra nuevos clientes</h3>
          <p>Atrae a nuevos clientes y consigue que repitan y se queden.</p>
        </div>
        <div>
          <img src={img} alt="" />
          <h3>Automatizar el marketing</h3>
          <p>
            Ponemos todos nuestros recursos y medios a tu servicio para ayudarte
            a reducir costes e impulsar tu negocio
          </p>
        </div>
      </div>
    </div>
  );
}

import React from "react";

import "./index.css";

const img = require("./imagen.png");

export default function Sofware() {
  var navInfo = window.navigator.appVersion.toLowerCase();
  var so = "Sistema Operativo";
  function retornarSO() {
    if (navInfo.indexOf("win") != -1) {
      so = "Windows";
    } else if (navInfo.indexOf("linux") != -1) {
      so = "Linux";
    } else if (navInfo.indexOf("mac") != -1) {
      so = "MacOS (x64)";
    }
    return so;
  }
  var sistem = "MacOS (x64)";
  var link = "https://foody-img.s3.eu-west-3.amazonaws.com/foody-1.0.0.dmg";
  var href = `foody-1.0.0.dmg`;
  switch (retornarSO()) {
    case "Windows":
      sistem = "Windows";
      link =
        "https://foody-img.s3.eu-west-3.amazonaws.com/foody_Setup_1.0.0.exe";
      href = `foody-1.0.0.dmg`;
      break;
    case "Linux":
      sistem = "Linux";
      link =
        "https://foody-img.s3.eu-west-3.amazonaws.com/foody_1.0.0_amd64.snap";
      href = `foody-1.0.0.dmg`;
      break;
    default:
      sistem = "MacOS (x64)";
      link = "https://foody-img.s3.eu-west-3.amazonaws.com/foody-1.0.0.dmg";
      href = `foody-1.0.0.dmg`;
      break;
  }
  return (
    <div className="software_container">
      <h1 className="restaurant_title">
        ¿Obtén la herramienta de Foody® for Restaurants?
      </h1>
      <div className="software_container_childrem">
        <div className="software_item">
          <div className="u">
            <h1>
              Descarga la herramienta gratuita de Foody® for Restaurants y
              digitaliza tu negocio.
            </h1>
            <p>
              Disponemos de una gran herramienta totalmente gratis para la
              gestión de tu restaurante disponible para todos los sistemas
              operativos.
            </p>
            <a
              className="btn-dowload"
              href={link}
              download={href}
            >{`Descargar para ${sistem}`}</a>
          </div>
          <div className="d">
            <img src={img} alt="" style={{ borderRadius: 20 }} />
          </div>
        </div>
      </div>
    </div>
  );
}

import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";

import "../index.css";

export default function TermAndCondition() {
  useEffect(() => {
    document.title = "Términos y condiciones";
  }, []);
  return (
    <div>
      <Barner title="Términos y condiciones de uso de Foody®" />
      <div className="containers_hepls">
        <div className="cookies">
          <h2 className="text-center">
            Términos y condiciones de uso de la aplicación móvil Foody® Pick Up
          </h2>
          <h4>1. Foody® Pick Up</h4>
          <p>
            En cumplimiento de la normativa reguladora de los Servicios de la
            Sociedad de la Información de Comercio Electrónico, le informamos
            que el titular de la aplicación Foody® Pick Up es Foody® Pick Up
            S.L. sociedad española, inscrita en el registro Mercantil de
            Barcelona al tomo 000000, libro 0, hoja 000000, con CIF 0000000,
            domicilio social en Dirección y página de contacto www.foodyapp.es
            Pueden usar Foody® Pick Up todas aquellas personas que tengan
            capacidad legal necesaria para celebrar contratos.
          </p>
          <h4>2. PERFECCIÓN DEL CONTRATO</h4>
          <p>
            El contrato se entenderá perfeccionado entre Foody® Pick Up y el
            usuario de la aplicación en el momento en que éste último, una vez
            realizado el pedido, reciba a través de la propia aplicación, una
            nota de confirmación del mismo. Para poder realizar el primer pedido
            y ser Cliente de la plataforma es indispensable que se cumplan los
            siguientes requisitos:
          </p>
          <ul>
            <li>Haber cumplido o ser mayor de 18 años de edad.</li>
            <li>
              Cumplimentar de manera veraz los campos obligatorios del
              formulario de registro, en el que se solicitan datos de carácter
              personal como nombre de usuario, correo electrónico, número de
              teléfono y número de tarjeta bancaria.
            </li>
            <li>
              Aceptar las presentes Condiciones de uso, que incluyen la
              aceptación de la política de Privacidad y Protección de Datos.
            </li>
          </ul>
          El Usuario garantiza que todos los datos sobre su identidad y
          legitimidad facilitados a Foody® en sus formularios de registro de la
          Plataforma son veraces, exactos y completos. Asimismo se compromete a
          mantener actualizados sus datos. En el supuesto en que el Usuario
          facilite cualquier dato falso, inexacto o incompleto o si Foody®
          considera que existen motivos fundados para dudar sobre la veracidad,
          exactitud e integridad de los mismos, Foody® podrá denegarle el acceso
          y uso presente o futuro de la Plataforma o de cualquiera de sus
          contenidos y/o servicios. Al darse de alta en la Plataforma el usuario
          seleccionará un nombre de usuario (username) y una clave de acceso
          (password). Tanto el username como el password son estrictamente
          confidenciales, personales e intransferibles. El usuario se compromete
          a no divulgar los datos relativos a su cuenta ni hacerlos accesibles a
          terceros. El usuario será el único responsable en caso de uso de
          dichos datos por terceros, incluidas las manifestaciones vertidas en
          la Plataforma, o cualquier otra actuación que se lleve a cabo mediante
          el uso del username y/o password. Foody® no puede garantizar la
          identidad de los usuarios registrados, por tanto, no será responsable
          del uso de la identidad de un usuario registrado por terceros no
          registrados. Los usuarios se obligan a poner inmediatamente en
          conocimiento de Foody® la sustracción, divulgación o pérdida de su
          username o password, comunicándolo a&nbsp;la siguiente dirección de
          correo electrónico: info@foodyapp.es Para poder completar el registro
          en la Plataforma el Usuario deberá proporcionar algunos datos como:
          nombre de Usuario, dirección de correo electrónico, teléfono, datos de
          la tarjeta bancaria, etc. Una vez completado el registro, todo Usuario
          podrá acceder a su perfil y completarlo y/o editarlo según estime
          conveniente. El Usuario podrá darse de baja de la Plataforma
          comunicándolo mediante un correo electrónico a través de la dirección:
          info@foodyapp.es.
          <p></p>
          <h4>3. OBJETO. SERVICIOS OFRECIDOS</h4>
          <p>
            A través de esta aplicación el usuario, tras darse de alta en la
            plataforma, podrá efectuar pedidos en los Restaurantes asociados a
            Foody® para su consumo en el propio establecimiento, y pagar la
            cuenta, una vez concluido el servicio de comida, bien en el propio
            establecimiento, bien a través de la pasarela de pago de la propia
            aplicación. Utilizando estos servicios el usuario está aceptando las
            condiciones que figuran a continuación. Todos los productos que
            aparecen en la aplicación están condicionados a su disponibilidad.
            Cuando el usuario realice un pedido, la propia aplicación le
            informará automáticamente sobre la disponibilidad del mismo. Su
            Restaurante asociado podrá ofrecerle una alternativa a cualquier
            comida que no pueda proporcionarle. Es posible que nuestros
            Restaurantes asociados utilicen frutos secos en la preparación de
            ciertas comidas. Si Vd. padece alguna alergia, puede comunicárselo a
            nuestro Restaurante asociado antes de hacer el pedido. Foody® Pick
            Up no puede garantizar que las Comidas vendidas por nuestros
            Restaurantes asociados estén libres de alergénicos.
          </p>
          <h4>4. VENTA DE ALCOHOL</h4>
          <p>
            Las personas que realicen un pedido de alcohol a nuestros
            Restaurantes asociados deben tener 18 años o más. Las bebidas
            alcohólicas sólo pueden venderse a personas mayores de 18 años. Al
            realizar un pedido que incluya alcohol, Vd. confirma que tiene al
            menos 18 años de edad. Foody® se reserva el derecho de negarse a
            entregar alcohol a cualquiera persona que no pueda probar tener al
            menos 18 años de edad.
          </p>
          <h4>5. PRECIO DE LOS PRODUCTOS</h4>
          <p>
            Los precios de los productos serán los que figuren en nuestra
            aplicación; dichos precios podrán variar y ser revisados
            periódicamente, atendiendo a las modificaciones que periódicamente
            se incorporen a la carta/menú de los establecimientos asociados.
            Foody® Pick Up toma todas las precauciones necesarias para tratar de
            asegurar que los precios de los productos que figuran en la misma
            son correctos. Sin embargo, si existieran diferencias entre el
            precio de la aplicación y el que figura en la carta/menú del
            Restaurante, prevalecerá siempre el menor de los dos precios. A
            menos que se indique lo contrario, el precio de un Producto figurará
            con IVA.
          </p>
          <h4>6. FORMA Y CONDICIONES DE PAGO</h4>
          <p>
            Concluido el servicio, el pago de la factura podrá realizarse bien
            en el propio establecimiento, con tarjeta de crédito/débito o en
            efectivo, bien a través de la aplicación, mediante tarjeta de débito
            o crédito. El cargo en la tarjeta de débito o crédito se efectuará
            en el momento en que desde la aplicación se cierre la cuenta y se
            emita la factura, por el importe que figurará en la misma.
          </p>
          <h4>7. DEVOLUCIONES Y REEMBOLSOS</h4>
          <p>
            Si, por cualquier causa que fuere, existiere un error en el cobro de
            la factura, el usuario deberá comunicarlo a Foody® Pick Up dentro de
            los 2 días siguientes a la fecha de su emisión y pago. Aceptada la
            reclamación, después de efectuar las debidas comprobaciones, durante
            los 3 días siguientes a la misma Foody® Pick Up restituirá los pagos
            indebidamente efectuados.
          </p>
          <h4>8. RESPONSABILIDAD</h4>
          <p>
            Foody® Pick Up no se hace responsable de posibles fallos o retrasos
            del establecimiento en la prestación del servicio, siendo únicamente
            responsable el adecuado funcionamiento de la aplicación durante todo
            el proceso de prestación de los mismos.
          </p>
          <h4>9. DERECHOS DE PROPIEDAD INTELECTUAL</h4>
          <p>
            Todos los derechos de propiedad intelectual que integran la
            aplicación son propiedad de Foody® Pick Up S.L. y nada en estos
            Términos constituirá una cesión de los mismos. Los derechos de
            Propiedad Intelectual incluyen las patentes, los derechos sobre las
            invenciones, modelos de utilidad, derechos de autor y derechos
            conexos, marcas comerciales, marcas de servicio, comerciales,
            empresariales y de dominio, derechos de imagen comercial o
            presentación, los derechos de no competencia, competencia desleal,
            derechos de diseños, derechos de software, base de datos, derechos
            de topografía, derechos morales, derechos de información
            confidencial (incluyendo know-how y secretos comerciales ) y
            cualquier otro derecho de propiedad intelectual, que esté registrado
            o no, y que incluye todas las solicitudes y renovaciones o
            extensiones de tales derechos, y todos los derechos o formas de
            protección semejantes o equivalentes en cualquier parte del mundo.
          </p>
          <h4>10. SUBIR MATERIAL A NUESTRO SITIO Y NUESTRO SERVICIO</h4>
          <p>
            Cualquier material que suba a nuestro Servicio será considerado no
            confidencial y no privado, y Vd. reconoce y acepta que nosotros
            tenemos el derecho de usar, copiar, distribuir, vender y divulgar a
            terceros el material citado con cualquier finalidad ligada a nuestra
            actividad económica. En la medida en que dicho material esté
            protegido por derecho de propiedad intelectual, Vd. nos otorga una
            licencia perpetua, para todos los territorios del mundo y gratuita,
            para el uso, copia, modificación, distribución, venta y divulgación
            a terceros de dicho material o información para cualquier finalidad
            ligada a nuestra actividad económica.
          </p>
          <h4>11. COMUNICACIONES</h4>
          <p>
            Dando cumplimiento a la normativa aplicable, parte de la información
            o comunicaciones que se envíen al usuario se realizará por escrito.
            Al utilizar nuestro sitio Web, el usuario acepta que las
            comunicaciones con Foody® Pick Up sean principalmente electrónicas.
            Foody® Pick Up se pondrá en contacto con el usuario a través de la
            propia aplicación. A efectos contractuales el usuario acepta estos
            medios de comunicación y reconoce la validez de los contratos,
            notificaciones, información y otras comunicaciones que le sean
            facilitadas por estos medios. Esta condición no afecta a los
            derechos que le pudieran corresponder. Las comunicaciones a Foody®
            Pick Up deberán realizarse a la siguiente dirección:
            info@foodyapp.es El usuario será notificado en la dirección postal o
            correo electrónico que proporcione al darse de alta en la
            aplicación. El uso de su información personal facilitada a través de
            la Web se regirá por nuestra Política de Privacidad y Política de
            Cookies.
          </p>
          <h4>12. POLITICA DE PRIVACIDAD</h4>
          <p>
            El Usuario, al proporcionar a Foody®, S.L. sus datos de carácter
            personal a través de los formularios electrónicos de la Web o de la
            Aplicación, consiente expresamente que Foody® S.L. pueda tratar esos
            datos en los términos de esta cláusula de Política de Privacidad y
            Protección de Datos y para los fines aquí expresados. Antes de
            registrase en Foody®, los usuarios deben leer la presente Política
            de Privacidad y Protección de Datos. Al marcar el botón
            “registrarse”, los usuarios afirman que han leído y que consienten
            expresamente las presente Política de Privacidad de Datos. Al
            registrarse, los usuarios deberán proporcionar algunos datos para la
            creación de su cuenta y la edición de su perfil. Éstos deberán
            proporcionar los siguientes datos: nombre de Usuario, correo
            electrónico, número de teléfono y tarjeta de crédito. Asimismo,
            Foody® Pick Uo, S.L, siempre y cuando los usuarios lo autoricen,
            recogerá datos relacionados con su localización, incluyendo la
            localización geográfica en tiempo real del ordenador o dispositivo
            móvil de los Usuario. Una vez completado el registro, todo usuario
            podrá acceder a su perfil y completarlo y/o editarlo según estime
            conveniente. El usuario también podrá acceder y crear una cuenta a
            través de su perfil de Facebook. La información y datos facilitados
            por el usuario estarán en todo momento disponibles en su cuenta de
            usuario y podrán ser modificados por el usuario a través de la
            opción editar perfil. El usuario se compromete a introducir datos
            reales y veraces. Asimismo, será el único responsable de los daños y
            perjuicios que Foody®, S.L. o terceros pudieran sufrir como
            consecuencia de la falta de veracidad, inexactitud, falta de
            vigencia y autenticidad de los datos facilitados. Los datos
            recabados por Foody®, S.L., serán exclusivamente utilizados para la
            consecución del objeto definido en las presentes Condiciones
            Generales de Uso de la Web.
          </p>
          <h4>13. NULIDAD</h4>
          <p>
            Si cualquier estipulación, o parte de la misma, de las Condiciones
            de esta aplicación móvil fuera declarada ilegal, nula o no aplicable
            de otra forma, por cualquier juzgado o autoridad competente, dicha
            estipulación o parte de la misma será eliminada de las Condiciones
            de la aplicación, y el resto de Condiciones de la misma regirán como
            si la estipulación, o parte de la misma, ilegal, nula o no
            aplicable, nunca se hubiera acordado.
          </p>
          <h4>14. DERECHO A MODIFICAR LOS TÉRMINOS Y CONDICIONES</h4>
          <p>
            Foody® Pick Up se reserva el derecho de revisar y modificar estos
            Términos y Condiciones periódicamente. El usuario estará sujeto a
            las políticas y los términos y condiciones vigentes en el momento en
            que efectúe un pedido.
          </p>
          <h4>15. PROTECCIÓN DE DATOS PERSONALES</h4>
          <p>
            En cumplimiento del REGLAMENTO (UE) 2016/679 DEL PARLAMENTO EUROPEO
            Y DEL CONSEJO de&nbsp;27 de abril de 2016, relativo a la protección
            de las personas físicas en lo que respecta al tratamiento de datos
            personales y a la libre circulación de estos datos y su normativa de
            desarrollo, Foody® Pick Up informa al usuario que los datos
            personales que nos facilite serán tratados con la finalidad de
            facilitarle el acceso a nuestra aplicación y prestarle nuestros
            servicios, descritos correctamente en la segunda cláusula de las
            presentes condiciones. Cuando el usuario crea una cuenta y se
            registra en nuestra aplicación, accede a la aplicación a través de
            su perfil en la red social de Facebook, o contacta con Foody® Pick
            Up a través de los medios puestos disposición del usuario, está
            prestando su consentimiento expreso para el tratamiento de sus datos
            personales con el fin de atenderle, que pueda utilizar los servicios
            de nuestra aplicación, recibir información sobre novedades en
            nuestros servicios y disfrutar de promociones y/o descuentos
            personalizados. En ningún caso Foody® Pick Up utilizará los datos
            personales de los interesados para fines distintos de los
            anteriormente mencionados, y se compromete a guardar el debido
            secreto profesional y a establecer las medidas técnicas y
            organizativas necesarias para salvaguardar la información conforme a
            los requerimientos que establece el mencionado Reglamento. Para la
            correcta gestión de su pedido, en ocasiones, será necesaria que
            Foody comunique sus datos al establecimiento asociado al que usted
            haya realizado su pedido. Esta cesión se realizará únicamente cuando
            sea estrictamente necesario para prestar el servicio solicitado. El
            usuario queda informado y consiente esta comunicación. Foody® Pick
            Up conservará los datos personales del usuario mientras esté dado de
            alta y utilice nuestra aplicación. El usuario puede, no obstante, en
            cualquier momento, solicitar el acceso, rectificación o supresión de
            sus datos personales. Así como, limitar u oponerse a su tratamiento,
            o ejercer el derecho a la portabilidad de los mismos. También le
            informamos de su derecho a presentar una reclamación ante la Agencia
            Española de Protección de Datos, si considera que en el tratamiento
            de sus datos no se están respetando sus derechos. Datos del
            responsable: Foody® Pick Up S.L. – c/ Balmes 209 5º 2ª, 08006,
            Barcelona – info@foodyapp.es
          </p>
          <h4>16. LEY APLICABLE Y JURISDICCIÓN</h4>
          <p>
            Las Condiciones de esta aplicación se regirán e interpretarán de
            conformidad con la ley española. Foody® Pick Up y el usuario, con
            renuncia expresa a cualquier otro fuero o legislación aplicable que
            pudiere corresponderles, se someten a la legislación de Derecho
            común española y a la jurisdicción de los Juzgados y Tribunales de
            la ciudad de Barcelona (España), con renuncia a cualquier otro fuero
            que pudiere corresponderles. Por favor, marque la casilla
            confirmando que acepta estos términos y condiciones que figura al
            final de las mismas. Por favor comprenda que si rechaza aceptarlos
            no podrá realizar pedidos a través de nuestra aplicación.
          </p>
        </div>
      </div>
    </div>
  );
}

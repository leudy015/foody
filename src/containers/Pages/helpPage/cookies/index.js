import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";

import "../index.css";

export default function Cookies() {
  useEffect(() => {
    document.title = "Politíca de cookies";
  }, []);

  return (
    <div>
      <Barner title="Politíca de cookies" />
      <div className="containers_hepls">
        <div className="entry-content">
          <blockquote className="cookies">
            <p>
              <strong>¿Qué son las cookies?</strong>
            </p>
            <p>
              Una cookie es un fichero que se descarga en su ordenador
              al&nbsp;acceder a determinadas páginas web. Las cookies permiten a
              una página web,&nbsp;entre otras cosas, almacenar y recuperar
              información sobre los hábitos de&nbsp;navegación de un usuario o
              de su equipo y, dependiendo de la información que&nbsp;contengan y
              de la forma en que utilice su equipo, pueden utilizarse
              para&nbsp;reconocer al usuario.
            </p>
            <p>
              <strong>
                ¿Qué tipos de cookies utiliza esta página web?&nbsp;
              </strong>
            </p>
            <p>Esta página web utiliza los siguientes tipos de cookies:</p>
            <p>
              <strong>Cookies de análisis</strong>: Son aquéllas que bien
              tratadas por nosotros o por&nbsp;terceros, nos permiten
              cuantificar el número de usuarios y así realizar la&nbsp;medición
              y análisis estadístico de la utilización que hacen los usuarios
              del&nbsp;servicio ofertado. Para ello se analiza su navegación en
              nuestra página web&nbsp;con el fin de mejorar la oferta de
              productos o servicios que le ofrecemos.
            </p>
            <p>
              <strong>Cookies técnicas</strong>: Son aquellas que permiten al
              usuario la navegación a través del área restringida y la
              utilización de sus diferentes funciones, como por ejemplo, llevar
              a cambio el proceso de compra de un artículo.
            </p>
            <p>
              <strong>Cookies de personalización</strong>: Son aquellas que
              permiten al usuario acceder al servicio con algunas
              características de carácter general predefinidas en función de una
              serie de criterios en el terminal del usuario como por ejemplo
              serian el idioma o el tipo de navegador a través del cual se
              conecta al servicio.
            </p>
            <p>
              <strong>Cookies publicitarias</strong>: Son aquéllas que, bien
              tratadas por esta web&nbsp;o por&nbsp;terceros, permiten gestionar
              de la forma más eficaz posible la oferta de&nbsp;los espacios
              publicitarios que hay en la página web, adecuando el
              contenido&nbsp;del anuncio al contenido del servicio solicitado o
              al uso que realice de nuestra&nbsp;página web. Para ello podemos
              analizar sus hábitos de navegación en Internet&nbsp;y podemos
              mostrarle publicidad relacionada con su perfil de navegación.
            </p>
            <p>
              <strong>Cookies de publicidad comportamental</strong>: Son
              aquellas que permiten la gestión, de la forma más eficaz posible,
              de los espacios publicitarios que, en su caso, el editor haya
              incluido en una página web, aplicación o plataforma desde la que
              presta el servicio solicitado. Este tipo de cookies almacenan
              información del comportamiento de los visitantes&nbsp;obtenida a
              través de la observación continuada de sus hábitos de navegación,
              lo que permite desarrollar un perfil específico para mostrar
              avisos publicitarios&nbsp;en función del mismo.
            </p>
            <p>
              <strong>Desactivar las cookies.</strong>
            </p>
            <p>
              Puede usted&nbsp;
              <strong>permitir, bloquear o eliminar las cookies</strong>
              &nbsp;instaladas en su equipo&nbsp;mediante la configuración de
              las opciones del navegador instalado en su&nbsp;ordenador.
            </p>
            <p>
              En la mayoría de los navegadores web se ofrece la posibilidad de
              permitir, bloquear o eliminar las cookies instaladas en su equipo.
            </p>
            <p>
              A continuación puede acceder a la configuración de los navegadores
              webs más frecuentes para aceptar, instalar o desactivar las
              cookies:
            </p>
            <p>
              <a
                href="https://support.google.com/chrome/answer/95647?hl=es"
                target="_blank"
                rel="noreferrer noopener"
              >
                Configurar cookies en Google Chrome
              </a>
            </p>
            <p>
              <a
                href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9"
                target="_blank"
                rel="noreferrer noopener"
              >
                Configurar cookies en Microsoft Internet Explorer
              </a>
            </p>
            <p>
              <a
                href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&amp;redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we"
                target="_blank"
                rel="noreferrer noopener"
              >
                Configurar cookies en Mozilla Firefox
              </a>
            </p>
            <p>
              <a
                href="https://support.apple.com/es-es/HT201265"
                target="_blank"
                rel="noreferrer noopener"
              >
                Configurar cookies en Safari (Apple)
              </a>
            </p>
            <p>
              <strong>Cookies de terceros.</strong>
            </p>
            <p>
              Esta página web utiliza&nbsp;servicios de terceros para recopilar
              información con fines estadísticos y de uso de la web. Se usan
              cookies de DoubleClick para mejorar la publicidad que se incluye
              en el sitio web. Son utilizadas para orientar la publicidad según
              el contenido que es relevante para un usuario, mejorando así la
              calidad de experiencia en el uso del mismo.
            </p>
            <p>
              En concreto, usamos los servicios de Google Adsense y de Google
              Analytics para nuestras estadísticas y publicidad. Algunas cookies
              son esenciales para el funcionamiento del sitio, por ejemplo el
              buscador incorporado.
            </p>
            <p>
              Nuestro sitio incluye otras funcionalidades proporcionadas por
              terceros. Usted puede fácilmente compartir el contenido en redes
              sociales como Facebook, Twitter o Google +, con los botones que
              hemos incluido a tal efecto.
            </p>
            <p>
              <strong>Advertencia sobre eliminar&nbsp;cookies.</strong>
            </p>
            <p>
              Usted puede eliminar y bloquear todas las cookies de este sitio,
              pero parte del sitio no funcionará o la calidad de la página web
              puede verse afectada.
            </p>
            <p>
              Si tiene cualquier duda acerca de&nbsp;nuestra política de
              cookies, puede contactar con esta página web a través de nuestros
              canales de Contacto.
            </p>
          </blockquote>
        </div>
      </div>
    </div>
  );
}

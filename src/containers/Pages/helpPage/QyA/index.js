import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";
import { Collapse } from "antd";

import "../index.css";

const { Panel } = Collapse;

const Preguntas = () => {
  useEffect(() => {
    document.title = "Preguntas frecuentes";
  }, []);
  return (
    <div>
      <Barner title="Preguntas frecuentes" />
      <div className="containers_hepls">
        <div className="cookie">
          <Collapse accordion>
            <Panel header="¿Cuándo está abierto Foody?" key="1">
              <p>
                Muchas ciudades en las que Foody opera ofrecen un servicio 24/7.
                No obstante, cada establecimiento tiene su propio horario, por
                lo que es posible que un establecimiento esté cerrado aunque
                Foody esté disponible. Para guiarte, los establecimientos
                abiertos tienen una imagen a todo color, mientras que los que
                están cerrados se muestran en gris desenfocado.
              </p>
            </Panel>
            <Panel
              header="¿Cómo puedo programar un pedido y cambiar los detalles de mi pedido programado?"
              key="2"
            >
              <p>
                ¡Programar pedidos nunca ha sido tan fácil! Puedes programar tu
                pedido y elegir cuándo quieres que se entregue para recibirlo
                cuando te venga mejor. Si deseas cambiar los detalles de tu
                pedido programado o simplemente deseas cancelarlo, deberás ir a
                tu pedido programado y hacer clic en "Cancelar pedido". No se te
                cobrará por la cancelación. Una vez hayas cancelado el pedido,
                realiza otro con los artículos necesarios y los detalles de
                entrega o recogida.
              </p>
            </Panel>
            <Panel header="Quiero devolver un producto, ¿cómo lo hago?" key="3">
              <p>
                Para devoluciones de pedido puedes contacatar con el equipo de
                soporte ellos te guiaran el todo el proceso hasta que tu dinero
                este de vuelta el tu cuenta.
              </p>
            </Panel>
            <Panel header="¿Qué es Foody?" key="4">
              <p>
                Foody es la app que te permite realizar pedidos en los
                restaurantes de tu ciuda para recoger, ahorandote tiempo y
                dinero en tus compras.
              </p>
            </Panel>
            <Panel header="¿Qué Tipo De Restaurantes Hay En Foody?" key="5">
              <p>
                Seleccionamos personalmente nuestros restaurantes. Desde un
                Gastro-bar de moda, hasta un restaurante de comida mediterránea
                de renombre. Lo único que no encontrarás en Foody es comida de
                baja calidad.
              </p>
            </Panel>
            <Panel header="¿A Qué Horas Puedo Hacer Un Pedido?" key="6">
              <p>
                Repartimos todos los días, desde la mañana hasta la noche, y en
                función de los horarios de apertura de los restaurantes. Visita
                la web o la aplicación para ver qué restaurantes están
                disponibles en tu área.
              </p>
            </Panel>
          </Collapse>
        </div>
      </div>
    </div>
  );
};

export default Preguntas;

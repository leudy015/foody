import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";

import "../index.css";

export default function Privacity() {
  useEffect(() => {
    document.title = "Política de Privacidad";
  }, []);
  return (
    <div>
      <Barner title="Aviso Legal y Política de Privacidad" />
      <div className="containers_hepls">
        <div className="cookies">
          <h2>Aviso Legal y Política de Privacidad</h2>
          <p>
            <strong>
              <span>Objeto Social. </span>
            </strong>
            <span>
              Foody® Pick Up S.L. (en adelante Foody® Pick Up), es una empresa
              dedicada al desarrollo, diseño, fabricación y comercialización de
              un software de menú digital a través del cual los usuarios pueden
              realizar pedidos.
            </span>
          </p>
          <p>
            <strong>
              <span>Identificación. </span>
            </strong>
            <span>
              En cumplimiento con el deber de información recogido en artículo
              10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad
              de la Información y del Comercio Electrónico, los datos aquí
              consignados corresponden a la entidad titular del sitio web{" "}
              <u>foodyapp.es</u>.
            </span>
          </p>
          <p>
            <strong>
              <span>Denominación</span>
            </strong>
            <span>: Foody® Pick Up , S.L.</span>
          </p>
          <p>
            <strong>
              <span>Domicilio</span>
            </strong>
            <span>: c/ Balmes 209 5º 2ª – 08006 – Barcelona</span>
          </p>
          <p>
            <strong>
              <span>Teléfono</span>
            </strong>
            <span>: 689 351 592</span>
          </p>
          <p>
            <strong>
              <span>Email</span>
            </strong>
            <span>: info@foodyapp.es</span>
          </p>
          <p>
            <strong>
              <span>CIF</span>
            </strong>
            <span>: 000000000000</span>
          </p>
          <p>
            <strong>
              <span>Inscripción registral</span>
            </strong>
            <span>
              :{" "}
              <span>
                Inscrita en el Registro Mercantil de Barcelona Tomo 000, Folio
                00, Hoja 0000000000.
              </span>
            </span>
          </p>
          <p>
            <strong>
              <span>Propiedad intelectual e industrial. </span>
            </strong>
            <span>
              Todos los derechos de Propiedad Industrial e Intelectual de la
              totalidad de elementos contenidos en esta Web, incluidos los
              formatos, diseños gráficos, textos, imágenes y documentos,
              pertenecen a Foody® Pick Up y se encuentran protegidos por las
              leyes españolas e internacionales sobre propiedad Intelectual e
              Industrial. Asímismo todas las marcas y nombres comerciales
              contenidos en esta Web pertenecen a Foody® Pick Up, o a la persona
              física, jurídica, organismo o entidad que ostente la titularidad
              del signo distintivo y se encuentran del mismo modo protegidos por
              las leyes españolas e internacionales sobre propiedad Intelectual
              e Industrial. Queda expresamente prohibida la reproducción total o
              parcial de este sitio Web y de cualquiera de sus contenidos sin el
              permiso expreso y por escrito de Foody® Pick Up.
            </span>
          </p>
          <p>
            <span>
              El acceso al sitio web no implica ningún tipo de renuncia,
              transmisión, licencia o cesión de dichos derechos por parte de
              Foody® o los titulares de los derechos sobre los signos
              distintivos que se muestran en esta página web, salvo que se
              establezca expresamente lo contrario.{" "}
            </span>
          </p>
          <p>
            <strong>
              <span>Condiciones de uso. </span>
            </strong>
            <span>
              El acceso a este sitio Web implica la aceptación de estas{" "}
            </span>
            <span>condiciones</span>
            <span>
              {" "}
              de uso sin reservas que regulan el acceso y la utilización del
              mismo con el fin de poner a disposición de los usuarios
              información sobre nuestros servicios.
            </span>
          </p>
          <p>
            <span>
              Se prohíbe expresamente la utilización de los contenidos de este
              sitio Web para su utilización con fines comerciales o para su
              distribución, transformación o comunicación.
            </span>
          </p>
          <p>
            <span>
              {" "}
              Foody®, no responderá de ninguna consecuencia, daño o perjuicio
              que pudieran derivarse de dicha utilización o uso de la
              información.{" "}
            </span>
          </p>
          <p>
            <span>
              Tanto el acceso a esta Web como el uso que pueda hacerse de la
              información contenida en la misma es de la exclusiva
              responsabilidad de quien lo realiza.{" "}
            </span>
          </p>
          <p>
            <span>
              El usuario se obliga a no utilizar la información que se publica
              en esta Web con fines o efectos ilícitos o lesivos, a no dañar o
              inutilizar la información y a no realizar cualquier otra acción
              que puedan ser contraria al contenido de este Aviso Legal.
            </span>
          </p>
          <p>
            <span>
              {" "}
              Foody® se reserva el derecho a modificar los contenidos de la
              oferta comercial de sus servicios cuando lo estime oportuno y a
              mantener su contenido actualizado.
            </span>
          </p>
          <p>
            <span>
              {" "}
              Foody® no puede asegurar la inexistencia de interrupciones o
              errores en el acceso a este sitio Web, aunque pondrá sus mayores
              esfuerzos para evitarlos.
            </span>
          </p>
          <p>
            <strong>
              <span>Política de Protección de Datos. </span>
            </strong>
            <span>
              En cumplimiento del REGLAMENTO (UE) 2016/679 DEL PARLAMENTO
              EUROPEO Y DEL CONSEJO de 27 de Abril de 2016, relativo a la
              protección de las personas físicas en lo que respecta al
              tratamiento de datos personales y a la libre circulación de estos
              datos y su normativa de desarrollo, se le informa que los datos
              personales proporcionados por usted serán tratados con la
              finalidad de facilitarle la información solicitada acerca de
              nuestros servicios y proyectos de reforma, gestión técnica de
              edificios e interiorismo y gestionar nuestra relación comercial.
            </span>
          </p>
          <p>
            <span>
              A través de la cumplimentación del formulario de la Web o
              cualquier otro tipo de solicitud de información remitida a Foody®
              Pick Up, el interesado presta su consentimiento expreso para el
              tratamiento de sus datos personales.
            </span>
          </p>
          <p>
            <span>
              En ningún caso Foody® Pick Up utilizará los datos personales de
              los interesados para fines distintos de los anteriormente
              mencionados, ni los comunicará a terceros sin el consentimiento
              previo y expreso del afectado, y se compromete a guardar el debido
              secreto profesional y a establecer las medidas técnicas y
              organizativas necesarias para salvaguardar la información conforme
              a los requerimientos que establece el mencionado Reglamento.
            </span>
          </p>
          <p>
            <span>
              Sus datos personales se conservarán en nuestros registros durante
              un periodo de dos años. No obstante, en cualquier momento usted
              puede solicitar el acceso a sus datos personales y su
              rectificación o supresión. Así como, limitar su tratamiento, o
              directamente oponerse al tratamiento o ejercer el derecho a la
              portabilidad de los mismos.
            </span>
          </p>
          <p>
            <span>
              También le informamos de su derecho a presentar una reclamación
              ante la Agencia Española de Protección de Datos, si considera que
              en el tratamiento de sus datos no se están respetando sus
              derechos.
            </span>
          </p>
          <p>
            <span>
              Identidad del responsable (contacto): Foody® Pick Up , S.L.- c/
              Balmes 209 5º 2ª, 08006, Barcelona.
            </span>
          </p>
          <p>
            <strong>
              <span>Política de protección de menores. </span>
            </strong>
            <span>
              Quien facilita los datos a través de los formularios de esta Web y
              acepta su tratamiento declara formalmente ser mayor de 14 años.
            </span>
          </p>
          <p>
            <span>
              Queda prohibido el acceso y uso del portal a los menores de 14
              años de edad.
            </span>
          </p>
          <p>
            <span>
              {" "}
              Foody® recuerda a las personas mayores de edad que tengan a su
              cargo menores, que será de su exclusiva responsabilidad si algún
              menor incorpora sus datos para solicitar algún servicio.
            </span>
          </p>
          <p>
            <span>
              También les informa que existen programas informáticos para acotar
              la navegación mediante el filtro o bloqueo a determinados
              contenidos.
            </span>
          </p>
          <p>
            <strong>
              <span>Redes sociales. </span>
            </strong>
            <span>
              Al seguir nuestro perfil de las redes sociales consiente
              expresamente en el tratamiento de sus datos personales conforme a
              su política de privacidad. También consiente expresamente el
              acceso de Foody® Pick Up al tratamiento de sus datos contenidos en
              la lista de amigos y a que a que las noticias publicadas sobre los
              servicios ofrecidos por Foody® Pick Up aparezcan en&nbsp; su muro.
            </span>
          </p>
          <p>
            <span>
              De conformidad con el REGLAMENTO (UE) 2016/679 DEL PARLAMENTO
              EUROPEO, le informamos que los datos personales de los seguidores
              de Foody® Pick Up en las redes sociales, serán incorporados a un
              fichero cuyo titular es Foody® Pick Up con la finalidad de
              mantenerle informados a través de las redes sociales sobre
              noticias relativas a los servicios ofrecidos por Foody® Pick Up a
              través de su aplicación.
            </span>
          </p>
          <p>
            <span>
              Su petición para conectar, implica necesariamente su
              consentimiento para los tratamientos señalados.
            </span>
          </p>
          <p>
            <span>
              La publicación de comentarios y contenidos en las redes sociales
              se convertirán en información pública, por lo que los usuarios
              deberán tener especial cautela cuando decidan compartir su
              información personal. Foody® Pick Up no se hace responsable por la
              información que los usuarios incorporen en la página. No obstante,
              las personas cuyos datos personales se encuentren publicados o
              estén incluidos en comentarios, podrán solicitar a Foody® Pick Up
              la cancelación de los mismos.
            </span>
          </p>
          <p>
            <span>
              En cualquier momento, usted puede ejercer los derechos de acceso,
              rectificación, supresión y oposición, limitar el tratamiento de
              sus datos, o ejercer el derecho a la portabilidad de los mismos.
              Todo ello, mediante escrito, acompañado de copia de documento
              oficial que le identifique, dirigido a Foody® Pick Up. En caso de
              disconformidad con el tratamiento, también tiene derecho a
              presentar una reclamación ante la Agencia Española de Protección
              de Datos.
            </span>
          </p>
          <p>
            <span>
              Identidad del responsable (contacto): Foody® Pick Up , S.L.- c/
              Balmes 209 5º 2ª, 08006, Barcelona.
            </span>
          </p>
          <p>
            <strong>
              <span>Enlaces a otras Webs. </span>
            </strong>
            <span>
              Los enlaces (links) que puede encontrar en esta Web son un
              servicio a los usuarios. Estas páginas no son operadas ni
              controladas por Foody®, por ello, Foody® Pick Up no se hace
              responsable de los contenidos de esos sitios Web ni están
              cubiertas por el presente Aviso Legal. Si accede a estas páginas
              Webs deberá tener en cuenta que sus políticas de privacidad pueden
              ser diferentes a la nuestra.
            </span>
          </p>
          <p>
            <strong>
              <span>Legislación aplicable y competencia jurisdiccional. </span>
            </strong>
            <span>
              El presente Aviso Legal se rige por la normativa española vigente
              que le es de aplicación.
            </span>
          </p>
          <p>
            <span>
              Para la resolución de las controversias que pudieran derivarse
              como consecuencia de lo dispuesto en las presentes disposiciones y
              sobre su interpretación, aplicación y cumplimiento. El usuario, en
              virtud de la aceptación de las condiciones recogidas en este Aviso
              legal, renuncia expresamente a cualquier otro fuero que pudiera
              corresponderle.
            </span>
          </p>
          <p>
            <span>
              En cualquier caso, dentro de la jurisdicción española, si la
              legislación permitiese someterse a un fuero en concreto, el
              usuario renuncia expresamente al fuero que pudiera corresponderle
              y se somete voluntariamente a la jurisdicción de los Juzgados y
              Tribunales de Barcelona.
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

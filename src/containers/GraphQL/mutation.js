import gql from "graphql-tag";

export const AUTENTICAR_USUARIO = gql`
  mutation autenticarUsuario($email: String!, $password: String!) {
    autenticarUsuario(email: $email, password: $password) {
      success
      message
      data {
        token
        id
        verifyPhone
      }
    }
  }
`;

export const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        _id
        name
        lastName
        email
        verifyPhone
      }
    }
  }
`;

export const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const ELIMINAR_USUARIO = gql`
  mutation eliminarUsuario($id: ID!) {
    eliminarUsuario(id: $id) {
      success
      messages
    }
  }
`;

export const ANADIR_RESTAURANT_FAVORITE = gql`
  mutation crearFavorito($restaurantID: ID, $usuarioId: ID) {
    crearFavorito(restaurantID: $restaurantID, usuarioId: $usuarioId) {
      success
      messages
    }
  }
`;

export const ELIMINAR_RESTAURANT_FAVORITE = gql`
  mutation eliminarFavorito($id: ID) {
    eliminarFavorito(id: $id) {
      success
      messages
    }
  }
`;

export const CREATE_CARD_ITEMS = gql`
  mutation createItemCard($input: CreateCardInput) {
    createItemCard(input: $input) {
      success
      messages
    }
  }
`;

export const ACTUALIZAR_CARD = gql`
  mutation actualizarCardItem($input: AtualizarCardInput) {
    actualizarCardItem(input: $input) {
      id
      userId
      restaurant
      plato
      complementos
    }
  }
`;

export const DELETE_CARD_ITEMS = gql`
  mutation eliminarCardItem($id: ID) {
    eliminarCardItem(id: $id) {
      success
      messages
    }
  }
`;

export const CREAR_MODIFICAR_ORDEN = gql`
  mutation crearModificarOrden($input: OrdenCreationInput) {
    crearModificarOrden(input: $input) {
      id
      cupon
      nota
      aceptaTerminos
      restaurant
      time
      cantidad
      userID
      propina
      descuento {
        clave
        descuento
        tipo
      }
      platos {
        userId
        platoID
        restaurant
        complementos
        plato {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          anadidoCart
          cant
        }
      }
      restaurants {
        _id
        title
        image
        description
        type
        rating
        address {
          calle
          ciudad
          codigoPostal
          numero
        }
        coordetate
        anadidoFavorito
        coordenateone
        categoryName
        categoryID
        minime
        menu
        city
        phone
        email
        logo
        password
        apertura
        cierre
        diaslaborales
        OnesignalID
      }
      cubiertos
      estado
      status
      isvalored
      progreso
      created_at
      total
    }
  }
`;

export const CREAR_VALORACIONES = gql`
  mutation crearValoracion($input: ValoracionCreationInput) {
    crearValoracion(input: $input) {
      id
      comment
      value
      restaurant
      user
    }
  }
`;

export const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      messages
      success
    }
  }
`;

export const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID) {
    readNotification(notificationId: $notificationId) {
      messages
      success
    }
  }
`;

export const CREATE_OPINIO = gql`
  mutation createOpinion($input: OpinionInput) {
    createOpinion(input: $input) {
      messages
      success
    }
  }
`;

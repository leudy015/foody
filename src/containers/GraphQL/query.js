import gql from "graphql-tag";

export const USUARIO_ACTUAL = gql`
  query obtenerUsuario {
    obtenerUsuario {
      _id
      email
      name
      lastName
    }
  }
`;

export const GET_USUARIO = gql`
  query getUsuario {
    getUsuario {
      messages
      success
      data {
        _id
        name
        lastName
        email
        created_at
        updated_at
      }
    }
  }
`;

export const USER_DETAIL = gql`
  query getUsuario {
    getUsuario {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        telefono
        city
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        OnesignalID
      }
    }
  }
`;

export const CATEGORY = gql`
  query getCategory {
    getCategory {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

export const RESTAURANT = gql`
  query getRestaurant($city: String) {
    getRestaurant(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        description
        rating
        anadidoFavorito
        type
        address {
          calle
          numero
          codigoPostal
          ciudad
        }
        Valoracion {
          id
          comment
          value
          created_at
        }
        coordetate
        coordenateone
        categoryID
        categoryName
        minime
        menu
        city
        phone
        email
        isnew
        logo
        apertura
        cierre
        diaslaborales
        open
        OnesignalID
      }
    }
  }
`;

export const RESTAURANT_ID = gql`
  query getRestaurantForID($id: ID) {
    getRestaurantForID(id: $id) {
      messages
      success
      data {
        _id
        title
        image
        description
        rating
        anadidoFavorito
        type
        address {
          calle
          numero
          codigoPostal
          ciudad
        }
        Valoracion {
          id
          comment
          value
          created_at
        }
        coordetate
        coordenateone
        categoryID
        categoryName
        minime
        menu
        city
        phone
        email
        isnew
        logo
        apertura
        cierre
        diaslaborales
        open
        OnesignalID
      }
    }
  }
`;

export const RESTAURANT_FAVORITO = gql`
  query getRestaurantFavorito($id: ID) {
    getRestaurantFavorito(id: $id) {
      message
      success
      list {
        _id
        restaurantID
        usuarioId
        restaurant {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          Valoracion {
            id
            comment
            value
            created_at
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          menu
          city
          phone
          email
          logo
          isnew
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
      }
    }
  }
`;

export const RESTAURANT_FOR_CATEGORY = gql`
  query getRestaurantForCategory($city: String, $category: String) {
    getRestaurantForCategory(city: $city, category: $category) {
      messages
      success
      data {
        _id
        title
        image
        description
        rating
        anadidoFavorito
        type
        address {
          calle
          numero
          codigoPostal
          ciudad
        }
        Valoracion {
          id
          comment
          value
          created_at
        }
        coordetate
        coordenateone
        categoryID
        categoryName
        minime
        menu
        city
        isnew
        phone
        email
        logo
        apertura
        cierre
        diaslaborales
        open
        OnesignalID
      }
    }
  }
`;

export const GET_MENU = gql`
  query getMenu($id: ID) {
    getMenu(id: $id) {
      message
      success
      list {
        _id
        title
        subtitle
        restaurant
        platos {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          anadidoCart
          news
          opiniones {
            id
            plato
            comment
            rating
            created_at
            user
          }
        }
      }
    }
  }
`;

export const GET_ACOMPANANTE = gql`
  query getAcompanante($id: ID) {
    getAcompanante(id: $id) {
      message
      success
      list {
        id
        name
        plato
        required
        restaurant
        children {
          id
          name
          price
          cant
        }
      }
    }
  }
`;

export const GET_CART = gql`
  query getItemCarts($id: ID) {
    getItemCarts(id: $id) {
      message
      success
      list {
        id
        userId
        restaurant
        platoID
        total
        extra
        plato {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          cant
        }
        Restaurant {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          Valoracion {
            id
            comment
            value
            created_at
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          menu
          city
          phone
          email
          logo
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        UserId {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_CART_RESTAURANT = gql`
  query getItemCart($id: ID, $PlatoID: ID) {
    getItemCart(id: $id, PlatoID: $PlatoID) {
      message
      success
      list {
        id
        userId
        restaurant
        platoID
        total
        extra
        complementos
        plato {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          cant
        }
        Restaurant {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          Valoracion {
            id
            comment
            value
            created_at
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          menu
          city
          phone
          email
          logo
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        UserId {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_MY_CART_RESTAURANT = gql`
  query getMyItemCart($id: ID, $restaurant: ID) {
    getMyItemCart(id: $id, restaurant: $restaurant) {
      message
      success
      list {
        id
        userId
        restaurant
        platoID
        total
        extra
        complementos
        plato {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          cant
        }
        Restaurant {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          menu
          city
          phone
          email
          logo
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        UserId {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_CUPON = gql`
  {
    getCuponAll {
      id
      clave
      tipo
      descuento
    }
  }
`;

export const GET_MY_CART_RESTAURANT_FOR_ORDER = gql`
  query getMyItemCartForOrder($id: ID, $restaurant: ID) {
    getMyItemCartForOrder(id: $id, restaurant: $restaurant) {
      message
      success
      list {
        id
        userId
        platoID
        restaurant
        total
        extra
        complementos
        plato {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          cant
        }
      }
    }
  }
`;

export const GET_ORDEN = gql`
  query getOrderByUsuario($usuarios: ID!, $dateRange: DateRangeInput) {
    getOrderByUsuario(usuarios: $usuarios, dateRange: $dateRange) {
      message
      success
      list {
        id
        nota
        isvalored
        aceptaTerminos
        restaurant
        restaurants {
          _id
          title
          image
          description
          type
          rating
          address {
            calle
            ciudad
            codigoPostal
            numero
          }
          coordetate
          anadidoFavorito
          coordenateone
          categoryName
          categoryID
          minime
          city
          phone
          email
          logo
          password
          apertura
          cierre
          diaslaborales
          open
          OnesignalID
        }
        time
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos
          plato {
            _id
            title
            ingredientes
            price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
          }
        }
        cubiertos
        estado
        status
        progreso
        created_at
        total
      }
    }
  }
`;

export const GET_VALORACION = gql`
  query getValoraciones($restaurant: ID) {
    getValoraciones(restaurant: $restaurant) {
      messages
      success
      data {
        id
        comment
        value
        user
        created_at
        Usuario {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_NOTIFICATION = gql`
  query getNotifications($Id: ID) {
    getNotifications(Id: $Id) {
      messages
      success
      notifications {
        _id
        user
        usuario
        read
        ordenId
        restaurant
        type
        created_at
        Restaurant {
          _id
          title
          image
          description
          type
          logo
        }
        Usuarios {
          _id
          name
          lastName
          avatar
        }
        Orden {
          id
        }
      }
    }
  }
`;

export const RESTAURANT_SEARCH = gql`
  query getRestaurantSearch(
    $search: String
    $city: String
    $category: String
    $price: Int
  ) {
    getRestaurantSearch(
      search: $search
      city: $city
      category: $category
      price: $price
    ) {
      messages
      success
      data {
        _id
        title
        image
        description
        rating
        anadidoFavorito
        type
        address {
          calle
          numero
          codigoPostal
          ciudad
        }
        Valoracion {
          id
          comment
          value
          created_at
        }
        coordetate
        coordenateone
        categoryID
        categoryName
        minime
        menu
        city
        phone
        email
        isnew
        logo
        apertura
        cierre
        diaslaborales
        open
        OnesignalID
      }
    }
  }
`;

export const GET_OPINIONES = gql`
  query getOpinion($id: ID) {
    getOpinion(id: $id) {
      messages
      success
      data {
        id
        rating
        comment
        plato
        created_at
        user
        Usuario {
          _id
          name
          lastName
          avatar
        }
      }
    }
  }
`;

export const LOCAL_API_URL = "https://api.foodyapp.es";
export const LOCAL_API_PATH = "/graphql";
export const IMAGES_PATH = `${LOCAL_API_URL}/assets/images/`;
export const FACEBOOK_APP_ID = "1643284065840485";
export const GOOGLE_CLIENT_ID =
  "870704439978-7ab2nklj1f02dscm5ttodqd079ook4cp.apps.googleusercontent.com";
